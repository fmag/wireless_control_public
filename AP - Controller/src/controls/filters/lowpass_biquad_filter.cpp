#include "controls/filters/lowpass_biquad_filter.hpp"
#include "math.h"

namespace WirelessControl {

data_t LowPassBiquadFilter::ProcessData(data_t unfilteredState) {
	// for first run, use current state as single delayed and data_t delayed state
	if (!initialized) {
		_z2 = unfilteredState * _a2 - _b2 * unfilteredState;
		_z1 = unfilteredState * _a1 + _z2 - _b1 * unfilteredState;
		initialized = true;
	}

	// filter the data using basic formulas
	data_t retVal = unfilteredState * _a0 + _z1;
	_z1 = unfilteredState * _a1 + _z2 - _b1 * retVal;
	_z2 = unfilteredState * _a2 - _b2 * retVal;

	return retVal;
}

// TODO: rename parameters
void LowPassBiquadFilter::initialize(data_t Fs, data_t Fc, data_t Q) {
	_Fs = Fs;
	_Fc = Fc;
	_Q = Q;

	// Calculate coefficients based on basic formulas
	data_t K = tan(M_PI * _Fc / _Fs);
	data_t normalizer = 1 / (1 + K / _Q + K * K);
	_a0 = K * K * normalizer;
	_a1 = 2 * _a0;
	_a2 = _a0;
	_b1 = 2 * (K * K - 1) * normalizer;
	_b2 = (1 - K / _Q + K * K) * normalizer;

	initialized = false;
}

} // namespace WirelessControl

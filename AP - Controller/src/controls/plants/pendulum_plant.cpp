#include "controls/plants/pendulum_plant.hpp"
#include "common.hpp"
#include "controls/control_manager.hpp"
#include "math.h"

namespace WirelessControl {

PendulumPlant::PendulumPlant(ControlManager *manager)
    : Plant(manager)
    , _encoderPosition(Encoders::ENCODER_POSITION)
    , _encoderAngle(Encoders::ENCODER_ANGLE)
    , _motor(Motors::MOTOR_0){};


void PendulumPlant::TurnOff() {
	_motor.TurnOff();
}

void PendulumPlant::SetControl(control_t *currControl) {
	if (currControl == 0) {
		_motor.RunMotor(0);
		return;
	}

	// First filter the control command (low-pass)
	currControl->data[0] = manager->getFilter().ProcessData(currControl->data[0]);
	// Calculate the pre-gain required value
	data_t preGain = currControl->data[0] / 3;
	// Calculate the percentage (max/min to 100/-100%)
	data_t perct = 0;

	if (preGain < 0) {
		perct = fmax(-100, 100 * (preGain / 3.3));
	} else if (preGain > 0) {
		perct = fmin(100, 100 * (preGain / 3.3));
	}

	_motor.RunMotor(perct);
	return;
}

measure_t PendulumPlant::GetMeasurements() {
	measure_t returnVal;

	// get values from encoders
	uint16_t data0, data1;
	_encoderPosition.GetCount(data0);
	_encoderAngle.GetCount(data1);

	// convert counters to values
	returnVal.data[0] = countToPosition(data0);
	returnVal.data[1] = countToAngle(data1);

	return returnVal;
}

bool PendulumPlant::Initialize() {
	// Set-up quadrature counters
	if (_encoderAngle.Initialize() != LLATypes::SUCCESS) { return false; }
	if (_encoderPosition.Initialize() != LLATypes::SUCCESS) { return false; }
	// Set-up motor
	if (_motor.Initialize() != LLATypes::SUCCESS) { return false; }
	// Start quadrature counters
	if (_encoderAngle.Start() != LLATypes::SUCCESS) { return false; }
	if (_encoderPosition.Start() != LLATypes::SUCCESS) { return false; }
	// Start motor
	if (_motor.Start() != LLATypes::SUCCESS) { return false; }

	return true;
}

} // namespace WirelessControl

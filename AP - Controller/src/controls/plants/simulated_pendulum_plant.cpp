#include "controls/plants/simulated_pendulum_plant.hpp"
#include "controls/control_manager.hpp"
#include "math.h"

namespace WirelessControl {

SimulatedPendulumPlant::SimulatedPendulumPlant(ControlManager *manager)
    : Plant(manager) {
	Initialize();
}

bool SimulatedPendulumPlant::Initialize() {
	_measure.data[0] = 0;
	_measure.data[1] = 3.14;
	_state_plant.data[0] = 0;
	_state_plant.data[1] = 3.14;
	_state_plant.data[2] = 0;
	_state_plant.data[3] = 0;
	desState = _state_plant;
	desState.data[1] = M_PI;

	return true;
}

void SimulatedPendulumPlant::SetControl(control_t *currControl) {
	if (currControl == 0) {
		control_t tmpControl = control_t();
		_state_plant =
		    manager->getPendulumModel().GetStatePrediction(_state_plant, tmpControl, desState);
	} else {
#if SIM_PENDULUM_OSCILLATION
		if (currControl->data[0] < 0.5 && currControl->data[0] > -0.5) currControl->data[0] = 0;
#endif
		_state_plant =
		    manager->getPendulumModel().GetStatePrediction(_state_plant, *currControl, desState);
	}
}

measure_t SimulatedPendulumPlant::GetMeasurements() {
	_measure.data[0] = _state_plant.data[0];
	_measure.data[1] = _state_plant.data[1];
	return _measure;
}

void SimulatedPendulumPlant::TurnOff() {
	return;
}

} // namespace WirelessControl

#include "controls/observers/finite_difference_observer.hpp"

namespace WirelessControl {

void FiniteDifferenceObserver::initialize(const unsigned int sampleRate, const data_t cutoffFreq) {
	_samplePeriod = 1.0 / sampleRate;
	// start lowpass filters with the given sample rate (one for each state)
	for (int i = 0; i < PendulumDimensions::STATE; i++) {
		_lpFilter[i].initialize(sampleRate, cutoffFreq, 0.707);
	}
	initialized = false;
}

state_t FiniteDifferenceObserver::GetStateFromMeasurementsAndControl(const measure_t &currState,
                                                                     const control_t &prevControl) {
	return state_t();
};

state_t FiniteDifferenceObserver::GetStateFromMeasurementsAndControl(const measure_t &currState) {
	if (!initialized) {
		_z1 = currState;
		initialized = true;
	}

	state_t retVal;

	// measured states are directly output
	for (int i = 0; i < PendulumDimensions::OUTPUT; i++) { retVal.data[i] = currState.data[i]; }

	// finite difference for derivatives
	for (int i = 0; i < PendulumDimensions::OUTPUT; i++) {
		retVal.data[i + 2] = (currState.data[i] - _z1.data[i]) / _samplePeriod;
		_z1.data[i] = currState.data[i];
	}

	// filter the outputs
	for (int i = 0; i < PendulumDimensions::STATE; i++) {
		retVal.data[i] = _lpFilter[i].ProcessData(retVal.data[i]);
	}

	return retVal;
}

state_t FiniteDifferenceObserver::GetStatePrediction(const state_t &currState,
                                                     const control_t &prevControl,
                                                     const state_t &desState) {
	return currState;
}

} // namespace WirelessControl

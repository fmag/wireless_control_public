#include "controls/observers/kalman_observer.hpp"
#include "common.hpp"
#include "stdio.h"

namespace WirelessControl {

void KalmanObserver::initialize(const unsigned int sampleRate) {
	// mean starts as zero, covar starts as identity
	for (int i = 0; i < PendulumDimensions::STATE; i++) {
		for (int j = 0; j < PendulumDimensions::STATE; j++) {
			if (i == j) {
				_P[i][j] = 1;
			} else {
				_P[i][j] = 0;
			}
		}
		_xCurr.data[i] = 0;
	}

	// clang-format off
    // TODO: Include all necessary sample rates as accurate as possible. For example, more precision in the 50Hz case (currently only 6 digits after comma) or precise values for 33Hz case which is 30,303030303...ms and not 30ms.
	switch (sampleRate) {
	    // 66Hz is only used for sampling with finite difference observer, thus no system matrices needed
    case 66:
        break;
        // 200Hz is not working!!!
	case 200:
		_A[0][0] = 1;  _A[0][1] = 1.8868e-5;  _A[0][2] = 0.0049;      _A[0][3] = -2.9271e-8;
	    _A[1][0] = 0;  _A[1][1] = 1.0003;     _A[1][2] = -1.3676e-4;  _A[1][3] = 0.005;
	    _A[2][0] = 0;  _A[2][1] = 0.0075;     _A[2][2] = 0.9764;      _A[2][3] = -5.3472e-6;
	    _A[3][0] = 0;  _A[3][1] = 0.1303;     _A[3][2] = -0.0545;     _A[3][3] = 0.9999;

	    _B[0][0] = 1.0992e-5;  _B[0][1] = 2.5325e-5;  _B[0][2] = 0.0044;  _B[0][3] = 0.0101;
	    break;
	case 100:
	    // discrete matrices for delay of 10ms (Quanser)
	    _A[0][0] = 1;   _A[0][1] = 7.487781080141488e-05;   _A[0][2] = 0.009764443835106;       _A[0][3] = 9.373473102897065e-09;
	    _A[1][0] = 0;   _A[1][1] = 1.001302618322111;       _A[1][2] = -5.427164506414011e-04;  _A[1][3] = 0.010000148089865;
	    _A[2][0] = 0;   _A[2][1] = 0.014858215273336;       _A[2][2] = 0.953261121293411;       _A[2][3] = 2.701436340800697e-05;
	    _A[3][0] = 0;   _A[3][1] = 0.260270783333524;       _A[3][2] = -0.107692756635168;      _A[3][3] = 1.000464196161915;

	    _B[0][0] = 4.362151201736732e-05;   _B[0][1] = 1.005030464150743e-04;    _B[0][2] = 0.008655347908628;    _B[0][3] = 0.019943103080587;
	    break;
	case 50:
		// discrete matrices for delay of 20ms (Quanser)
		_A[0][0] = 1;           _A[0][1] = 2.949378069786926e-04;    _A[0][2] = 0.019072466867291;    _A[0][3] = 1.031320728234340e-06;
		_A[1][0] = 0;           _A[1][1] = 1.005201616037660;    _A[1][2] = -0.002137717409874;   _A[1][3] = 0.020017949925078;
		_A[2][0] = 0;           _A[2][1] = 0.029048359858190;    _A[2][2] = 0.908695792320451;    _A[2][3] = 2.013629988096415e-04;
		_A[3][0] = 0;           _A[3][1] = 0.519401294695346;    _A[3][2] = -0.210543318380419;   _A[3][3] = 1.003528444927444;

		_B[0][0] = 1.717653949460677e-04;    _B[1][0] = 3.958735944211041e-04;    _B[2][0] = 0.016908186607324;    _B[3][0] = 0.038989503403781;
		break;
	case 40:
		// discrete matrices for delay of 25ms (Quanser)
		_A[0][0] = 1;   _A[0][1] = 4.574063505980101e-04;   _A[0][2] = 0.023561973562976;   _A[0][3] = 2.374508900498661e-06;
		_A[1][0] = 0;   _A[1][1] = 1.008122072920109;       _A[1][2] = -0.003315293922732;  _A[1][3] = 0.025041581825534;
		_A[2][0] = 0;   _A[2][1] = 0.035914843515506;       _A[2][2] = 0.887191373064059;   _A[2][3] = 3.417122250504184e-04;
		_A[3][0] = 0;   _A[3][1] = 0.648772826656170;       _A[3][2] = -0.260311782482142;  _A[3][3] = 1.006032151385949;

		_B[0][0] = 2.663011920415044e-04;   _B[1][0] = 6.139433190243959e-04;   _B[2][0] = 0.020890486469619;   _B[3][0] = 0.048205885644841;
		break;
	case 33:
		// discrete matrices for delay of 30ms (Quanser)
		_A[0][0] = 1;   _A[0][1] = 6.538510503329506e-04;   _A[0][2] = 0.027945213853480;   _A[0][3] = 4.505825234589201e-06;
		_A[1][0] = 0;   _A[1][1] = 1.011689334922388;       _A[1][2] = -0.004739130557996;  _A[1][3] = 0.030079349452076;
		_A[2][0] = 0;   _A[2][1] = 0.042640205389675;       _A[2][2] = 0.866186919542813;   _A[2][3] = 5.164922094515117e-04;
		_A[3][0] = 0;   _A[3][1] = 0.778138712848224;       _A[3][2] = -0.309057391983309;  _A[3][3] = 1.009182681151257;

		_B[0][0] = 3.805159530593106e-04;   _B[1][0] = 8.776167699993141e-04;   _B[2][0] = 0.024780200084664;   _B[3][0] = 0.057232850367280;
		break;

	case 28:
		// discrete matrices for delay of 35ms (Quanser)
		_A[0][0] = 1;   _A[0][1] = 8.835868780472802e-04;   _A[0][2] = 0.032224653214274;   _A[0][3] = 7.595627571185288e-06;
		_A[1][0] = 0;   _A[1][1] = 1.015903603970063;       _A[1][2] = -0.006404262212726;  _A[1][3] = 0.035134486493014;
		_A[2][0] = 0;   _A[2][1] = 0.049232644966826;       _A[2][2] = 0.845668751580996;   _A[2][3] = 7.249915104975698e-04;
		_A[3][0] = 0;   _A[3][1] = 0.907590937624686;       _A[3][2] = -0.356839576986930;  _A[3][3] = 1.012979939835537;

		_B[0][0] = 5.139531084677675e-04;   _B[1][0] = 0.001185974483838;   _B[2][0] = 0.028579860818334;   _B[3][0] = 0.066081403145728;
		break;

	case 25:
		// discrete matrices for delay of 40ms (Quanser)
		_A[0][0] = 1;   _A[0][1] = 0.001145969317450;    _A[0][2] = 0.036402689627397;      _A[0][3] = 1.181081978906656e-05;
		_A[1][0] = 0;   _A[1][1] = 1.020765540808358;       _A[1][2] = -0.008306017414959;   _A[1][3] = 0.040210227316211;
		_A[2][0] = 0;   _A[2][1] = 0.055700168851929;      _A[2][2] = 0.825623550162326;      _A[2][3] = 9.665398198555815e-04;
		_A[3][0] = 0;   _A[3][1] = 1.037221056280686;       _A[3][2] = -0.403716369588019;     _A[3][3] = 1.017424293252758;

		_B[0][0] = 6.661685875191214e-04;   _B[1][0] = 0.001538151373141;    _B[2][0] = 0.032291935155125; _B[3][0] = 0.074762290664448;
		break;

    case 22:
        // discrete matrices for delay of ~45ms (Quanser)
        _A[0][0] = 1;   _A[0][1] = 0.001468728219556;    _A[0][2] = 0.040847636718333;   _A[0][3] = 1.788493300882636e-05;
        _A[1][0] = 0;   _A[1][1] = 1.026809459745597;    _A[1][2] = -0.010645382894383;   _A[1][3] = 0.045774701841470;
        _A[2][0] = 0;   _A[2][1] = 0.062622369838584;    _A[2][2] = 0.804280244371657;   _A[2][3] = 0.001266999919805;
        _A[3][0] = 0;   _A[3][1] = 1.178945771479537;    _A[3][2] = -0.453888674439014;  _A[3][3] = 1.023011667892753;

        _B[0][0] = 8.531312474466755e-04;   _B[1][0] = 0.001971367202663;    _B[2][0] = 0.036244399190434; _B[3][0] = 0.084053458229447;
        break;

    case 20:
        // discrete matrices for delay of 50ms (Quanser)
        _A[0][0] = 1;   _A[0][1] = 0.001766292899212;    _A[0][2] = 0.044463818691273;   _A[0][3] = 2.426893416695429e-05;
        _A[1][0] = 0;   _A[1][1] = 1.032437343471188;    _A[1][2] = 0.012802139950320;   _A[1][3] = 0.050436474940743;
        _A[2][0] = 0;   _A[2][1] = 0.068291579431505;    _A[2][2] = 0.786900512458449;   _A[2][3] = 0.001546302115702;
        _A[3][0] = 0;   _A[3][1] = 1.297379472491418;    _A[3][2] = -0.494979262896151;  _A[3][3] = 1.028258035728737;

        _B[0][0] = 0.001025218760875;   _B[1][0] = 0.002370766657467;    _B[2][0] = 0.039462868063250; _B[3][0] = 0.091662826462251;
        break;
	default:
		ASSERT_PRINT();
	}
	// clang-format on
}


state_t KalmanObserver::GetStateFromMeasurementsAndControl(const measure_t &newMeasurement,
                                                           const control_t &prevControl) {

	//	state_t xNew;
	//	data_t pNew[PendulumDimensions::STATE][PendulumDimensions::STATE];
	//	data_t K[PendulumDimensions::STATE][PendulumDimensions::OUTPUT];
	//
	//	xNew = _A*_xCurr + _B*prevControl;
	//	pNew = _A*_P*_A + _Q;
	//
	//	K = pNew*_C*inv(_C*pNew*C + _R);
	//
	//	_xCurr = xNew + K*(newMeasurement - _C*xNew);
	//	_P = (eye(size(_A)) - K*_C)*pNew;
	//
	// return _xCurr;
	return state_t();
}

state_t KalmanObserver::GetStateFromMeasurementsAndControl(const measure_t &newMeasurement) {
	return _xCurr;
}

state_t KalmanObserver::GetStatePrediction(const state_t &currState, const control_t &prevControl,
                                           const state_t &desState) {
	state_t xPred1;

	for (int i = 0; i < PendulumDimensions::STATE; i++) {
		xPred1.data[i] = 0;
		for (int j = 0; j < PendulumDimensions::STATE; j++) {
			xPred1.data[i] += _A[i][j] * (desState.data[j] - currState.data[j]);
		}
		for (int j = 0; j < PendulumDimensions::CONTROL; j++) {
			xPred1.data[i] += _B[i][j] * (prevControl.data[j]);
		}
	}

	for (int i = 0; i < PendulumDimensions::STATE; i++) {
		xPred1.data[i] = desState.data[i] - xPred1.data[i];
	}
	return xPred1;
}

} // namespace WirelessControl

#include "controls/controllers/fbff_controller.hpp"
#include "string.h"

namespace WirelessControl {

control_t FeedforwardFeedbackController::GetControlCommand(const state_t &currState,
                                                           const state_t &desState,
                                                           bool isLocalState) {
	control_t retVal;

	// manual -1*K*(x-xd) matrix multiplication
	if (isLocalState) {
		for (int i = 0; i < PendulumDimensions::CONTROL; i++) {
			retVal.data[i] = 0;
			for (int j = 0; j < PendulumDimensions::STATE; j++) {
				retVal.data[i] += _K[j][i] * (currState.data[j] - desState.data[j]);
			}
		}
	} else {
		for (int i = 0; i < PendulumDimensions::CONTROL; i++) {
			retVal.data[i] = 0;
			for (int j = 0; j < PendulumDimensions::STATE; j++) {
				retVal.data[i] +=
				    _K[j + PendulumDimensions::STATE][i] * (currState.data[j] - desState.data[j]);
			}
		}
	}

	return retVal;
}


void FeedforwardFeedbackController::SetGains(data_t K[][PendulumDimensions::CONTROL],
                                             unsigned int nbPlants) {
	unsigned int i;
	for (i = 0; i < nbPlants * PendulumDimensions::STATE; i++) {
		memcpy(&_K[i][0], &K[i][0], sizeof(data_t) * PendulumDimensions::CONTROL);
	}
}

} // namespace WirelessControl

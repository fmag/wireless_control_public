#include "controls/control_manager.hpp"
#include "common.hpp"
#include "config.hpp"
#include "controls/plants/pendulum_plant.hpp"
#include "controls/plants/simulated_pendulum_plant.hpp"
#include "driverlib/MSP432P4xx/gpio.h"
#include "driverlib/MSP432P4xx/timer32.h"
#include "modes/localStab.hpp"
#include "modes/localStabRemoteSync.hpp"
#include "modes/remoteStab1Loop.hpp"
#include "modes/timeObserver.hpp"
#include "stdio.h"
#include "string.h"

namespace WirelessControl {

bool ControlManager::syncLineHigh = 0;
ControlManager *ControlManager::manager = 0;

typedef struct __attribute__((packed)) control_pkt_t_tag {
	uint8_t curSchedID : 4;
	uint8_t newSchedID : 4;
	uint8_t newSchedRnds;
} control_pkt_t;

/**************************************************************************************************/

void ControlManager::syncLineISR(void) {
	uint32_t status = GPIO_getInterruptStatus(GPIO_PORT_P4, PIN_ALL8);
	GPIO_clearInterruptFlag(GPIO_PORT_P4, PIN_ALL8);

	manager->getTimeObserver().reset(TIMER32_1_BASE);
	syncLineHigh = true;
	GPIO_setOutputHighOnPin(GPIO_PORT_P2, GPIO_PIN2);
	GPIO_setOutputHighOnPin(GPIO_PORT_P2, GPIO_PIN3);
	GPIO_setOutputLowOnPin(GPIO_PORT_P2, GPIO_PIN3);
	GPIO_setOutputLowOnPin(GPIO_PORT_P2, GPIO_PIN2);
}

/**************************************************************************************************/

ControlManager::ControlManager()
    : curSchedId(DEFAULT_MODE_ID)
    , led(LEDs::LED_A)
    // TODO: For the moment we keep timerFast in order to use the PeriodElapsedCallback function.
    , timerFast(Timers::TIMER_A)
    , timeObserver()
    , bolt()
    , serOutput(SerialPorts::DEBUG, SerialPorts::br115200)
    , controller()
    , predictor()
    , observer()
    , lpFilter()
    , pendulumModel() {

	if (serOutput.Initialize() != LLATypes::SUCCESS) {
		while (1) {
			GPIO_setOutputHighOnPin(GPIO_PORT_P2, GPIO_PIN3);
			GPIO_setOutputLowOnPin(GPIO_PORT_P2, GPIO_PIN3);
		}
	}

	if (bolt.Initialize() != LLATypes::SUCCESS) { ASSERT_PRINT(); }
	if (led.Initialize() != LLATypes::SUCCESS) { ASSERT_PRINT(); }

	if (REAL_PENDULUM_CONNECTED) {
		plant = new PendulumPlant(this);
	} else {
		plant = new SimulatedPendulumPlant(this);
	}

	if (plant->Initialize() != true) {
		plant->TurnOff();
		ASSERT_PRINT();
	}

	memset((void *)modeList, 0, sizeof(modeList));
	modeCnt = 0;

	// 0: Local stabilization.
	uint16_t plantIds1[5] = {2, 3, 5, 6, 7};
	modeList[modeCnt++] = new LocalStab(this, 100, plantIds1, NUM_ELEMS(plantIds1));

	// 1: Remote stabilization (1 pendulum - 1 loop)
#if NODE_ID <= 2
	uint16_t plantIds2[1] = {2};
	modeList[modeCnt++] = new RemoteStab1Loop(this, 100, 25, plantIds2, NUM_ELEMS(plantIds2), 1);
#else
	// The other pendulums are stabilized locally and relay nodes need to know which nodes have a
	// pendulum.
	uint16_t plantIds2[5] = {2, 3, 5, 6, 7};
	modeList[modeCnt++] = new LocalStab(this, 100, plantIds2, NUM_ELEMS(plantIds2));
#endif

	// 2: Remote stabilization (2 pendulums - 1 loop)
#if NODE_ID <= 3
	uint16_t plantIds3[2] = {2, 3};
	modeList[modeCnt++] = new RemoteStab1Loop(this, 100, 25, plantIds3, NUM_ELEMS(plantIds3), 1);
#else
	// The other pendulums are stabilized locally and relay nodes need to know which nodes have a
	// pendulum.
	uint16_t plantIds3[5] = {2, 3, 5, 6, 7};
	modeList[modeCnt++] = new LocalStab(this, 100, plantIds3, NUM_ELEMS(plantIds3));
#endif

	// 3: Remote stabilization (2 pendulums - 2 loops)
#if NODE_ID <= 2
	uint16_t plantIds4[1] = {2};
	modeList[modeCnt++] = new RemoteStab1Loop(this, 66, 22, plantIds4, NUM_ELEMS(plantIds4), 1);
#elif NODE_ID <= 4
	uint16_t plantIds4[1] = {3};
	modeList[modeCnt++] = new RemoteStab1Loop(this, 66, 22, plantIds4, NUM_ELEMS(plantIds4), 4);
#else
	// The other pendulums are stabilized locally and relay nodes need to know which nodes have a
	// pendulum.
	uint16_t plantIds4[5] = {2, 3, 5, 6, 7};
	modeList[modeCnt++] = new LocalStab(this, 100, plantIds4, NUM_ELEMS(plantIds4));
#endif

	// 4: Remote stabilization (3 pendulums - 1 loop)
#if NODE_ID <= 5
	uint16_t plantIds5[3] = {2, 3, 5};
	modeList[modeCnt++] = new RemoteStab1Loop(this, 66, 22, plantIds5, NUM_ELEMS(plantIds5), 1);
#else
	// The other pendulums are stabilized locally and relay nodes need to know which nodes have a
	// pendulum.
	uint16_t plantIds5[5] = {2, 3, 5, 6, 7};
	modeList[modeCnt++] = new LocalStab(this, 100, plantIds5, NUM_ELEMS(plantIds5));
#endif

	// 5: Local stabilization and synchronization (5 pendulums)
	uint16_t plantIds6[5] = {2, 3, 5, 6, 7};
	modeList[modeCnt++] = new LocalStabRemoteSync(this, 100, 20, plantIds6, NUM_ELEMS(plantIds6));

	/*
	    // 4: Local stabilization and synchronization (2 pendulums)
	    // TODO: Test switching a controller to a synchronized pendulum back and forth (e.g. nodes
	1,4).
	    // However, in which scenario is this useful? A pendulum cannot be paused while the node
	acts as
	    // a controller.
	#if NODE_ID <= 3
	    uint16_t plantIds5[2] = {2, 3};
	    modeList[modeCnt++] = new LocalStabRemoteSync(this, 100, 20, plantIds5,
	NUM_ELEMS(plantIds5)); #else
	    // The other pendulums are stabilized locally and relay nodes need to know which nodes have
	a
	    // pendulum.
	    uint16_t plantIds5[5] = {2, 3, 5, 6, 7};
	    modeList[modeCnt++] = new LocalStab(this, 100, plantIds5, NUM_ELEMS(plantIds5));
	#endif

	    // 5: Local stabilization and synchronization (3 pendulums)
	#if NODE_ID <= 5
	    uint16_t plantIds6[3] = {2, 3, 5};
	    modeList[modeCnt++] = new LocalStabRemoteSync(this, 100, 20, plantIds6,
	NUM_ELEMS(plantIds6)); #else
	    // The other pendulums are stabilized locally and relay nodes need to know which nodes have
	a
	    // pendulum.
	    uint16_t plantIds6[5] = {2, 3, 5, 6, 7};
	    modeList[modeCnt++] = new LocalStab(this, 100, plantIds6, NUM_ELEMS(plantIds6));
	#endif

	    // 6: Local stabilization and synchronization (4 pendulums)
	#if NODE_ID <= 6
	    uint16_t plantIds7[4] = {2, 3, 5, 6};
	    modeList[modeCnt++] = new LocalStabRemoteSync(this, 100, 20, plantIds7,
	NUM_ELEMS(plantIds7)); #else
	    // The other pendulums are stabilized locally and relay nodes need to know which nodes have
	a
	    // pendulum.
	    uint16_t plantIds7[5] = {2, 3, 5, 6, 7};
	    modeList[modeCnt++] = new LocalStab(this, 100, plantIds7, NUM_ELEMS(plantIds7));
	#endif

	    // 7: Local stabilization and synchronization (5 pendulums)
	    uint16_t plantIds8[5] = {2, 3, 5, 6, 7};
	    modeList[modeCnt++] = new LocalStabRemoteSync(this, 100, 20, plantIds8,
	NUM_ELEMS(plantIds8));

	    // 8: Remote stabilization (1 pendulum, 30 ms)
	#if NODE_ID <= 2
	    uint16_t plantIds9[1] = {2};
	    modeList[modeCnt++] = new RemoteStab1Loop(this, 100, 33, plantIds9, NUM_ELEMS(plantIds9),
	1); #else
	    // The other pendulums are stabilized locally and relay nodes need to know which nodes have
	a
	    // pendulum.
	    uint16_t plantIds9[5] = {2, 3, 5, 6, 7};
	    modeList[modeCnt++] = new LocalStab(this, 100, plantIds9, NUM_ELEMS(plantIds9));
	#endif

	    // 9: Remote stabilization (1 pendulum, 40 ms)
	#if NODE_ID <= 2
	    uint16_t plantIds10[1] = {2};
	    modeList[modeCnt++] = new RemoteStab1Loop(this, 100, 25, plantIds10, NUM_ELEMS(plantIds10),
	1); #else
	    // The other pendulums are stabilized locally and relay nodes need to know which nodes have
	a
	    // pendulum.
	    uint16_t plantIds10[5] = {2, 3, 5, 6, 7};
	    modeList[modeCnt++] = new LocalStab(this, 100, plantIds10, NUM_ELEMS(plantIds10));
	#endif
	*/

	// Set up initial mode.
	modeList[curSchedId]->setup();

	// Setup sync line interrupt.
	GPIO_unregisterInterrupt(GPIO_PORT_P4);
	GPIO_registerInterrupt(GPIO_PORT_P4, syncLineISR);
	GPIO_interruptEdgeSelect(GPIO_PORT_P4, GPIO_PIN2, GPIO_LOW_TO_HIGH_TRANSITION);
	GPIO_clearInterruptFlag(GPIO_PORT_P4, GPIO_PIN2);
	GPIO_enableInterrupt(GPIO_PORT_P4, GPIO_PIN2);

	manager = this;

	LLATypes::lla_return_t retCode = LLATypes::FAILURE;
	char stringBuffer[200];
	int bytes = sprintf(stringBuffer, "\n\n\nInit done! Number of modes: %u\r\n", modeCnt);
	do {
		retCode = serOutput.Write((uint8_t *)stringBuffer, bytes);
	} while (retCode == LLATypes::FAILURE);
}

/**************************************************************************************************/

ControlManager::~ControlManager() {
	delete plant;
	for (unsigned int i = 0; i < modeCnt; i++) { delete modeList[i]; }
}

/**************************************************************************************************/

// This function is executed after a sync line event. If a node (CP) is bootstrapping than there is
// no sync line event but otherwise there will be such an event even if nothing was received.
void ControlManager::processRound() {
	// The first message is always the control packet.
	// TODO: make it more fault tolerant if this is not the case (e.g. include msg types)
	control_pkt_t control_pkt;
	uint8_t control_pkt_len = 0;
	bolt.Read((uint8_t *)&control_pkt, control_pkt_len);

	if (control_pkt_len == 2) { // workaround for control pkt type detection (has always length 2)
		// AP is in the right mode -> execute control tasks
		if (control_pkt.curSchedID == curSchedId) {
			GPIO_setOutputHighOnPin(GPIO_PORT_P2, GPIO_PIN3);
			GPIO_setOutputLowOnPin(GPIO_PORT_P2, GPIO_PIN3);
			modeList[curSchedId]->operation();
		}

		// The mode changes in the next round -> prepare internal structures
		if (control_pkt.newSchedRnds == 1) {
			GPIO_setOutputHighOnPin(GPIO_PORT_P2, GPIO_PIN3);
			GPIO_setOutputLowOnPin(GPIO_PORT_P2, GPIO_PIN3);
			GPIO_setOutputHighOnPin(GPIO_PORT_P2, GPIO_PIN3);
			GPIO_setOutputLowOnPin(GPIO_PORT_P2, GPIO_PIN3);
			// if ((control_pkt.newSchedID != curSchedID) && (control_pkt.newSchedRnds == 1)) {
			// First tear down old mode state.
			modeList[curSchedId]->teardown();
			// Change to new mode.
			curSchedId = control_pkt.newSchedID;
			// Set up structures for new mode.
			modeList[curSchedId]->setup();
		}
		// No mode change but AP is in the wrong mode -> prepare internal data structures
		else if (control_pkt.curSchedID != curSchedId) {
			GPIO_setOutputHighOnPin(GPIO_PORT_P2, GPIO_PIN3);
			GPIO_setOutputLowOnPin(GPIO_PORT_P2, GPIO_PIN3);
			GPIO_setOutputHighOnPin(GPIO_PORT_P2, GPIO_PIN3);
			GPIO_setOutputLowOnPin(GPIO_PORT_P2, GPIO_PIN3);
			GPIO_setOutputHighOnPin(GPIO_PORT_P2, GPIO_PIN3);
			GPIO_setOutputLowOnPin(GPIO_PORT_P2, GPIO_PIN3);
			// First tear down old mode state.
			modeList[curSchedId]->teardown();
			// Change to new mode.
			curSchedId = control_pkt.curSchedID;
			// Set up structures for new mode.
			modeList[curSchedId]->setup();
		}
	} else {
		char buffer[100];
		int buffer_bytes = sprintf(
		    buffer,
		    "syncLine event but control_pkt empty or with wrong size! (should not happen)\r\n");
		serOutput.Write((uint8_t *)buffer, buffer_bytes);
		bolt.Flush();
	}

	GPIO_setOutputHighOnPin(GPIO_PORT_P2, GPIO_PIN3);
	GPIO_setOutputLowOnPin(GPIO_PORT_P2, GPIO_PIN3);

	// The Bolt queue should be empty but we can flush it again to be sure.
	// GPIO_setOutputHighOnPin(GPIO_PORT_P2, GPIO_PIN3);
	// GPIO_setOutputLowOnPin(GPIO_PORT_P2, GPIO_PIN3);
	// GPIO_setOutputHighOnPin(GPIO_PORT_P2, GPIO_PIN3);
	// GPIO_setOutputLowOnPin(GPIO_PORT_P2, GPIO_PIN3);
	// bolt.Flush();
}

} // namespace WirelessControl

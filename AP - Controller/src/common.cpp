#include "common.hpp"
#include "float.h"
#include "math.h"
#include "stdint.h"

namespace WirelessControl {

const controlRequirements_t lowReq = {-0.3, 0.3, (180 - 20) * M_PI / 180,
                                      (180 + 20) * M_PI / 180};
const controlRequirements_t highReq = {-0.05, 0.05, (180 - 2) * M_PI / 180, (180 + 2) * M_PI / 180};

bool isEqual(float a, float b) {
	return std::fabsf(a - b) < FLT_EPSILON;
}

bool isEqual(double a, double b) {
	return std::fabs(a - b) < DBL_EPSILON;
}

double countToPosition(uint16_t count) {
	return static_cast<double>(static_cast<int16_t>(count) * EncoderConstants::pos_enc_resolution);
}

double countToAngle(uint16_t count) {
	return static_cast<double>(static_cast<int16_t>(count) % EncoderConstants::counts_per_rev *
	                           (EncoderConstants::ang_enc_resolution));
}

float radToDeg(float angleRad) {
	return angleRad * 180 / M_PI;
}

float degToRad(float angleDeg) {
	return angleDeg * M_PI / 180;
}

} // namespace WirelessControl

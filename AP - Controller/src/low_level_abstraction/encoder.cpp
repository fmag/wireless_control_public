/*
 * encoder.cpp
 *
 *  Created on: May 10, 2016
 *      Author: singhh23
 *
 */

// NOTE This file is hardware specific - it assumes that the ls7366 is being used.

#include "drivers_msp432/ls7366.h"
#include "drivers_msp432/board_config.h"
#include "low_level_abstraction/encoder.hpp"
#include "low_level_abstraction/hw_config.hpp"

namespace WirelessControl {

LLATypes::lla_return_t EncoderInterface::DoInitialize() {
	if(DRV_LS7366_Init(static_cast<drv_ls7366_device_t>(_encType))
			!= DRV_OK) {
		return LLATypes::FAILURE;
	}

	return LLATypes::SUCCESS;
}

LLATypes::lla_return_t EncoderInterface::Start() {
	if(DRV_LS7366_Start(static_cast<drv_ls7366_device_t>(_encType))
			!= DRV_OK) {
		return LLATypes::FAILURE;
	}

	return LLATypes::SUCCESS;
}

LLATypes::lla_return_t EncoderInterface::GetCount(uint16_t& count) {
	if(DRV_LS7366_GetCount(static_cast<drv_ls7366_device_t>(_encType), &count)
			!= DRV_OK) {
		return LLATypes::FAILURE;
	}

	return LLATypes::SUCCESS;
}

}

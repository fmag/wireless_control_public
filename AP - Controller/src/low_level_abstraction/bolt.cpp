#include "drivers_msp432/bolt.h"
#include "low_level_abstraction/bolt.hpp"

namespace WirelessControl {

LLATypes::lla_return_t BoltInterface::DoInitialize() {
	if (!bolt_init()) { return LLATypes::FAILURE; }

	bolt_flush();

	return LLATypes::SUCCESS;
}

// TODO: Fix Bolt API to return the correct number of bytes (based on SPI speeds compute the number
// of trash bytes) or send the number of bytes as the first element over Bolt.
void BoltInterface::Write(uint8_t *data, const uint8_t numBytes) {
	BOLT_WRITE(data, numBytes);
}

// TODO: Prevent buffer overflows when writing to buffer (should work if buffer is at least as big
// as the maximum size of a Bolt message plus SPI trash bytes).
void BoltInterface::Read(uint8_t *buffer, uint8_t &numBytes) {
	BOLT_READ(buffer, numBytes);
}

void BoltInterface::Flush(void) {
	uint8_t trashbin[BOLT_MAX_MSG_LEN + 2]; // TODO: 2 trash bytes
	uint8_t trashbinBytes;
	while (GPIO_getInputPinValue(GPIO_PORT_P4, GPIO_PIN4)) { Read(trashbin, trashbinBytes); }
}

} // namespace WirelessControl

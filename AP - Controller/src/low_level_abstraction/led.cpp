/*
 * led.cpp
 *
 *  Created on: Aug 11, 2016
 *      Author: singhh23
 */


#include "drivers_msp432/led.h"
#include "drivers_msp432/board_config.h"
#include "low_level_abstraction/led.hpp"
#include "low_level_abstraction/hw_config.hpp"

namespace WirelessControl {

LLATypes::lla_return_t LEDInterface::DoInitialize() {
	if(DRV_LED_Init(static_cast<drv_led_device_t>(_ledId))
			!= DRV_OK) {
		return LLATypes::FAILURE;
	}

	return LLATypes::SUCCESS;
}

LLATypes::lla_return_t LEDInterface::TurnOn() {
	if(DRV_LED_TurnOn(static_cast<drv_led_device_t>(_ledId))
			!= DRV_OK) {
		return LLATypes::FAILURE;
	}
	_on = true;

	return LLATypes::SUCCESS;
}

LLATypes::lla_return_t LEDInterface::TurnOff() {
	if(DRV_LED_TurnOff(static_cast<drv_led_device_t>(_ledId))
			!= DRV_OK) {
		return LLATypes::FAILURE;
	}
	_on = false;

	return LLATypes::SUCCESS;
}

LLATypes::lla_return_t LEDInterface::Toggle() {
	if(_on) {
		return TurnOff();
	} else  {
		return TurnOn();
	}
}


}

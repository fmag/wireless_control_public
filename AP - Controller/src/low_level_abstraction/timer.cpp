/*
 * timer.cpp
 *
 *  Created on: May 11, 2016
 *      Author: singhh23
 */

#include "low_level_abstraction/timer.hpp"
#include "drivers_msp432/t32.h"
#include "low_level_abstraction/timer_listener.hpp"
#include "stdlib.h"

namespace WirelessControl {

void TimerElapsed(void *usrObj) {
	// if timer not set-up correctly, exit
	if (usrObj == NULL) { return; }

	// else cast to correct class and call the related listener
	TimerInterface *timer = static_cast<TimerInterface *>(usrObj);

	timer->NotifyListener();
}

LLATypes::lla_return_t TimerInterface::DoInitialize() {

	if (DRV_T32_Setup(static_cast<t32_timer_t>(_timerNum), &TimerElapsed, this) == DRV_OK) {
		return LLATypes::SUCCESS;
	}

	return LLATypes::FAILURE;
}

LLATypes::lla_return_t TimerInterface::Start(uint32_t msPeriod) {

	if (DRV_T32_Start(static_cast<t32_timer_t>(_timerNum), msPeriod, this) == DRV_OK) {
		return LLATypes::SUCCESS;
	}

	return LLATypes::FAILURE;
}

void TimerInterface::Halt() {
	DRV_T32_Halt(static_cast<t32_timer_t>(_timerNum));
}

void TimerInterface::NotifyListener() {
	// if listener exists, notify them of elapsed period
	if (_listener != NULL) { _listener->PeriodElapsedCallback(_timerNum); }
}


} // namespace WirelessControl

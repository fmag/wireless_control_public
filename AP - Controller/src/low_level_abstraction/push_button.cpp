/*
 * push_button.cpp
 *
 *  Created on: Oct 14, 2016
 *      Author: singhh23
 */

#include "drivers_msp432/push_button.h"
#include "low_level_abstraction/push_button.hpp"

namespace WirelessControl {

LLATypes::lla_return_t PushButtonInterface::DoInitialize() {
	if(DRV_PB_Init() != DRV_OK) {
		return LLATypes::FAILURE;
	}

	if(DRV_PB_RegisterInterrupt(static_cast<drv_pb_device_t>(_portId), this, &PressedCallback) != DRV_OK) {
		return LLATypes::FAILURE;
	}

	return LLATypes::SUCCESS;
}

LLATypes::lla_return_t PushButtonInterface::Start() {
	if(DRV_PB_EnableInterrupt(static_cast<drv_pb_device_t>(_portId)) != DRV_OK) {
		return LLATypes::FAILURE;
	}

	return LLATypes::SUCCESS;
}

bool PushButtonInterface::ButtonWasPressed() {
	if(_wasPressed) {
		_wasPressed = false;
		return true;
	}

	return false;
}

}

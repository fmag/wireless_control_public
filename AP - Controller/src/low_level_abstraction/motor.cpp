/*
 * motor.cpp
 *
 *  Created on: Jun 15, 2016
 *      Author: singhh23
 */

// NOTE This file is hardware specific - it assumes that the DAC8831 is being used.

#include "drivers_msp432/dac8831.h"
#include "drivers_msp432/board_config.h"
#include "low_level_abstraction/motor.hpp"

namespace WirelessControl {

LLATypes::lla_return_t MotorInterface::DoInitialize() {
	if(DRV_DAC8831_Init(static_cast<drv_dac8831_device_t>(_motorType))
			!= DRV_OK) {
		return LLATypes::FAILURE;
	}

	return LLATypes::SUCCESS;
}

LLATypes::lla_return_t MotorInterface::Start() {
	if(DRV_DAC8831_Start(static_cast<drv_dac8831_device_t>(_motorType))
			!= DRV_OK) {
		return LLATypes::FAILURE;
	}

	return LLATypes::SUCCESS;
}

LLATypes::lla_return_t MotorInterface::RunMotor(int perct) {
	// extraneous bound check...
	if(perct < -100 || perct > 100) {
		return LLATypes::FAILURE;
	}

	if(DRV_DAC8831_SetVoltage(static_cast<drv_dac8831_device_t>(_motorType), perct)
			!= DRV_OK) {
		return LLATypes::FAILURE;
	}

	return LLATypes::SUCCESS;
}

void MotorInterface::TurnOff() {
	DRV_DAC8831_TurnOff(static_cast<drv_dac8831_device_t>(_motorType));
}


}

/*
 * serial_port.cpp
 *
 *  Created on: Jul 28, 2016
 *      Author: singhh23
 */

#include "drivers_msp432/uart.h"
#include "drivers_msp432/board_config.h"
#include "low_level_abstraction/serial_port.hpp"

namespace WirelessControl {

LLATypes::lla_return_t SerialPortInterface::DoInitialize() {
	if(DRV_UART_Init(static_cast<uart_bus_t>(_portId),
			static_cast<drv_baud_rate_t>(_baudRate)) != DRV_OK) {
		return LLATypes::FAILURE;
	}

	if(DRV_UART_Start(static_cast<uart_bus_t>(_portId)) != DRV_OK) {
		return LLATypes::FAILURE;
	}

	return LLATypes::SUCCESS;
}

LLATypes::lla_return_t SerialPortInterface:: Write(uint8_t *data, const uint16_t length) {
	//ToDo need better error returns...
	if(DRV_UART_Write(static_cast<uart_bus_t>(_portId), data, length) != DRV_OK) {
		return LLATypes::FAILURE;
	}

	return LLATypes::SUCCESS;
}

LLATypes::lla_return_t SerialPortInterface::GetByte(uint8_t &byte) {
	// ToDo need better error returns
	if(DRV_UART_GetByte(static_cast<uart_bus_t>(_portId), &byte) != DRV_OK) {
		return LLATypes::FAILURE;
	}

	return LLATypes::SUCCESS;
}

uint16_t SerialPortInterface::GetRxStatus() {
	return DRV_UART_GetRxAmount(static_cast<uart_bus_t>(_portId));
}

uint16_t SerialPortInterface::GetTxStatus() {
	return DRV_UART_GetTxAmount(static_cast<uart_bus_t>(_portId));
}

LLATypes::lla_return_t SerialPortInterface::Flush() {
	if(DRV_UART_Flush(static_cast<uart_bus_t>(_portId)) != DRV_OK) {
		return LLATypes::FAILURE;
	}

	return LLATypes::SUCCESS;
}

}

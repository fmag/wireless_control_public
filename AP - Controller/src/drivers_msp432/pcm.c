/*
 * pcm.c
 *
 *  Created on: Sep 22, 2016
 *      Author: singhh23
 */

#include "drivers_msp432/board_config.h"
#include "drivers_msp432/pcm.h"
#include "driverlib/MSP432P4xx/pcm.h"

drv_status_t DRV_PCM_Init(void) {

    // Multiple power modes are available - only AM (active mode) allows
    // for CPU usage (ie all other modes are sleep/shutdown and used for
    // power consumption optimization - ignored here).
    //
    // Within the AM, there are 2 different core voltage levels (VCORE0, VCORE1),
    // each offering different clock support - we wish to use the highest clock
    // rate (48MHz) and so require VCORE1 (see Table 7-1 of userguide for more details).
    //
    // Note also that transitions between power states must be valid - only
    // paths following Fig 7-2/7-3 of userguide are allowable, however the
    // driverlib takes care of this and so we can transition regardless.

	if(PCM_setCoreVoltageLevel(PCM_VCORE1) == false) {
		return DRV_ERROR;
	}

	return DRV_OK;
}

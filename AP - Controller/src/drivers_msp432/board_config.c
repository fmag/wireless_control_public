/*
 * board_config.c
 *
 *  Created on: May 17, 2016
 *      Author: singhh23
 */

#include "drivers_msp432/board_config.h"
#include "driverlib/MSP432P4xx/interrupt.h"
#include "driverlib/MSP432P4xx/timer32.h"
#include "driverlib/MSP432P4xx/gpio.h"
#include "driverlib/MSP432P4xx/uart.h"
#include "driverlib/MSP432P4xx/dma.h"
#include "driverlib/MSP432P4xx/spi.h"
#include "driverlib/MSP432P4xx/cs.h"

/******************** SPI BUSES *********************/

const eUSCI_SPI_MasterConfig spiDefaultBusConfigA =
{
		EUSCI_A_SPI_CLOCKSOURCE_SMCLK,             // SMCLK Clock Source
		48000000,                                  // SMCLK = HFXT = 48MHz
		500000,                                    // SPICLK = 500khz
		EUSCI_A_SPI_MSB_FIRST,                     // MSB First
		EUSCI_A_SPI_PHASE_DATA_CHANGED_ONFIRST_CAPTURED_ON_NEXT,    // Phase
		EUSCI_A_SPI_CLOCKPOLARITY_INACTIVITY_HIGH, // High polarity
		EUSCI_A_SPI_3PIN                           // 3Wire SPI Mode
};

const eUSCI_SPI_MasterConfig spiDefaultBusConfigB =
{
		EUSCI_B_SPI_CLOCKSOURCE_SMCLK,				// SMCLK Clock Source
		48000000,									// SMCLK = HFXT = 48MHz
		2000000,									// SPICLK = 2MHz
		EUSCI_B_SPI_MSB_FIRST,						// MSB First
		EUSCI_B_SPI_PHASE_DATA_CAPTURED_ONFIRST_CHANGED_ON_NEXT,	// Phase
		EUSCI_B_SPI_CLOCKPOLARITY_INACTIVITY_LOW,	// Low polarity
		EUSCI_B_SPI_3PIN							// 3Wire SPI Mode
};

const eUSCI_SPI_MasterConfig spiDefaultBusConfigC =
{
		EUSCI_B_SPI_CLOCKSOURCE_SMCLK,             // SMCLK Clock Source
		48000000,                                  // SMCLK = HFXT = 48MHz
		3000000,                                    // SPICLK = 3MHz
		EUSCI_B_SPI_MSB_FIRST,                     // MSB First
		EUSCI_B_SPI_PHASE_DATA_CHANGED_ONFIRST_CAPTURED_ON_NEXT,    // Phase
		EUSCI_B_SPI_CLOCKPOLARITY_INACTIVITY_HIGH, // High polarity
		EUSCI_B_SPI_3PIN                           // 3Wire SPI Mode
};

// THE ORDER OF THIS INITIALIZATION IS INTEGRAL TO INTERRUPT FUNCTIONALITY
const spi_bus_handle_t spiBus[SPI_NUM_BUSES] =
{
		// SPI B0 (bolt spi)
		{
				GPIO_PORT_P1,						// Port 1
				GPIO_PIN5 | GPIO_PIN6 | GPIO_PIN7,	// CLK/MOSI/MISO (3-wire)
				GPIO_PRIMARY_MODULE_FUNCTION,		// Primary mode (see datasheet table 6-32)
				&spiDefaultBusConfigC,				// Default config from above
				EUSCI_B_SPI_TRANSMIT_INTERRUPT,		// Transmit interrupt status register
				EUSCI_B_SPI_RECEIVE_INTERRUPT,		// Receive interrupt status register
				EUSCI_B0_BASE,						// Base address location
		},

		// SPI B2 (application spi)
		{
				GPIO_PORT_P3,						// Port 3
				GPIO_PIN5 | GPIO_PIN7 | GPIO_PIN6,	// CLK/MOSI/MISO (3-wire)
				GPIO_PRIMARY_MODULE_FUNCTION,		// Primary mode (see datasheet table 6-34)
				&spiDefaultBusConfigB,				// Default config from above
				EUSCI_B_SPI_TRANSMIT_INTERRUPT,		// Transmit interrupt status register
				EUSCI_B_SPI_RECEIVE_INTERRUPT,		// Receive interrupt status register
				EUSCI_B2_BASE,						// Base address location
		}

		// Add more buses here if necessary
};

/******************** UART BUSES *********************/
// Different configurations for different baudrates:
// The numbers for BRDIV, firstMod, secondMod were calculated using:
// http://software-dl.ti.com/msp430/msp430_public_sw/mcu/msp430/MSP430BaudRateConverter/index.html
// A conversion of the source code could be made from js to c if required:
// http://software-dl.ti.com/msp430/msp430_public_sw/mcu/msp430/MSP430BaudRateConverter/baudrate.js

const eUSCI_UART_Config uartBusConfig[NUM_BAUDRATES] =
{
		{		// 9600
			EUSCI_A_UART_CLOCKSOURCE_SMCLK,					// SMCLK Clock Source (= 48Mhz)
			312, 											// BRRDIV
			8,												// First Modulation
			0,												// Second Modulation
			EUSCI_A_UART_NO_PARITY,                  		// No Parity
			EUSCI_A_UART_LSB_FIRST,                  		// LSB First
			EUSCI_A_UART_ONE_STOP_BIT,               		// One stop bit
			EUSCI_A_UART_MODE,                       		// UART mode
			EUSCI_A_UART_OVERSAMPLING_BAUDRATE_GENERATION 	// Oversampling on
		},

		{		// 115200
			EUSCI_A_UART_CLOCKSOURCE_SMCLK,					// SMCLK Clock Source (= 48Mhz)
			26, 											// BRRDIV
			1,												// First Modulation
			0,												// Second Modulation
			EUSCI_A_UART_NO_PARITY,                  		// No Parity
			EUSCI_A_UART_LSB_FIRST,                  		// LSB First
			EUSCI_A_UART_ONE_STOP_BIT,               		// One stop bit
			EUSCI_A_UART_MODE,                       		// UART mode
			EUSCI_A_UART_OVERSAMPLING_BAUDRATE_GENERATION 	// Oversampling on
		},

		{		// 230400
			EUSCI_A_UART_CLOCKSOURCE_SMCLK,					// SMCLK Clock Source (= 48Mhz)
			13, 											// BRRDIV
			0,												// First Modulation
			0,												// Second Modulation
			EUSCI_A_UART_NO_PARITY,                  		// No Parity
			EUSCI_A_UART_LSB_FIRST,                  		// LSB First
			EUSCI_A_UART_ONE_STOP_BIT,               		// One stop bit
			EUSCI_A_UART_MODE,                       		// UART mode
			EUSCI_A_UART_OVERSAMPLING_BAUDRATE_GENERATION 	// Oversampling on
		},

		{		// 460800
			EUSCI_A_UART_CLOCKSOURCE_SMCLK,					// SMCLK Clock Source (= 48Mhz)
			6, 												// BRRDIV
			8,												// First Modulation
			0,												// Second Modulation
			EUSCI_A_UART_NO_PARITY,                  		// No Parity
			EUSCI_A_UART_LSB_FIRST,                  		// LSB First
			EUSCI_A_UART_ONE_STOP_BIT,               		// One stop bit
			EUSCI_A_UART_MODE,                       		// UART mode
			EUSCI_A_UART_OVERSAMPLING_BAUDRATE_GENERATION 	// Oversampling on
		}
};

// THE ORDER OF THIS INITIALIZATION IS INTEGRAL TO INTERRUPT FUNCTIONALITY
const uart_bus_handle_t uartBus[UART_NUM_BUSES] =
{
		// Debug bus (APP_UART_TXD/RXD)
		{
				GPIO_PORT_P1,						// Port 1
				GPIO_PIN2 | GPIO_PIN3,				// RX/TX
				GPIO_PRIMARY_MODULE_FUNCTION,		// Primary mode (see datasheet table 6-32)
				EUSCI_A_UART_TRANSMIT_INTERRUPT,	// Transmit interrupt status register
				EUSCI_A_UART_RECEIVE_INTERRUPT,		// Recieve interrupt status register
				EUSCI_A0_BASE,						// Base address location
		},

		// Application bus (APP_SENSOR_UART_TXD/RXD)
		{
				GPIO_PORT_P3,						// Port 3
				GPIO_PIN2 | GPIO_PIN3,				// RX/TX
				GPIO_PRIMARY_MODULE_FUNCTION,		// Primary mode (see datasheet table 6-34)
				EUSCI_A_UART_TRANSMIT_INTERRUPT,	// Transmit interrupt status register
				EUSCI_A_UART_RECEIVE_INTERRUPT,		// Recieve interrupt status register
				EUSCI_A2_BASE,						// Base address location
		}

		// Add more buses here if necessary
};

/******************** CS CONFIG **********************/
const cs_handle_t csConfig =
{
		GPIO_PORT_PJ,
		GPIO_PIN0 | GPIO_PIN1 | GPIO_PIN2 | GPIO_PIN3,		// LF & HF xtals
		GPIO_PRIMARY_MODULE_FUNCTION
};

/******************** T32 TIMERS *********************/

// THE ORDER OF THIS INITIALIZATION IS INTEGRAL TO INTERRUPT FUNCTIONALITY
t32_timer_handle_t t32Timer[T32_NUM_TIMERS] =
{
		{
				(Timer32_Type*)TIMER32_0_BASE,
				TIMER32_PRESCALER_1,
				TIMER32_32BIT,
				TIMER32_PERIODIC_MODE,
				NULL,
				NULL
		},

		{
		        (Timer32_Type*)TIMER32_1_BASE,
				TIMER32_PRESCALER_1,
				TIMER32_32BIT,
				TIMER32_FREE_RUN_MODE,
				NULL,
				NULL
		}
};

/******************* LS7366 DEVICE ******************/

// Map from device number to spi bus - both encoders are currently on the same bus (which is declared above)
const spi_bus_t LS7366_SPI_BUS[LS7366_NUM_DEV] = {SPI_BUS_1, SPI_BUS_1};

// THE ORDER OF THIS INITIALIZATION IS INTEGRAL TO FUNCTIONALITY
gpio_handle_t LS7366_GPIO[LS7366_NUM_DEV] =
{
		{
				GPIO_PORT_P3,		// port for the CS signal
				GPIO_PIN0,			// pin for the CS signal
				GPIO_TRISTATE		// irrelavent (will change automatically)
		},

		{
				GPIO_PORT_P3,
				GPIO_PIN1,
				GPIO_TRISTATE
		}
};

/******************* DAC8831 DEVICE ******************/

// Map from device number to spi bus - the motor is on the spi bus 2 (declared above)
const spi_bus_t DAC8831_SPI_BUS[DAC8831_NUM_DEV] = {SPI_BUS_1};

// THE ORDER OF THIS INITIALIZATION IS INTEGRAL TO FUNCTIONALITY
gpio_handle_t DAC8831_GPIO[DAC8831_NUM_DEV] =
{
		{
				GPIO_PORT_P5,		// port for the CS signal
				GPIO_PIN0,			// pin for the CS signal
				GPIO_TRISTATE		// irrelavent (will change automatically)
		}
};

// location of dac enable signal (order should match CS above)
gpio_handle_t DAC8831_EN[DAC8831_NUM_DEV] =
{
		{
				GPIO_PORT_P5,
				GPIO_PIN1,
				GPIO_TRISTATE
		}
};

/******************* LED DEVICE ******************/

// THE ORDER OF THIS INITIALIZATION IS INTEGRAL TO FUNCTIONALITY
gpio_handle_t LED_GPIO[LED_NUM_DEV] =
{
		{
				GPIO_PORT_P2,		// port for the LED signal
				GPIO_PIN0,			// pin for the LED signal
				GPIO_TRISTATE		// irrelavent (will change automatically)
		}
};

/******************* PUSHBUTTON DEVICE ******************/

// THE ORDER OF THIS INITIALIZATION IS INTEGRAL TO FUNCTIONALITY
gpio_handle_t PB_GPIO[PB_NUM_DEV] =
{
		{
				GPIO_PORT_P2,		// port for the PB signal
				GPIO_PIN5,			// pin for the PB signal
				GPIO_TRISTATE		// irrelavent (will change automatically)
		},

		{
				GPIO_PORT_P2,		// port for the PB signal
				GPIO_PIN6,			// pin for the PB signal
				GPIO_TRISTATE		// irrelavent (will change automatically)
		}
};

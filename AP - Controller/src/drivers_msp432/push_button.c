/*
 * push_button.c
 *
 *  Created on: Oct 14, 2016
 *      Author: singhh23
 */

#include "math.h"
#include "drivers_msp432/board_config.h"
#include "drivers_msp432/push_button.h"
#include "drivers_msp432/interrupts.h"
#include "driverlib/MSP432P4xx/gpio.h"

#define _PB_IS_INITIALIZED() {\
	if(pbInitialized == 0) {\
		return DRV_ERROR;\
	}\
}

static uint8_t pbInitialized = 0;
drv_pb_port_handle_t portCallbacks[PB_NUM_PORTS];

drv_status_t DRV_PB_Init() {
	int i, j;

	if(pbInitialized == 0) {
		for(i = 0; i < PB_NUM_PORTS; i++) {
			for(j = 0; j < PB_NUM_PINS; j++) {
				portCallbacks[i].classObj[j] = NULL;
				portCallbacks[i].callbackArray[j] = NULL;
			}
		}

		pbInitialized = 1;
	}

	return DRV_OK;
}

drv_status_t DRV_PB_RegisterInterrupt(drv_pb_device_t buttonNum,
		void *classObj, void(*fun)(void *obj)) {
	_PB_IS_INITIALIZED();

	if(PB_GPIO[buttonNum].port != GPIO_PORT_P1 &&
			PB_GPIO[buttonNum].port != GPIO_PORT_P2) {
		return DRV_ERROR;
	}

	uint8_t port_idx = PB_GPIO[buttonNum].port - 1;
	uint8_t pin_idx = log2(PB_GPIO[buttonNum].pin);

	portCallbacks[port_idx].classObj[pin_idx] = classObj;
	portCallbacks[port_idx].callbackArray[pin_idx] = fun;

	GPIO_registerInterrupt(PB_GPIO[buttonNum].port, PB_IRQHandler);

	return DRV_OK;

}

drv_status_t DRV_PB_EnableInterrupt(drv_pb_device_t buttonNum) {
	_PB_IS_INITIALIZED();

	uint8_t port_idx = PB_GPIO[buttonNum].port - 1;
	uint8_t pin_idx = log2(PB_GPIO[buttonNum].pin);

	if(portCallbacks[port_idx].classObj[pin_idx] == NULL ||
			portCallbacks[port_idx].callbackArray[pin_idx] == NULL) {
		return DRV_ERROR;
	}

	GPIO_interruptEdgeSelect(PB_GPIO[buttonNum].port, PB_GPIO[buttonNum].pin, GPIO_LOW_TO_HIGH_TRANSITION);
	GPIO_clearInterruptFlag(PB_GPIO[buttonNum].port, PB_GPIO[buttonNum].pin);
	GPIO_enableInterrupt(PB_GPIO[buttonNum].port, PB_GPIO[buttonNum].pin);

	return DRV_OK;
}

/*
 * led.c
 *
 *  Created on: Aug 11, 2016
 *      Author: singhh23
 */

#include "drivers_msp432/led.h"
#include "drivers_msp432/gpio.h"
#include "drivers_msp432/board_config.h"

drv_status_t DRV_LED_Init(drv_led_device_t devNum) {

	// Start with LED off
	DRV_GPIO_SetAsOutput(&LED_GPIO[devNum]);
	DRV_GPIO_SetLow(&LED_GPIO[devNum]);

	return DRV_OK;
}

drv_status_t DRV_LED_TurnOn(drv_led_device_t devNum) {
	if(DRV_GPIO_SetHigh(&LED_GPIO[devNum]) != DRV_OK) {
		return DRV_ERROR;
	}

	return DRV_OK;
}

drv_status_t DRV_LED_TurnOff(drv_led_device_t devNum) {
	if(DRV_GPIO_SetLow(&LED_GPIO[devNum]) != DRV_OK) {
		return DRV_ERROR;
	}

	return DRV_OK;
}


/*
 * spi.cpp
 *
 *  Created on: May 10, 2016
 *      Author: singhh23
 */

#include "drivers_msp432/spi.h"
#include "driverlib/MSP432P4xx/gpio.h"

#define _SPI_IS_INITIALIZED() {\
	if(spiInitialized == 0) {\
		return DRV_ERROR;\
	}\
}

static uint8_t spiInitialized = 0;
spi_handle_t spiHandle[SPI_NUM_BUSES];

drv_status_t DRV_SPI_Init() {

	// Initialize the number of buses we have with the pre-set configurations
	if(!spiInitialized) {

		uint8_t i;

		// start every bus with the related parameters
		for(i = 0; i < SPI_NUM_BUSES; i++) {
			// setup pins to SPI function
			GPIO_setAsPeripheralModuleFunctionInputPin(spiBus[i].port, spiBus[i].pinCombination, spiBus[i].functionMode);

			// start spi with proper configuration (master mode)
			if(!SPI_initMaster(spiBus[i].baseAddr, spiBus[i].busConfiguration)) {
				return DRV_ERROR;
			}

			spiHandle[i].busHandle = spiBus[i];
			spiHandle[i].lengthRemaining = 0;
			spiHandle[i].txPointer = NULL;
			spiHandle[i].rxPointer = NULL;

			// enable the module
			SPI_enableModule(spiBus[i].baseAddr);
		}

		spiInitialized = 1;
	}

	return DRV_OK;
}

drv_status_t DRV_SPI_Start(spi_bus_t busId) {
	_SPI_IS_INITIALIZED();

	return DRV_OK;
}

drv_status_t DRV_SPI_WriteRead(spi_bus_t busId, volatile uint8_t byte, volatile uint8_t* ret) {
	_SPI_IS_INITIALIZED();

	if(DRV_SPI_CheckStatus(busId) != DRV_OK) {
		return DRV_BUSY;
	}

	// setup pointers for interrupt handling
	spiHandle[busId].lengthRemaining = 1;
	spiHandle[busId].txPointer = &byte;
	spiHandle[busId].rxPointer = ret;

	// enable receive interrupts
	SPI_enableInterrupt(spiHandle[busId].busHandle.baseAddr, spiHandle[busId].busHandle.rxIntAddr);
	// wait for tx buffer ready
	while(!(SPI_getInterruptStatus(spiHandle[busId].busHandle.baseAddr, spiHandle[busId].busHandle.txIntAddr)));
	// transmit first (and only) byte
	SPI_transmitData(spiHandle[busId].busHandle.baseAddr, *spiHandle[busId].txPointer);

	// wait for receive interrupt to be handled
	while(DRV_SPI_CheckStatus(busId) != DRV_OK);

	return DRV_OK;
}

drv_status_t DRV_SPI_WriteReadBlock(spi_bus_t busId, volatile uint8_t* txData,
		volatile uint8_t* rxData, uint16_t length) {
	_SPI_IS_INITIALIZED();

	if(DRV_SPI_CheckStatus(busId) != DRV_OK) {
		return DRV_BUSY;
	}

	// setup pointers for interrupt handling
	spiHandle[busId].lengthRemaining = length;
	spiHandle[busId].txPointer = txData;
	spiHandle[busId].rxPointer = rxData;

	// enable receive interrupts
	SPI_enableInterrupt(spiHandle[busId].busHandle.baseAddr, spiHandle[busId].busHandle.rxIntAddr);
	// wait for tx buffer ready
	while(!(SPI_getInterruptStatus(spiHandle[busId].busHandle.baseAddr, spiHandle[busId].busHandle.txIntAddr)));
	// transmit first byte
	SPI_transmitData(spiHandle[busId].busHandle.baseAddr, *spiHandle[busId].txPointer);

	return DRV_OK;
}

drv_status_t DRV_SPI_CheckStatus(spi_bus_t busId) {
	_SPI_IS_INITIALIZED();

	// check if spi is busy
	if(EUSCI_B_SPI_isBusy(spiHandle[busId].busHandle.baseAddr)) {
		return DRV_BUSY;
	}

	if(spiHandle[busId].lengthRemaining != 0) {
		return DRV_BUSY;
	}

	return DRV_OK;
}

/*
 * Copyright (c) 2015, Swiss Federal Institute of Technology (ETH Zurich).
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Author:  Reto Da Forno
 */

/*
 * I N C L U D E S
 */

#include "drivers_msp432/board_config.h"
#include "drivers_msp432/bolt.h"
#include "drivers_msp432/uart.h"
#include "drivers_msp432/spi.h"

/*
 * T Y P E D E F S   &   G L O B A L S
 */

typedef enum
{
    BOLT_STATE_IDLE = 0,
    BOLT_STATE_READ,
    BOLT_STATE_WRITE,
    NUM_OF_STATES
} bolt_state_t;

static volatile bolt_state_t bolt_state = BOLT_STATE_IDLE;


/*
 * H E L P E R   M A C R O S
 */

#define BOLT_SPI_INIT
#define BOLT_SPI_WAIT           SPI_WAIT_BUSY(B0)  // -> disabled, caused problems (would lock up occasionally)
#define BOLT_SPI_CLR_RX
#define BOLT_SPI_DISABLE
#define BOLT_SPI_ENABLE
#define BOLT_SPI_IS_ENABLED     SPI_IS_ENABLED(B0)
#define BOLT_SPI_TRANSMIT(b)    DRV_SPI_WriteRead(BOLT_SPI, b, NULL)
#define BOLT_SPI_RECEIVE(b)     DRV_SPI_WriteRead(BOLT_SPI, 0x0, b)


/*
 * F U N C T I O N S
 */

/* init the GPIOs + SPI and check if BOLT is ready */
uint8_t bolt_init(void)
{
    // control signals
    PIN_CFG_IN(BOLT_IND_PIN);
    //PIN_CFG_INT(BOLT_IND_PIN, 1);
    PIN_CLR(BOLT_MODE_PIN);
    PIN_CFG_OUT(BOLT_MODE_PIN);
    PIN_CLR(BOLT_REQ_PIN);
    PIN_CFG_OUT(BOLT_REQ_PIN);
    PIN_CFG_IN(BOLT_ACK_PIN);
    PIN_CFG_IN(BOLT_IND_OUT_PIN);
    PIN_CFG_OUT(BOLT_FUTURE_USE_PIN);
    // the time request pin is an output on the application processor
//    PIN_CLR(BOLT_TIMEREQ_PIN);
//    PIN_CFG_OUT(BOLT_TIMEREQ_PIN);
    PIN_CFG_INT(BOLT_TIMEREQ_PIN, 1);
#if BOLT_USE_DMA
    PIN_CFG_PORT_INT(BOLT_ACK_PIN);
    PIN_IES_FALLING(BOLT_ACK_PIN);
#endif /* BOLT_USE_DMA */

    BOLT_SPI_INIT;

    return bolt_status();
}


void bolt_release(void)
{
    if (BOLT_SPI_IS_ENABLED)
    {
        /* --- wait for BUSY flag --- */
        BOLT_SPI_WAIT;
        /* --- empty the RX buffer --- */
        BOLT_SPI_CLR_RX;
        BOLT_SPI_DISABLE;
    }
    /* --- set REQ = 0 --- */
    PIN_CLR(BOLT_REQ_PIN);

	/* --- wait for ACK to go down --- */
	while (PIN_GET(BOLT_ACK_PIN));
	bolt_state = BOLT_STATE_IDLE;
}


/* returns 1 if BOLT is active/ready (= responds to a write request), 0 otherwise */
uint8_t bolt_status(void)
{
    if (bolt_acquire(BOLT_OP_WRITE))    // check BOLT state
    {
        bolt_release();
        return 1;
    }
    return 0;
}


/* clear the bolt queue (read and drop all messages) */
void bolt_flush(void)
{
    uint8_t buffer[BOLT_MAX_MSG_LEN];
    while (BOLT_DATA_AVAILABLE)
    {
        if (bolt_acquire(BOLT_OP_READ))
        {
            bolt_start(buffer, 0);
            bolt_release();
        }
    }
}


uint8_t bolt_acquire(bolt_op_mode_t mode)
{
    if (PIN_GET(BOLT_REQ_PIN) || PIN_GET(BOLT_ACK_PIN))
    {
        //uart_println("bolt_acquire: REQ or ACK line is high\r\n");
        return 0;
    }
    if (BOLT_STATE_IDLE != bolt_state)
    {
        //uart_println("bolt_acquire: not in idle state, operation skipped\r\n");
        return 0;
    }

    // --- MODE ---
    // READ
    if (BOLT_OP_READ == mode)
    {
        if(!BOLT_DATA_AVAILABLE)
        {
            //uart_println("bolt_acquire: no data available, read operation skipped\r\n");
            return 0;
        }
        PIN_CLR(BOLT_MODE_PIN); /* 0 = READ */
    // WRITE
    } else
    {
      PIN_SET(BOLT_MODE_PIN); /* 1 = WRITE */
    }

    /* --- set REQ = 1 --- */
    PIN_SET(BOLT_REQ_PIN);

    // now wait for a rising edge on the ACK line (max. 100us)
    uint8_t cnt = 0;
    do
    {
        __delay_cycles(MCLK_SPEED / 100000);       /* wait 10 us */
        cnt++;
    } while (!PIN_GET(BOLT_ACK_PIN) && cnt < 10);

    if (!PIN_GET(BOLT_ACK_PIN))
    {
        /* ack is still low -> failed */
        bolt_state = BOLT_STATE_IDLE;
        PIN_CLR(BOLT_REQ_PIN);
        //uart_println("bolt_acquire: access denied\r\n");
        return 0;
    }
    bolt_state = (mode == BOLT_OP_READ) ? BOLT_STATE_READ : BOLT_STATE_WRITE;

    /* make sure SPI is enabled */
    BOLT_SPI_ENABLE;

    return 1;
}


uint8_t bolt_start(uint8_t *data, uint16_t num_bytes)
{
#if !(BOLT_USE_DMA)
    uint16_t count = 0;
#endif /* BOLT_USE_DMA */

    if (!data)
    {
        //uart_println("bolt_start: invalid parameter\r\n");
        return 0;
    }
    ////uart_println("starting data transfer... ");

    // WRITE OPERATION
    if (BOLT_STATE_WRITE == bolt_state)
    {
        if (0 == num_bytes)
        {
            //uart_println("bolt_start: invalid parameter num_bytes\r\n");
            return 0;
        }
#if BOLT_USE_DMA
        dma_config_spi(BOLT_SPI, bolt_release);
        dma_start(0, (uint16_t)data, num_bytes);
#else
        while (count < num_bytes)
        {
            BOLT_SPI_TRANSMIT(*data);
            data++;
            count++;
            if (!PIN_GET(BOLT_ACK_PIN))
            {
                //uart_println("bolt_start: transfer aborted\r\n");
                return 0;
            }
        }
        ////uart_println("%d bytes transmitted", count);
#endif /* BOLT_USE_DMA */
        return 1;

    // READ OPERATION
    } else if (BOLT_STATE_READ == bolt_state)
    {
#if BOLT_USE_DMA
        dma_config_spi(bolt_release);
        dma_start((uint16_t)data, 0, BOLT_MAX_MSG_LEN);
#else
        // first, clear the RX buffer
        BOLT_SPI_CLR_RX;
#if SPI_FAST_READ
        /* transmit 1 byte ahead for faster read speed (fills RXBUF faster) */
        SPI_TRANSMIT_BYTE(0x00);
#endif
        while ((count < BOLT_MAX_MSG_LEN) && PIN_GET(BOLT_ACK_PIN))
        {
          BOLT_SPI_RECEIVE(data);
          data++;
          count++;
        }
        ////uart_println("%d bytes received", count);
#endif /* BOLT_USE_DMA */
        return count;
    }
    return 0;
}

/*
 * uart.c
 *
 *  Created on: Jul 25, 2016
 *      Author: singhh23
 */

#include "drivers_msp432/uart.h"
#include "driverlib/MSP432P4xx/gpio.h"
#include "driverlib/MSP432P4xx/interrupt.h"
#include "driverlib/MSP432P4xx/uart.h"
#include "drivers_msp432/bolt.h"
#include "drivers_msp432/spi.h"
#include "string.h"

// CIRCULAR BUFFER HELPER FUNCTIONS
int cb_rx_flush(circular_buffer_rx_t *cb) {
	Interrupt_disableMaster();

	cb->tail = cb->head;
	cb->size = 0;

	Interrupt_enableMaster();

	return 0;
}

int cb_rx_pop(circular_buffer_rx_t *cb, volatile uint8_t *byte, uint16_t maxSize) {
	Interrupt_disableMaster();

	// is there any data in the buffer?
	if (cb->size == 0) {
		Interrupt_enableMaster();
		return -1;
	}

	// if so, take one byte out
	*byte = *cb->head;

	// make array smaller (incr head with wraparound)
	if (cb->head == &cb->data[maxSize - 1]) {
		cb->head = &cb->data[0];
	} else {
		cb->head++;
	}

	cb->size--;

	Interrupt_enableMaster();

	return 0;
}
int cb_tx_pop(circular_buffer_tx_t *cb, volatile uint8_t *byte, uint16_t maxSize) {

	// is there any data in the buffer?
	if (cb->size == 0) { return -1; }

	// if so, take one byte out
	*byte = *cb->head;

	// make array smaller (incr head with wraparound)
	if (cb->head == &cb->data[maxSize - 1]) {
		cb->head = &cb->data[0];
	} else {
		cb->head++;
	}

	cb->size--;

	return 0;
}

int cb_rx_push(circular_buffer_rx_t *cb, uint8_t byte, uint16_t maxSize) {

	// is there room to add data?
	if (cb->size == maxSize) { return -1; }

	// add data to tail
	*cb->tail = byte;

	// make array larger (incr tail with wraparound)
	if (cb->tail == &cb->data[maxSize - 1]) {
		cb->tail = &cb->data[0];
	} else {
		cb->tail++;
	}

	cb->size++;

	return 0;
}
int cb_tx_push(circular_buffer_tx_t *cb, uint8_t byte, uint16_t maxSize) {
	Interrupt_disableMaster();

	// is there room to add data?
	if (cb->size == maxSize) {
		Interrupt_enableMaster();
		return -1;
	}

	// add data to tail
	*cb->tail = byte;

	// make array larger (incr tail with wraparound)
	if (cb->tail == &cb->data[maxSize - 1]) {
		cb->tail = &cb->data[0];
	} else {
		cb->tail++;
	}

	cb->size++;

	Interrupt_enableMaster();

	return 0;
}

#define _UART_IS_INITIALIZED()                                                                     \
	{                                                                                              \
		if (uartInitialized[busId] == 0) { return DRV_ERROR; }                                     \
	}

static uint8_t uartInitialized[UART_NUM_BUSES] = {0};
extern const eUSCI_UART_Config uartBusConfig[NUM_BAUDRATES];
uart_handle_t uartHandle[UART_NUM_BUSES];

drv_status_t DRV_UART_Init(uart_bus_t busId, drv_baud_rate_t baudRate) {

	if (!uartInitialized[busId]) {

		// initialize with given baudrate
		GPIO_setAsPeripheralModuleFunctionInputPin(
		    uartBus[busId].port, uartBus[busId].pinCombination, uartBus[busId].functionMode);
		if (!UART_initModule(uartBus[busId].baseAddr, &uartBusConfig[baudRate])) {
			return DRV_ERROR;
		}

		// set-up default references defined in board_config
		uartHandle[busId].busHandle = uartBus[busId];

		// initialize buffers
		uartHandle[busId].rxBuffer.head = &uartHandle[busId].rxBuffer.data[0];
		uartHandle[busId].rxBuffer.tail = &uartHandle[busId].rxBuffer.data[0];
		uartHandle[busId].rxBuffer.size = 0;

		uartHandle[busId].txBuffer.head = &uartHandle[busId].txBuffer.data[0];
		uartHandle[busId].txBuffer.tail = &uartHandle[busId].txBuffer.data[0];
		uartHandle[busId].txBuffer.size = 0;

		uartHandle[busId].busy = false;

		// enable the module
		UART_enableModule(uartBus[busId].baseAddr);

		uartInitialized[busId] = 1;
	}

	return DRV_OK;
}

drv_status_t DRV_UART_Start(uart_bus_t busId) {
	_UART_IS_INITIALIZED();

	// start transceiving data through interrupts
	UART_enableInterrupt(uartHandle[busId].busHandle.baseAddr,
	                     uartHandle[busId].busHandle.txIntAddr);
	UART_enableInterrupt(uartHandle[busId].busHandle.baseAddr,
	                     uartHandle[busId].busHandle.rxIntAddr);

	return DRV_OK;
}

drv_status_t DRV_UART_Write(uart_bus_t busId, uint8_t *data, const uint16_t size) {
	_UART_IS_INITIALIZED();

	// ensure we aren't already writting data
	// TODO: This prevents any data to be added to the print queue while sth. is currently being printed.
	if (uartHandle[busId].busy) { return DRV_BUSY; }

	// is there enough room to send this data
	if (size > _DRV_UART_TXBUFFER_SIZE - uartHandle[busId].txBuffer.size) { return DRV_ERROR; }

	// first copy data to tx buffer
	// FIXME this could be skipped for optimization - if so, user must preserve
	// buffer state until tx operation ends
	uint8_t i = 1;
	static uint8_t byte;
	byte = data[0];
	for (; i < size; i++) {
		if (cb_tx_push(&uartHandle[busId].txBuffer, data[i], _DRV_UART_TXBUFFER_SIZE) == -1) {
			return DRV_ERROR;
		}
	}

	// begin transmit byte-by-byte
	uartHandle[busId].busy = true;
	UART_transmitData(uartHandle[busId].busHandle.baseAddr, byte);

	return DRV_OK;
}

drv_status_t DRV_UART_GetByte(uart_bus_t busId, uint8_t *byte) {
	_UART_IS_INITIALIZED();

	// if the buffer is full, notify user of overflow
	if (uartHandle[busId].rxBuffer.size == _DRV_UART_RXBUFFER_SIZE) {
		// pop a single byte before notifying user
		if (cb_rx_pop(&uartHandle[busId].rxBuffer, byte, _DRV_UART_RXBUFFER_SIZE) == -1) {
			return DRV_ERROR;
		}

		return DRV_BUSY;
	}

	// otherwise, pop and return
	if (cb_rx_pop(&uartHandle[busId].rxBuffer, byte, _DRV_UART_RXBUFFER_SIZE) == -1) {
		return DRV_ERROR;
	}

	return DRV_OK;
}

uint16_t DRV_UART_GetRxAmount(uart_bus_t busId) {
	_UART_IS_INITIALIZED();

	return uartHandle[busId].rxBuffer.size;
}

uint16_t DRV_UART_GetTxAmount(uart_bus_t busId) {
	_UART_IS_INITIALIZED();

	if (uartHandle[busId].txBuffer.size > 0) { return uartHandle[busId].txBuffer.size; }

	return 0;
}

drv_status_t DRV_UART_Flush(uart_bus_t busId) {
	cb_rx_flush(&uartHandle[busId].rxBuffer);

	return DRV_OK;
}

void uart_println(uint8_t *buffer) {
	DRV_UART_Init(BOLT_UART, BOLT_BAUDRATE); // nop if already init
	DRV_UART_Start(BOLT_UART);
	DRV_UART_Write(BOLT_UART, buffer, strlen((const char *)buffer));

	return;
}

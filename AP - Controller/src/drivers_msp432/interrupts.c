/*
 * interrupts.c
 *
 *  Created on: May 11, 2016
 *      Author: singhh23
 */

#include "math.h"
#include "drivers_msp432/interrupts.h"
#include "drivers_msp432/spi.h"
#include "drivers_msp432/uart.h"
#include "drivers_msp432/push_button.h"
#include "driverlib/MSP432P4xx/interrupt.h"
#include "driverlib/MSP432P4xx/timer32.h"
#include "driverlib/MSP432P4xx/uart.h"
#include "driverlib/MSP432P4xx/gpio.h"
#include "drivers_msp432/msp432driver.h"

extern spi_handle_t spiHandle[SPI_NUM_BUSES];
extern uart_handle_t uartHandle[UART_NUM_BUSES];

void InterruptsInit(void) {
	// this is partly done in the startup_msp432p401r_ccs.c file
	// dma settings are handled directly in dma.c

	// set priorities (only upper 3 bits in 8-bit priority are inspected)

	Interrupt_setPriority(INT_EUSCIA3, 	0b001 << 5);	// SPI
	Interrupt_setPriority(INT_EUSCIB2, 	0b001 << 5);	// SPI
	Interrupt_setPriority(INT_EUSCIB0, 	0b001 << 5);	// SPI
	Interrupt_setPriority(INT_EUSCIA0, 	0b010 << 5);	// UART
	Interrupt_setPriority(INT_EUSCIA2, 	0b010 << 5);	// UART
	Interrupt_setPriority(INT_T32_INT1, 0b011 << 5);	// T32 1 (fasttick)
	Interrupt_setPriority(INT_T32_INT2, 0b100 << 5);	// T32 2 (slowtick)
//	Interrupt_setPriority(INT_PORT4, 0b101 << 5);	// SYNC-LINE INT

	// enable required interrupts
	Interrupt_enableInterrupt(INT_EUSCIA3);		// SPI
	Interrupt_enableInterrupt(INT_EUSCIB2);		// SPI
	Interrupt_enableInterrupt(INT_EUSCIB0);		// SPI
	Interrupt_enableInterrupt(INT_EUSCIA0);		// UART
	Interrupt_enableInterrupt(INT_EUSCIA2);		// UART
    Interrupt_enableInterrupt(INT_T32_INT1);	// T32 1
    Interrupt_enableInterrupt(INT_T32_INT2);	// T32 2


    Interrupt_enableInterrupt(INT_PORT4);	// SYNC-LINE INT

    Interrupt_enableMaster();					// Controller-Processer gate
}

// SYNC-Line ISR is empty here since we redirect to another function in ControlManager.
void PORT4_IRQHandler(void) {}

// UART HANDLER - DEBUG
void EUSCIA0_IRQHandler(void) {
	// get cause of interrupt and clear flag
	uint32_t status = UART_getEnabledInterruptStatus(uartHandle[UART_BUS_0].busHandle.baseAddr);
	UART_clearInterruptFlag(uartHandle[UART_BUS_0].busHandle.baseAddr, status);

	// if interrupt caused by rx, store byte read
	if(status & uartHandle[UART_BUS_0].busHandle.rxIntAddr) {
		cb_rx_push(&uartHandle[UART_BUS_0].rxBuffer, UART_receiveData(uartHandle[UART_BUS_0].busHandle.baseAddr), _DRV_UART_RXBUFFER_SIZE);
	}

	// if interrupt caused by tx, queue next byte transmission
	if(status & uartHandle[UART_BUS_0].busHandle.txIntAddr && uartHandle[UART_BUS_0].txBuffer.size > 0) {
		static volatile uint8_t byte;
		cb_tx_pop(&uartHandle[UART_BUS_0].txBuffer, &byte, _DRV_UART_TXBUFFER_SIZE);
		UART_transmitData(uartHandle[UART_BUS_0].busHandle.baseAddr, byte);
		return;
	}

	uartHandle[UART_BUS_0].busy = false;

	return;
}

// UART HANDLER - APP
void EUSCIA2_IRQHandler(void) {

	// get cause of interrupt and clear flag
	uint32_t status = UART_getEnabledInterruptStatus(uartHandle[UART_BUS_1].busHandle.baseAddr);
	UART_clearInterruptFlag(uartHandle[UART_BUS_1].busHandle.baseAddr, status);

	// if interrupt caused by rx, store byte read
	if(status & uartHandle[UART_BUS_1].busHandle.rxIntAddr) {
		cb_rx_push(&uartHandle[UART_BUS_1].rxBuffer, UART_receiveData(uartHandle[UART_BUS_1].busHandle.baseAddr), _DRV_UART_RXBUFFER_SIZE);
	}

	// if interrupt caused by tx, queue next byte transmission
	if(status & uartHandle[UART_BUS_1].busHandle.txIntAddr && uartHandle[UART_BUS_1].txBuffer.size > 0) {
		static volatile uint8_t byte;
		cb_tx_pop(&uartHandle[UART_BUS_1].txBuffer, &byte, _DRV_UART_TXBUFFER_SIZE);
		UART_transmitData(uartHandle[UART_BUS_1].busHandle.baseAddr, byte);
		return;
	}

	uartHandle[UART_BUS_1].busy = false;

	return;
}

// SPI HANDLER - BOLT
void EUSCIB0_IRQHandler(void) {

	// get cause of interrupt and clear the flag
	uint32_t status = SPI_getEnabledInterruptStatus(spiHandle[SPI_BUS_0].busHandle.baseAddr);
	SPI_clearInterruptFlag(spiHandle[SPI_BUS_0].busHandle.baseAddr, status);

	// if interrupt caused by read, store byte read and transmit next byte (if pending)
	if(status & spiHandle[SPI_BUS_0].busHandle.rxIntAddr) {

		// if rx buffer exists, store new byte and increment buffer
		if(spiHandle[SPI_BUS_0].rxPointer != NULL) {
			*spiHandle[SPI_BUS_0].rxPointer = SPI_receiveData(spiHandle[SPI_BUS_0].busHandle.baseAddr);
			spiHandle[SPI_BUS_0].rxPointer++;
		}

		// decrement length
		spiHandle[SPI_BUS_0].lengthRemaining--;

		// if user expects more data to be sent/received
		if(spiHandle[SPI_BUS_0].lengthRemaining > 0) {

			// if tx buffer doesn't exist, exit
			if(spiHandle[SPI_BUS_0].txPointer == NULL) {
				return;
			}

			// wait until tx buffer ready
			while(!(SPI_getInterruptStatus(
					spiHandle[SPI_BUS_0].busHandle.baseAddr, spiHandle[SPI_BUS_0].busHandle.txIntAddr)));
			// transmit next byte
			spiHandle[SPI_BUS_0].txPointer++;
			SPI_transmitData(spiHandle[SPI_BUS_0].busHandle.baseAddr, *spiHandle[SPI_BUS_0].txPointer);
		} else {

			// otherwise, operation finished - reset pointers and disable interrupts
			spiHandle[SPI_BUS_0].rxPointer = NULL;
			spiHandle[SPI_BUS_0].txPointer = NULL;
			SPI_disableInterrupt(spiHandle[SPI_BUS_0].busHandle.baseAddr, spiHandle[SPI_BUS_0].busHandle.rxIntAddr);
		}

	}

	return;
}

// SPI HANDLER - APP
void EUSCIB2_IRQHandler(void) {

	// get cause of interrupt and clear the flag
	uint32_t status = SPI_getEnabledInterruptStatus(spiHandle[SPI_BUS_1].busHandle.baseAddr);
	SPI_clearInterruptFlag(spiHandle[SPI_BUS_1].busHandle.baseAddr, status);

	// if interrupt caused by read, store byte read and transmit next byte (if pending)
	if(status & spiHandle[SPI_BUS_1].busHandle.rxIntAddr) {

		// if rx buffer exists, store new byte and increment buffer
		if(spiHandle[SPI_BUS_1].rxPointer != NULL) {
			*spiHandle[SPI_BUS_1].rxPointer = SPI_receiveData(spiHandle[SPI_BUS_1].busHandle.baseAddr);
			spiHandle[SPI_BUS_1].rxPointer++;
		}

		// decrement length
		spiHandle[SPI_BUS_1].lengthRemaining--;

		// if user expects more data to be sent/received
		if(spiHandle[SPI_BUS_1].lengthRemaining > 0) {

			// if tx buffer doesn't exist, exit
			if(spiHandle[SPI_BUS_1].txPointer == NULL) {
				return;
			}

			// wait until tx buffer ready
			while(!(SPI_getInterruptStatus(
					spiHandle[SPI_BUS_1].busHandle.baseAddr, spiHandle[SPI_BUS_1].busHandle.txIntAddr)));
			// transmit next byte
			spiHandle[SPI_BUS_1].txPointer++;
			SPI_transmitData(spiHandle[SPI_BUS_1].busHandle.baseAddr, *spiHandle[SPI_BUS_1].txPointer);
		} else {

			// otherwise, operation finished - reset pointers and disable interrupts
			spiHandle[SPI_BUS_1].rxPointer = NULL;
			spiHandle[SPI_BUS_1].txPointer = NULL;
			SPI_disableInterrupt(spiHandle[SPI_BUS_1].busHandle.baseAddr, spiHandle[SPI_BUS_1].busHandle.rxIntAddr);
		}

	}

	return;
}

void T32_INT1_IRQHandler(void) {

	Timer32_clearInterruptFlag((uint32_t)t32Timer[T32_0].baseAddr);

	if(t32Timer[T32_0].timerCallback != NULL) {
		t32Timer[T32_0].timerCallback(t32Timer[T32_0].usrObj);
	}

	return;
}

void T32_INT2_IRQHandler(void) {

	Timer32_clearInterruptFlag((uint32_t)t32Timer[T32_1].baseAddr);

	if(t32Timer[T32_1].timerCallback != NULL) {
		t32Timer[T32_1].timerCallback(t32Timer[T32_1].usrObj);
	}

	return;
}

void PB_IRQHandler(void) {
	// loop through all interrupt sources and call correct callbacks

	uint8_t i, j;

	// note that this can run multiple times (inefficient) over the same port if
	// multiple push buttons are assigned to the same port...
	for(i = 0; i < PB_NUM_DEV; i++) {
		uint32_t status = GPIO_getInterruptStatus(PB_GPIO[i].port, PIN_ALL8);
		GPIO_clearInterruptFlag(PB_GPIO[i].port, PIN_ALL8);

		for(j = 0; j < PB_NUM_DEV; j++) {

			if((status & PB_GPIO[j].pin) != 0) {
				uint8_t port_idx = PB_GPIO[j].port - 1;
				uint8_t pin_idx = log2(PB_GPIO[j].pin);

				if(portCallbacks[port_idx].classObj[pin_idx] == NULL ||
						portCallbacks[port_idx].callbackArray[pin_idx] == NULL) {
					continue;
				}

				portCallbacks[port_idx].callbackArray[pin_idx](portCallbacks[port_idx].classObj[pin_idx]);
			}

		}
	}

}

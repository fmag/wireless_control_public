/*
 * cs.c
 *
 *  Created on: Jun 17, 2016
 *      Author: singhh23
 */

#include "drivers_msp432/board_config.h"
#include "drivers_msp432/cs.h"
#include "driverlib/MSP432P4xx/cs.h"
#include "driverlib/MSP432P4xx/gpio.h"
#include "driverlib/MSP432P4xx/flash.h"

drv_status_t DRV_CS_Init(void) {

	// Set wait states to allow MCLK to operate at 48MHz
	FlashCtl_setWaitState(FLASH_BANK0, 1);
	FlashCtl_setWaitState(FLASH_BANK1, 1);

	// Setup gpio for external xtal
    GPIO_setAsPeripheralModuleFunctionOutputPin(csConfig.port, csConfig.pinCombination, csConfig.functionMode);

	// Setup references for user API (LF 32kHz, HF 48MHz)
    CS_setReferenceOscillatorFrequency(CS_REFO_32KHZ);
    CS_setExternalClockSourceFrequency(LFXTCLK_SPEED, HFXTCLK_SPEED);

	// Internal Digital Clock setup
	CS_setDCOFrequency(DCOCLK_SPEED);

	// Start LF XT (32KHz) without bypass mode
	CS_startLFXT(CS_LFXT_DRIVE0);

	// Start HF XT (48MHz) without bypass mode
	if(CS_startHFXT(false) == false) {
		return DRV_ERROR;
	}

	// Configure clock system
    CS_initClockSignal(CS_MCLK, MCLK_SRC, MCLK_DIV);     	// MCLK   = HFXTCLK
    CS_initClockSignal(CS_HSMCLK, HSMCLK_SRC, HSMCLK_DIV);  // HSMCLK = DCOCLK
    CS_initClockSignal(CS_SMCLK, SMCLK_SRC, SMCLK_DIV);     // SMCLK  = HFXTCLK
    CS_initClockSignal(CS_ACLK, ACLK_SRC, ACLK_DIV);     	// ACLK   = LFXTCLK
    CS_initClockSignal(CS_BCLK, BCLK_SRC, BCLK_DIV);     	// BCLK   = REFOCLk

	return DRV_OK;

}

/*
 * Copyright (c) 2016, Swiss Federal Institute of Technology (ETH Zurich).
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Author:  Reto Da Forno
 */

/*
 * Important Notes:
 * - Do not set PJSEL0 to 0, otherwise a factory reset will be necessary to reprogram the core.
 * - Do not use MAP_Interrupt_enableSleepOnIsrExit(), it does not work properly. Use Interrupt_enableSleepOnIsrExit() instead.
 * - Do not use MAP_PCM_gotoLPM3() or PCM_gotoLPM3(), both don't work. Use PM_ENTER_LPM3 instead.
 * - SRAM retention (at least for bank #7) must be enabled before entering LPM3 or deeper to prevent stack corruption.
 * - SRAM retention is not guaranteed in LPM3 once the reset line was pulled low. A power cycle is then required to restore SRAM retention. (see Errata SRAM2)
 * - UCA3STATW & UCBUSY flag stays high sometimes! -> use a delay loop instead
 */

#include "drivers_msp432/msp432driver.h"

/*
 * H W   C O N F I G   F U N C T I O N S
 */

void spi_b0_init(uint32_t bitclk_speed)
{
    PIN_SEL_PRI(SPI_B0_SOMI);
    PIN_SEL_PRI(SPI_B0_SIMO);
    PIN_SEL_PRI(SPI_B0_CLK);
    PIN_CFG_IN(SPI_B0_SOMI);
    PIN_CFG_OUT(SPI_B0_SIMO);
    PIN_CFG_OUT(SPI_B0_CLK);

    //UCA3CTL1 |= UCSWRST;  /* note: UCA3CTL1 for byte, UCA3CTL for word access */
    UCB0CTLW0  = UCSWRST;   /* reset both control register (CTL0 and CTL1) & set the SW reset bit */
    // 3-pin SPI, MSB first, master mode, 8-bit character length
    UCB0CTLW0 |= (UCMSB + UCMST + UCSYNC + (SPI_CPOL ? UCCKPL : 0) + (SPI_CPHA ? 0 : UCCKPH) + SPI_CLK_SRC);
    UCB0BRW    = (uint16_t)(SPI_CLK_SRC_SPEED / bitclk_speed);  /* word register */
    UCB0IE    &= ~(UCTXIE + UCRXIE);    /* disable interrupts */
    // don't enable SPI at this point

    // alternatively: configure SPI via driver lib
    /*const eUSCI_SPI_MasterConfig spiMasterConfig =
    {
        EUSCI_A_SPI_CLOCKSOURCE_ACLK,              // ACLK Clock Source
        32768,                                     // ACLK = LFXT = 32.768khz
        500000,                                    // SPICLK = 500khz
        EUSCI_A_SPI_MSB_FIRST,                     // MSB First
        EUSCI_A_SPI_PHASE_DATA_CHANGED_ONFIRST_CAPTURED_ON_NEXT,    // Phase
        EUSCI_A_SPI_CLOCKPOLARITY_INACTIVITY_HIGH, // High polarity
        EUSCI_A_SPI_3PIN                           // 3Wire SPI Mode
    };
    SPI_initMaster(EUSCI_A3_BASE, &spiMasterConfig);
    SPI_enableModule(EUSCI_A3_BASE);
    SPI_enableInterrupt(EUSCI_A3_BASE, EUSCI_A_SPI_RECEIVE_INTERRUPT);
    Interrupt_enableInterrupt(INT_EUSCIA3);*/
}


/*
 * ls7366.c
 *
 *  Created on: May 17, 2016
 *      Author: singhh23
 */

#include "drivers_msp432/ls7366.h"
#include "drivers_msp432/gpio.h"

static drv_ls7366_handle_t deviceHandles[LS7366_NUM_DEV];

drv_status_t DRV_LS7366_Init(drv_ls7366_device_t devNum) {

	// Ensure device number within expected bounds
	if(devNum >= LS7366_NUM_DEV) {
		return DRV_ERROR;
	}

	// set-up direct references for this device
	deviceHandles[devNum].spiBus = LS7366_SPI_BUS[devNum];
	deviceHandles[devNum].csLoc = &LS7366_GPIO[devNum];

	// start the spi bus for this device
	if(DRV_SPI_Start(deviceHandles[devNum].spiBus) != DRV_OK) {
		return DRV_ERROR;
	}

	// set-up the chip select signal to output
	if(DRV_GPIO_SetAsOutput(deviceHandles[devNum].csLoc) != DRV_OK) {
		return DRV_ERROR;
	}

	// set cs to high (off)
	if(DRV_GPIO_SetHigh(deviceHandles[devNum].csLoc) != DRV_OK) {
		return DRV_ERROR;
	}

	return DRV_OK;
}

drv_status_t DRV_LS7366_Start(drv_ls7366_device_t devNum) {

	if(devNum >= LS7366_NUM_DEV) {
		return DRV_ERROR;
	}

	uint8_t response0, response1;
	uint8_t cmp0 = MDR0_SETUP, cmp1 = MDR1_SETUP;

	// Check that the gpio was setup
	if(DRV_GPIO_SetHigh(deviceHandles[devNum].csLoc) != DRV_OK) {
		return DRV_ERROR;
	}

	// Clear counter to zero
	DRV_GPIO_SetLow(deviceHandles[devNum].csLoc);
	if(DRV_SPI_WriteRead(deviceHandles[devNum].spiBus, CLR_CNTR, NULL) != DRV_OK) {
		return DRV_BUSY;
	}
	DRV_GPIO_SetHigh(deviceHandles[devNum].csLoc);

	// Set-up MDR0 control register
	DRV_GPIO_SetLow(deviceHandles[devNum].csLoc);
	if(DRV_SPI_WriteRead(deviceHandles[devNum].spiBus, WRITE_MDR0, NULL) != DRV_OK) {
		return DRV_BUSY;
	}
	if(DRV_SPI_WriteRead(deviceHandles[devNum].spiBus, MDR0_SETUP, NULL) != DRV_OK) {
		return DRV_BUSY;
	}
	DRV_GPIO_SetHigh(deviceHandles[devNum].csLoc);

	// Set-up MDR1 control register
	DRV_GPIO_SetLow(deviceHandles[devNum].csLoc);
	if(DRV_SPI_WriteRead(deviceHandles[devNum].spiBus, WRITE_MDR1, NULL) != DRV_OK) {
		return DRV_BUSY;
	}
	if(DRV_SPI_WriteRead(deviceHandles[devNum].spiBus, MDR1_SETUP, NULL) != DRV_OK) {
		return DRV_BUSY;
	}
	DRV_GPIO_SetHigh(deviceHandles[devNum].csLoc);

	// Read back registers to ensure correct set-up
	DRV_GPIO_SetLow(deviceHandles[devNum].csLoc);
	if(DRV_SPI_WriteRead(deviceHandles[devNum].spiBus, READ_MDR0, NULL) != DRV_OK) {
		return DRV_BUSY;
	}
	if(DRV_SPI_WriteRead(deviceHandles[devNum].spiBus, 0x0, &response0) != DRV_OK) {
		return DRV_BUSY;
	}
	DRV_GPIO_SetHigh(deviceHandles[devNum].csLoc);

	DRV_GPIO_SetLow(deviceHandles[devNum].csLoc);
	if(DRV_SPI_WriteRead(deviceHandles[devNum].spiBus, READ_MDR1, NULL) != DRV_OK) {
		return DRV_BUSY;
	}
	if(DRV_SPI_WriteRead(deviceHandles[devNum].spiBus, 0x0, &response1) != DRV_OK) {
		return DRV_BUSY;
	}
	DRV_GPIO_SetHigh(deviceHandles[devNum].csLoc);

	if(response0 != cmp0 || response1 != cmp1) {
		return DRV_ERROR;
	}

	return DRV_OK;

}

drv_status_t DRV_LS7366_GetCount(drv_ls7366_device_t devNum, uint16_t* count) {

	// 1 byte for command, 2 bytes for counter information
	// extra byte added due to driver issues (byte 3 not latched when CheckStatus returns true)
	volatile uint8_t txData[4] = {READ_CNTR, 0x0};
	volatile uint8_t rxData[4];

	if(devNum >= LS7366_NUM_DEV) {
		return DRV_ERROR;
	}

	// enable cs signal
	if(DRV_GPIO_SetLow(deviceHandles[devNum].csLoc) != DRV_OK) {
		return DRV_ERROR;
	}

	// enable transfer
	if(DRV_SPI_WriteReadBlock(deviceHandles[devNum].spiBus, txData, rxData, 4) != DRV_OK) {
		return DRV_BUSY;
	}
	while(DRV_SPI_CheckStatus(deviceHandles[devNum].spiBus) != DRV_OK);
	if(DRV_GPIO_SetHigh(deviceHandles[devNum].csLoc) != DRV_OK) {
		return DRV_ERROR;
	}

	// return counter data
	*count = (uint16_t)rxData[1] << 8 | (uint16_t)rxData[2];

	return DRV_OK;
}

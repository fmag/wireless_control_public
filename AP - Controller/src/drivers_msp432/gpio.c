/*
 * gpio.c
 *
 *  Created on: May 17, 2016
 *      Author: singhh23
 */

#include "drivers_msp432/gpio.h"
#include "drivers_msp432/board_config.h"
#include "driverlib/MSP432P4xx/gpio.h"

drv_status_t DRV_GPIO_Init() {

	// Set-up all pins as low ouputs for minimum current drain
	// Ports PA-E are mapped onto ports P1-10 so don't need changing
	// Never map PJ (jtag pins - if done by accident, requires factory reset .gel)
	GPIO_setOutputLowOnPin(GPIO_PORT_P1, PIN_ALL16);
	GPIO_setAsOutputPin(GPIO_PORT_P1, PIN_ALL16);
	GPIO_setOutputLowOnPin(GPIO_PORT_P2, PIN_ALL16);
	GPIO_setAsOutputPin(GPIO_PORT_P2, PIN_ALL16);
	GPIO_setOutputLowOnPin(GPIO_PORT_P3, PIN_ALL16);
	GPIO_setAsOutputPin(GPIO_PORT_P3, PIN_ALL16);
	GPIO_setOutputLowOnPin(GPIO_PORT_P4, PIN_ALL16);
	GPIO_setAsOutputPin(GPIO_PORT_P4, PIN_ALL16);
	GPIO_setOutputLowOnPin(GPIO_PORT_P5, PIN_ALL16);
	GPIO_setAsOutputPin(GPIO_PORT_P5, PIN_ALL16);
	GPIO_setOutputLowOnPin(GPIO_PORT_P6, PIN_ALL16);
	GPIO_setAsOutputPin(GPIO_PORT_P6, PIN_ALL16);
	GPIO_setOutputLowOnPin(GPIO_PORT_P7, PIN_ALL16);
	GPIO_setAsOutputPin(GPIO_PORT_P7, PIN_ALL16);
	GPIO_setOutputLowOnPin(GPIO_PORT_P8, PIN_ALL16);
	GPIO_setAsOutputPin(GPIO_PORT_P8, PIN_ALL16);
	GPIO_setOutputLowOnPin(GPIO_PORT_P9, PIN_ALL16);
	GPIO_setAsOutputPin(GPIO_PORT_P9, PIN_ALL16);
	GPIO_setOutputLowOnPin(GPIO_PORT_P10, PIN_ALL16);
	GPIO_setAsOutputPin(GPIO_PORT_P10, PIN_ALL16);

	return DRV_OK;
}

drv_status_t DRV_GPIO_SetAsInput(gpio_handle_t* gpio) {
	GPIO_setAsInputPin(gpio->port, gpio->pin);
	gpio->mode = GPIO_INPUT;

	return DRV_OK;
}

drv_status_t DRV_GPIO_SetAsOutput(gpio_handle_t* gpio) {
	GPIO_setAsOutputPin(gpio->port, gpio->pin);
	gpio->mode = GPIO_OUTPUT;

	return DRV_OK;
}

drv_status_t DRV_GPIO_SetHigh(gpio_handle_t* gpio) {
	if(gpio->mode != GPIO_OUTPUT) {
		return DRV_ERROR;
	}

	GPIO_setOutputHighOnPin(gpio->port, gpio->pin);
	return DRV_OK;
}

drv_status_t DRV_GPIO_SetLow(gpio_handle_t* gpio) {
	if(gpio->mode != GPIO_OUTPUT) {
		return DRV_ERROR;
	}

	GPIO_setOutputLowOnPin(gpio->port, gpio->pin);
	return DRV_OK;
}

drv_status_t DRV_GPIO_GetInputPinValue(gpio_handle_t* gpio, uint8_t* val) {
	if(gpio->mode != GPIO_INPUT) {
		return DRV_ERROR;
	}

	*val = GPIO_getInputPinValue(gpio->port, gpio->pin);
	return DRV_OK;
}

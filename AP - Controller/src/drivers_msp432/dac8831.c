/*
 * dac8831.c
 *
 *  Created on: Jun 15, 2016
 *      Author: singhh23
 */

#include "drivers_msp432/dac8831.h"
#include "drivers_msp432/gpio.h"
#include "math.h"

static drv_dac8831_handle_t deviceHandles[DAC8831_NUM_DEV];

drv_status_t DRV_DAC8831_Init(drv_dac8831_device_t devNum) {
	// Ensure devnum within expected bounds
	if (devNum >= DAC8831_NUM_DEV) { return DRV_ERROR; }

	// set-up direct references for this device
	deviceHandles[devNum].spiBus = DAC8831_SPI_BUS[devNum];
	deviceHandles[devNum].csLoc = &DAC8831_GPIO[devNum];
	deviceHandles[devNum].ampLoc = &DAC8831_EN[devNum];

	DRV_GPIO_SetAsOutput(deviceHandles[devNum].ampLoc);
	DRV_GPIO_SetLow(deviceHandles[devNum].ampLoc);

	// start the spi bus for this device
	if (DRV_SPI_Start(deviceHandles[devNum].spiBus) != DRV_OK) { return DRV_ERROR; }

	// set-up the chip select signal to output
	if (DRV_GPIO_SetAsOutput(deviceHandles[devNum].csLoc) != DRV_OK) { return DRV_ERROR; }

	// set cs to high (off)
	if (DRV_GPIO_SetHigh(deviceHandles[devNum].csLoc) != DRV_OK) { return DRV_ERROR; }

	return DRV_OK;
}

drv_status_t DRV_DAC8831_Start(drv_dac8831_device_t devNum) {
	if (devNum >= DAC8831_NUM_DEV) { return DRV_ERROR; }

	// Set output to 0% (ie 0 volts)
	DRV_DAC8831_SetVoltage(devNum, 0);

	// Enable signal transmission to AMP
	DRV_GPIO_SetHigh(deviceHandles[devNum].ampLoc);

	return DRV_OK;
}

drv_status_t DRV_DAC8831_SetVoltage(drv_dac8831_device_t devNum, int perct) {
	if (devNum >= DAC8831_NUM_DEV) { return DRV_ERROR; }

	// range between -100 to 100
	if (perct < MIN_PERCT || perct > MAX_PERCT) { return DRV_ERROR; }

	// calculate actual percentage between 0 to 100
	float perctF = ((float)perct - MIN_PERCT) / (MAX_PERCT - MIN_PERCT);

	// Multiply by max output to get actual value to send to device
	// this is hardcoded because it shouldn't need to change (DAC8831 specific)
	uint16_t value = round(perctF * 0xFFFF);
	uint8_t txData[2] = {(value & 0xff00) >> 8, (value & 0x00ff)};
	uint8_t rxData[2];

	// enable cs signal and send data
	if (DRV_GPIO_SetLow(deviceHandles[devNum].csLoc) != DRV_OK) { return DRV_ERROR; }
	if (DRV_SPI_WriteReadBlock(deviceHandles[devNum].spiBus, txData, rxData, 2) != DRV_OK) {
		return DRV_BUSY;
	}
	while (DRV_SPI_CheckStatus(deviceHandles[devNum].spiBus) != DRV_OK)
		;
	if (DRV_GPIO_SetHigh(deviceHandles[devNum].csLoc) != DRV_OK) { return DRV_ERROR; }

	return DRV_OK;
}

drv_status_t DRV_DAC8831_TurnOff(drv_dac8831_device_t devNum) {
	DRV_GPIO_SetLow(deviceHandles[devNum].ampLoc);

	return DRV_OK;
}

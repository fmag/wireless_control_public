/*
 * t32.c
 *
 *  Created on: May 11, 2016
 *      Author: singhh23
 */

#include "drivers_msp432/t32.h"
#include "driverlib/MSP432P4xx/timer32.h"

#define _T32_IS_INITIALIZED() {\
	if(t32Initialized == 0) {\
		return DRV_ERROR;\
	}\
}

static uint8_t t32Initialized = 0;

drv_status_t DRV_T32_Init() {

	// Initialize the number of t32 we have with the pre-set configurations
	if(!t32Initialized) {

		uint8_t i;

		// start every timer with the related parameters
		for(i = 0; i < T32_NUM_TIMERS; i++) {
			//initialize module
			Timer32_initModule((uint32_t)t32Timer[i].baseAddr,
					t32Timer[i].prescaler, t32Timer[i].bitMode, t32Timer[i].operMode);
		}

		t32Initialized = 1;
	}

	return DRV_OK;
}

drv_status_t DRV_T32_Setup(t32_timer_t timer, timer_callback_t callback, void *usrObj) {
	t32Timer[timer].timerCallback = callback;
	t32Timer[timer].usrObj = usrObj;

	return DRV_OK;
}

drv_status_t DRV_T32_Start(t32_timer_t timer, uint32_t msPeriod, void *usrObj) {
	_T32_IS_INITIALIZED();

	// period must be within range
	if(msPeriod == 0 || MS_TO_CYCLES(msPeriod) >= UINT32_MAX) {
		return DRV_ERROR;
	}

	// ensure the correct person is using this
	if(t32Timer[timer].usrObj != usrObj) {
		return DRV_ERROR;
	}

	// set the counter to start counting down
	uint32_t count = MS_TO_CYCLES(msPeriod);
	Timer32_setCount((uint32_t)t32Timer[timer].baseAddr, count);

	// enable interrupts and start the timer
	Timer32_enableInterrupt((uint32_t)t32Timer[timer].baseAddr);
	Timer32_startTimer((uint32_t)t32Timer[timer].baseAddr, false);

	return DRV_OK;
}

void DRV_T32_Halt(t32_timer_t timer) {
    Timer32_haltTimer((uint32_t)t32Timer[timer].baseAddr);
}

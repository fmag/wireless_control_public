/*
 * llinit.c
 *
 *  Created on: May 11, 2016
 *      Author: singhh23
 */

#include "drivers_msp432/llinit.h"
#include "assert.h"
#include "driverlib/MSP432P4xx/wdt_a.h"
#include "drivers_msp432/cs.h"
#include "drivers_msp432/gpio.h"
#include "drivers_msp432/interrupts.h"
#include "drivers_msp432/pcm.h"
#include "drivers_msp432/spi.h"
#include "drivers_msp432/t32.h"
#include "drivers_msp432/uart.h"

void LowLevelInit(void) {

	// ORDER OF INITIALIZATION IS VITAL

	WDT_A_holdTimer(); // watchdog not used

	// Set-up GPIO
	if (DRV_GPIO_Init() != DRV_OK) { assert(0); }

	// Set-up PCM
	if (DRV_PCM_Init() != DRV_OK) { assert(0); }

	// Set-up CS
	if (DRV_CS_Init() != DRV_OK) { assert(0); }

	// Set-up T32
	if (DRV_T32_Init() != DRV_OK) { assert(0); }

	// Set-up SPI
	if (DRV_SPI_Init() != DRV_OK) { assert(0); }

	// UART set-up is done on user request (need requested baudrate)

	// Set-up interrupt vectors
	InterruptsInit();

	return;
}

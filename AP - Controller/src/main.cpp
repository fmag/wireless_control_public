#ifndef __NOP
#define __NOP __nop
#endif

#include "assert.h"
#include "controls/control_manager.hpp"
#include "driverlib/MSP432P4xx/pcm.h"
#include "drivers_msp432/board_config.h"
#include "drivers_msp432/llinit.h"
#include "interrupt.h"

void main(void) {
	// Checks to ensure that lla and driver enums match -- exceptions to the rule may exist in which
	// case checks may be removed (eg. two elements of lla enum map to same drivers enum element)
	if (NUM_BAUDRATES != WirelessControl::SerialPorts::NUM_BAUDRATES ||
	    T32_NUM_TIMERS != WirelessControl::Timers::NUM_TIMERS ||
	    LS7366_NUM_DEV != WirelessControl::Encoders::NUM_ENCODERS ||
	    DAC8831_NUM_DEV != WirelessControl::Motors::NUM_MOTORS ||
	    LED_NUM_DEV != WirelessControl::LEDs::NUM_LEDS ||
	    PB_NUM_DEV != WirelessControl::PushButtons::NUM_BUTTONS) {
		assert(0);
	}

	// Run low-level initialization
	LowLevelInit();

	WirelessControl::ControlManager controlManager = WirelessControl::ControlManager();

	// Infinite loop where the processor goes into low power mode when no events have to be
	// processed. After an interrupt happened we quit LPM.
	Interrupt_disableSleepOnIsrExit();
	while (1) {
		if (WirelessControl::ControlManager::syncLineHigh) {
			WirelessControl::ControlManager::syncLineHigh = false;
			controlManager.processRound();
		}
		// TODO: If we have multiple events than it could be possible to miss events if we do it
		// that way. I think we need some event vector to prevent this.

		// Enter low power mode while awaiting interrupts
		PCM_gotoLPM0();
		__no_operation();
	}
}

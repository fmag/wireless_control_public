#include "modes/remoteStabQuickSwitch.hpp"
#include "common.hpp"
#include "controls/control_manager.hpp"
#include "driverlib/MSP432P4xx/gpio.h"
#include "stdio.h"
#include "string.h"

namespace WirelessControl {

RemoteStabQuickSwitch::RemoteStabQuickSwitch(ControlManager *manager, unsigned int samplingRateHz,
                                 unsigned int communicationRateHz, uint16_t plantIdArr[],
                                 unsigned int numPlants, int controllerId)
    : Mode(manager) {
	this->samplingRateHz = samplingRateHz;
	this->samplingRateTicks = SAMPLING_RATE_TO_CYCLES(samplingRateHz);
	this->communicationRateHz = communicationRateHz;
	this->numPlants = numPlants;
	this->controllerId = controllerId;
	this->synclineSamplingOffset = SAMPLING_OFFSET_FACTOR * numPlants;

	memcpy((void *)plantIds, (void *)plantIdArr, numPlants * sizeof(plantIdArr[0]));
}

/**************************************************************************************************/

void RemoteStabQuickSwitch::setupGeneral(void) {
	myIdx = getIndex(NODE_ID);
	setNodeRole(NODE_ID);

	desState.data[0] = 0;    // position
	desState.data[1] = M_PI; // angle
	desState.data[2] = 0;    // position derivative
	desState.data[3] = 0;    // angle derivative

	memset((void *)predState, 0, sizeof(predState));
	memset((void *)control, 0, sizeof(control));
	memset((void *)prevControl, 0, sizeof(prevControl));

	for (unsigned int i = 0; i < MAX_PLANTS; i++) {
		currState[i].data[0] = desState.data[0];
		currState[i].data[1] = desState.data[1];
		currState[i].data[2] = desState.data[2];
		currState[i].data[3] = desState.data[3];
	}

	receivedData = false;
}

/**************************************************************************************************/

void RemoteStabQuickSwitch::setupRelay(void) {
	// Initialize all values to 0 at relay nodes so we can easily detect online/offline nodes in the
	// pendulum monitor GUI.
	for (unsigned int i = 0; i < MAX_PLANTS; i++) {
		currState[i].data[0] = 0;
		currState[i].data[1] = 0;
		currState[i].data[2] = 0;
		currState[i].data[3] = 0;
	}
}

/**************************************************************************************************/

void RemoteStabQuickSwitch::setupController(void) {
	// Values of matrix K depend on the communication rate.
	// TODO: we need more matrices for different sampling rates
	data_t K[PendulumDimensions::STATE][PendulumDimensions::CONTROL];
	switch (communicationRateHz) {
	// case 200: //TODO  move to localStab (without sync)
	// 	K[0][0] = -7.7617;
	// 	K[1][0] = 55.5586;
	// 	K[2][0] = -14.6347;
	// 	K[3][0] = 11.2466;
	case 50: // 20ms update interval
		K[0][0] = -7.175125562769559;
		K[1][0] = 53.401601471217610;
		K[2][0] = -13.993429091829578;
		K[3][0] = 10.808051212932412;
	case 40: // 25ms update interval
		K[0][0] = -6.982596579162277;
		K[1][0] = 52.690083281647890;
		K[2][0] = -13.782221400025630;
		K[3][0] = 10.663356176069179;
		break;
	case 33: // 30ms update interval
		K[0][0] = -6.791698877084626;
		K[1][0] = 51.982781093497010;
		K[2][0] = -13.572422131578087;
		K[3][0] = 10.519508568332960;
		break;
	case 25: // 40ms update interval
		K[0][0] = -6.415096147978185;
		K[1][0] = 50.581984741259355;
		K[2][0] = -13.157385573695892;
		K[3][0] = 10.234592940285962;
		break;
	case 22: // 45ms update interval
		K[0][0] = -6.212764368035671;
		K[1][0] = 49.826338721856075;
		K[2][0] = -12.933758272268665;
		K[3][0] = 10.080882515810748;
		break;
	case 20: // 50ms update interval
		K[0][0] = -6.045907761155506;
		K[1][0] = 49.201525159194880;
		K[2][0] = -12.748989149792951;
		K[3][0] = 9.953777488166573;
		break;
	default: ASSERT_PRINT();
	}

	manager->getController().SetGains(K, PendulumDimensions::STATE);
	manager->getPredictor().initialize(communicationRateHz);
}

/**************************************************************************************************/

void RemoteStabQuickSwitch::switchCommunicationRate(unsigned int comRateHz) {
    if (role == Mode::Controller) {
        // Values of matrix K depend on the communication rate.
        // TODO: we need more matrices for different sampling rates
        data_t K[PendulumDimensions::STATE][PendulumDimensions::CONTROL];
        switch (comRateHz) {
        // case 200: //TODO  move to localStab (without sync)
        //  K[0][0] = -7.7617;
        //  K[1][0] = 55.5586;
        //  K[2][0] = -14.6347;
        //  K[3][0] = 11.2466;
        case 50: // 20ms update interval
            K[0][0] = -7.175125562769559;
            K[1][0] = 53.401601471217610;
            K[2][0] = -13.993429091829578;
            K[3][0] = 10.808051212932412;
        case 40: // 25ms update interval
            K[0][0] = -6.982596579162277;
            K[1][0] = 52.690083281647890;
            K[2][0] = -13.782221400025630;
            K[3][0] = 10.663356176069179;
            break;
        case 33: // 30ms update interval
            K[0][0] = -6.791698877084626;
            K[1][0] = 51.982781093497010;
            K[2][0] = -13.572422131578087;
            K[3][0] = 10.519508568332960;
            break;
        case 25: // 40ms update interval
            K[0][0] = -6.415096147978185;
            K[1][0] = 50.581984741259355;
            K[2][0] = -13.157385573695892;
            K[3][0] = 10.234592940285962;
            break;
        case 22: // 45ms update interval
            K[0][0] = -6.212764368035671;
            K[1][0] = 49.826338721856075;
            K[2][0] = -12.933758272268665;
            K[3][0] = 10.080882515810748;
            break;
        case 20: // 50ms update interval
            K[0][0] = -6.045907761155506;
            K[1][0] = 49.201525159194880;
            K[2][0] = -12.748989149792951;
            K[3][0] = 9.953777488166573;
            break;
        default: ASSERT_PRINT();
        }

        manager->getController().SetGains(K, PendulumDimensions::STATE);
        manager->getPredictor().initialize(comRateHz);
    }
}

/**************************************************************************************************/

void RemoteStabQuickSwitch::setupPlant(void) {
	manager->getPendulumModel().initialize(samplingRateHz);
	manager->getObserver().initialize(samplingRateHz, samplingRateHz * 0.4);
	manager->getFilter().initialize(samplingRateHz, 12.5, 0.707);

	// We currently need this to ensure that we are in the required stability region during mode
	// switch.
	// TODO: However, this also means that we initially start stabilizing the pendulum with low
	// instead of high requirements. If the pendulum is not in an upright position before the first
	// round we will automatically switch to high requirements. I think we should move this property
	// to the pendulum class and update it accordingly (e.g. every time we call GetMeasurements()).
	// We can then leave out the checks in the mode code.
	inStabilizationRange = true;

	// Set-up timers
	// TODO: For the moment we keep timerFast in order to use the PeriodElapsedCallback
	// function.
	if (manager->getTimerFast().Initialize() != LLATypes::SUCCESS) { ASSERT_PRINT(); }
	manager->getTimerFast().SetListener(this);
	// Compute the point in time [cycles] when the next sampling event should occur (call to
	// PeriodElapsedCallback).
	// TODO: start the local routine here or in operation?
	nextSamplingPoint =
	    manager->getTimeObserver().getStartTime() - MS_TO_CYCLES(synclineSamplingOffset);
	manager->getTimeObserver().setSamplingPoint(nextSamplingPoint);
}

/**************************************************************************************************/

void RemoteStabQuickSwitch::teardownPlant(void) {
	manager->getTimerFast().Halt();
	Timer32_clearInterruptFlag(0);
}

/**************************************************************************************************/

void RemoteStabQuickSwitch::operationController(void) {
	// Read and store all sensor measurements. Measurements have to be mapped to the correct
	// internal node because we cannot rely on message order in case a measurement is lost
	// during communication.
	bolt_data_pkt_t sensorData;
	uint8_t sensorDataLen = 0;
	do {
		manager->getBolt().Read((uint8_t *)&sensorData,
		                        sensorDataLen); // TODO no 2 extra bytes?

		if (sensorDataLen != 0 && sensorData.senderID != 0) {
			// Find index of senderID in plantIds.
			int16_t idx = getIndex(sensorData.senderID);
			// Copy the received state information.
			memcpy((void *)&currState[idx].data[0], (void *)&sensorData.data[0],
			       sizeof(data_t) * PendulumDimensions::STATE);
			receivedData = true;
		}
	} while (sensorDataLen != 0);

	if (receivedData) {
		// Do all the state predictions and computations even for plants for which no new state
		// information was received.
		for (unsigned int i = 0; i < numPlants; i++) {
			// TODO: Somehow the reference to the KalmanObserver object has to be stored in a local
			// variable in order to correctly access GetStatePrediction. This results in a direct
			// function call instead of a indirect one in the asm code. This needs some research.
			// TODO: test with KalmanObserver &ko = ...
			// TODO: memdump comparision (printf ko vs printf getPredictor)
			KalmanObserver ko = manager->getPredictor();
			// Predict current state
			currState[i] = ko.GetStatePrediction(currState[i], prevControl[i], desState);
			// Predict future state
			predState[i] = ko.GetStatePrediction(currState[i], control[i], desState);
			// // Predict current state
			// currState[i] = manager->getPredictor().GetStatePrediction(currState[i],
			// prevControl[i], desState);
			// // Predict future state
			// predState[i] = manager->getPredictor().GetStatePrediction(currState[i], control[i],
			// desState);
			// Save last control input
			prevControl[i] = control[i];
			// Derive new control input
			control[i] = manager->getController().GetControlCommand(predState[i], desState);

			// Make sure control input respects saturations
			if (control[i].data[0] > 10) {
				control[i].data[0] = 10;
			} else if (control[i].data[0] < -10) {
				control[i].data[0] = -10;
			} else if (control[i].data[0] != control[i].data[0]) { // float convention: NaN != NaN
				control[i].data[0] = 0;
			}
		}

		// Send actuator commands
		manager->getBolt().Write((uint8_t *)&control[0].data[0],
		                         numPlants * sizeof(data_t) * PendulumDimensions::CONTROL);
	}

	// Flush BOLT queue
	manager->getBolt().Flush();

	// char buffer[250];
	// int buffer_bytes = sprintf(buffer, "%u: ctrl=%f pos=%f ang=%f\r\n", manager->getCurSchedId(),
	//                            control[0].data[0], currState[0].data[0], currState[0].data[1]);
	// if (buffer_bytes > 250) {
	// 	buffer_bytes = sprintf(buffer, "Exceeded maximum print buffer size (256)!\r\n");
	// }
	// manager->getSerOutput().Write((uint8_t *)buffer, buffer_bytes);
}

/**************************************************************************************************/

void RemoteStabQuickSwitch::operationPlant(void) {
	GPIO_setOutputLowOnPin(GPIO_PORT_P2, GPIO_PIN3);
	bolt_data_pkt_t ctrlData;
	uint8_t ctrlDataBytes;
	// Get actuator commands
	manager->getBolt().Read((uint8_t *)&ctrlData, ctrlDataBytes); // TODO no 2 extra bytes?
	// TODO: possible to read directly into control[0] to avoid memcpy?
	if (ctrlDataBytes != 0) {
		// int16_t idx = getSenderIdIdx(NODE_ID);
		memcpy(&control[0].data[0], &ctrlData.data[myIdx * sizeof(data_t)],
		       PendulumDimensions::CONTROL * sizeof(data_t));
		receivedData = true;
	}
	GPIO_setOutputHighOnPin(GPIO_PORT_P2, GPIO_PIN3);

	callbackFlag = false;
	nextSamplingPoint =
	    manager->getTimeObserver().getStartTime() - MS_TO_CYCLES(synclineSamplingOffset);
	manager->getTimeObserver().setSamplingPoint(nextSamplingPoint);

	GPIO_setOutputLowOnPin(GPIO_PORT_P2, GPIO_PIN3);
	while (callbackFlag == false)
		;
	GPIO_setOutputHighOnPin(GPIO_PORT_P2, GPIO_PIN3);

	// Write own pendulum state to Bolt.
	// NOTE: If the local sampling rate is too high, PeriodElapsedCallback has to wait until data is
	// written to Bolt.
	Timer32_disableInterrupt(0);
	GPIO_setOutputLowOnPin(GPIO_PORT_P2, GPIO_PIN3);
	manager->getBolt().Write((uint8_t *)&currState[0].data[0],
	                         sizeof(data_t) * PendulumDimensions::STATE);
	// Backup data for printing.
	data_t tmpCtrl = control[0].data[0];
	data_t tmpPos = currState[0].data[0];
	data_t tmpAng = currState[0].data[1];
	GPIO_setOutputHighOnPin(GPIO_PORT_P2, GPIO_PIN3);
	Timer32_enableInterrupt(0);

	// Flush BOLT queue
	manager->getBolt().Flush();

	// The plant node itself locally prints its state.
	char buffer[250]; // max UART buffer size 256!
	int buffer_bytes = sprintf(buffer, "%u;ctrl=%f pos=%f ang=%f\r\n", manager->getCurSchedId(),
	                           tmpCtrl, tmpPos, tmpAng);
	if (buffer_bytes > 250) {
		buffer_bytes = sprintf(buffer, "Exceeded maximum print buffer size (256)!\r\n");
	}
	manager->getSerOutput().Write((uint8_t *)buffer, buffer_bytes);
}

/**************************************************************************************************/

void RemoteStabQuickSwitch::PeriodElapsedCallback(Timers::timer_t id) {
	GPIO_setOutputHighOnPin(GPIO_PORT_P2, GPIO_PIN2);

	// Apply control input if we are in the control region and the communication is
	// established
	if (inStabilizationRange && receivedData) {
		// Zero-order hold: If no new control input was received we apply the old one.
		manager->getPlant().SetControl(&control[0]);
	} else {
		manager->getPlant().SetControl(0);
	}

	// Toggle pin at the end of actuation
	// GPIO_toggleOutputOnPin(GPIO_PORT_P2, GPIO_PIN2);
	// Toggle pin at the beginning of sensing
	// GPIO_toggleOutputOnPin(GPIO_PORT_P2, GPIO_PIN3);

	// Get sensor measurements
	measure_t measure = manager->getPlant().GetMeasurements();
	// Compute current state (derivatives)
	currState[0] = manager->getObserver().GetStateFromMeasurementsAndControl(measure);

	// Set current stabilization range.
	const controlRequirements_t *cr = inStabilizationRange ? &lowReq : &highReq;
	// Check if we are outside of the control region
	if (currState[0].data[0] > cr->x_max || currState[0].data[0] < cr->x_min ||
	    currState[0].data[1] > cr->a_max || currState[0].data[1] < cr->a_min ||
	    currState[0].data[0] != currState[0].data[0] || // NaN check
	    currState[0].data[1] != currState[0].data[1]) {
		inStabilizationRange = false;
		manager->getLed().Toggle();
	} else {
		inStabilizationRange = true;
		manager->getLed().TurnOff();
	}

	nextSamplingPoint -= samplingRateTicks;
	manager->getTimeObserver().setSamplingPoint(nextSamplingPoint);
	callbackFlag = true;

	GPIO_setOutputLowOnPin(GPIO_PORT_P2, GPIO_PIN2);
}

/**************************************************************************************************/

void RemoteStabQuickSwitch::operationRelay(void) {
	// The relay receives all network traffic (state information as well as control commands). Copy
	// the received state information of other pendulums to the internal array (could be that some
	// pendulum states are missing).
	bolt_data_pkt_t data;
	uint8_t dataLen = 0;
	do {
		manager->getBolt().Read((uint8_t *)&data, dataLen); // TODO no 2 extra bytes?

		if (dataLen != 0 && data.senderID != 0) {
			int16_t idx = getIndex(data.senderID);
			// If idx is >= 0, we have data from a plant node.
			if (idx >= 0) {
				memcpy((void *)&currState[idx].data[0], (void *)&data.data[0],
				       sizeof(data_t) * PendulumDimensions::STATE);
			}
		}
	} while (dataLen != 0);

	// Flush BOLT queue
	manager->getBolt().Flush();

	// Relay nodes print all states.
	char buffer[250]; // max UART buffer size 256!
	int buffer_bytes = sprintf(buffer,
	                           "%u;id=0 pos=%f ang=%f;id=1 pos=%f ang=%f;id=2 pos=%f ang=%f;"
	                           "id=3 pos=%f ang=%f;id=4 pos=%f ang=%f\r\n",
	                           manager->getCurSchedId(), currState[0].data[0], currState[0].data[1],
	                           currState[1].data[0], currState[1].data[1], currState[2].data[0],
	                           currState[2].data[1], currState[3].data[0], currState[3].data[1],
	                           currState[4].data[0], currState[4].data[1]);
	if (buffer_bytes > 250) {
		buffer_bytes = sprintf(buffer, "Exceeded maximum print buffer size (256)!\r\n");
	}
	manager->getSerOutput().Write((uint8_t *)buffer, buffer_bytes);
}

} // namespace WirelessControl

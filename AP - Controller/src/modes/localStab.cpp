#include "modes/localStab.hpp"
#include "common.hpp"
#include "controls/control_manager.hpp"
#include "gpio.h"
#include "stdio.h"
#include "string.h"

namespace WirelessControl {

LocalStab::LocalStab(ControlManager *manager, unsigned int samplingRateHz, uint16_t plantIdArr[],
                     unsigned int numPlants)
    : Mode(manager) {
	this->samplingRateHz = samplingRateHz;
	this->samplingRateTicks = SAMPLING_RATE_TO_CYCLES(samplingRateHz);
	this->numPlants = numPlants;

	memcpy((void *)plantIds, (void *)plantIdArr, numPlants * sizeof(plantIdArr[0]));
}

/**************************************************************************************************/

void LocalStab::setupGeneral(void) {
	myIdx = getIndex(NODE_ID);
	setNodeRole(NODE_ID);

	desState.data[0] = 0;    // position
	desState.data[1] = M_PI; // angle
	desState.data[2] = 0;    // position derivative
	desState.data[3] = 0;    // angle derivative

	memset((void *)control, 0, sizeof(control));

	for (unsigned int i = 0; i < MAX_PLANTS; i++) {
		currState[i].data[0] = desState.data[0];
		currState[i].data[1] = desState.data[1];
		currState[i].data[2] = desState.data[2];
		currState[i].data[3] = desState.data[3];
	}

	inStabilizationRange = true;
}

/**************************************************************************************************/

void LocalStab::setupRelay(void) {
	// Initialize all values to 0 at relay nodes so we can easily detect online/offline nodes in the
	// pendulum monitor GUI.
	for (unsigned int i = 0; i < MAX_PLANTS; i++) {
		currState[i].data[0] = 0;
		currState[i].data[1] = 0;
		currState[i].data[2] = 0;
		currState[i].data[3] = 0;
	}
}

/**************************************************************************************************/

void LocalStab::setupPlant(void) {
	data_t K[PendulumDimensions::STATE][PendulumDimensions::CONTROL];
	switch (samplingRateHz) {
	// dlqr, Q as in manual, R=0.001
	case 100:
		K[0][0] = -24.8454;
		K[1][0] = 103.9136;
		K[2][0] = -26.5300;
		K[3][0] = 17.6203;
		break;
	default: ASSERT_PRINT();
	}

	manager->getController().SetGains(K, PendulumDimensions::STATE);
	manager->getPendulumModel().initialize(samplingRateHz);
	manager->getObserver().initialize(samplingRateHz, samplingRateHz * 0.4);
	manager->getFilter().initialize(samplingRateHz, 12.5, 0.707);

	// Set-up timers
	// TODO: For the moment we keep timerFast in order to use the PeriodElapsedCallback
	// function.
	if (manager->getTimerFast().Initialize() != LLATypes::SUCCESS) { ASSERT_PRINT(); }
	manager->getTimerFast().SetListener(this);
	// Compute the point in time [cycles] when the next sampling event should occur (call to
	// PeriodElapsedCallback).
	// nextSamplingPoint =
	//     manager->getTimeObserver().getStartTime() - MS_TO_CYCLES(SYNCLINE_SAMPLING_OFFSET);
	// manager->getTimeObserver().setSamplingPoint(nextSamplingPoint);
	// TODO: Timer is free running and not in sync with CP (syncline). Is this a problem?
	manager->getTimerFast().Start(1000 / samplingRateHz);
}

/**************************************************************************************************/

void LocalStab::teardownPlant(void) {
	manager->getTimerFast().Halt();
	Timer32_clearInterruptFlag(0);
}

/**************************************************************************************************/

void LocalStab::PeriodElapsedCallback(Timers::timer_t id) {
	GPIO_setOutputHighOnPin(GPIO_PORT_P2, GPIO_PIN2);

	// Get sensor measurements
	measure_t measure = manager->getPlant().GetMeasurements();
	// Compute current state (derivatives)
	currState[myIdx] = manager->getObserver().GetStateFromMeasurementsAndControl(measure);

	// Set current stabilization range.
	const controlRequirements_t *cr = inStabilizationRange ? &lowReq : &highReq;
	// Check current state.
	if (currState[myIdx].data[0] > cr->x_max || currState[myIdx].data[0] < cr->x_min ||
	    currState[myIdx].data[1] > cr->a_max || currState[myIdx].data[1] < cr->a_min ||
	    currState[myIdx].data[0] != currState[myIdx].data[0] || // NaN check
	    currState[myIdx].data[1] != currState[myIdx].data[1]) {
		inStabilizationRange = false;
		manager->getLed().Toggle();
	} else {
		inStabilizationRange = true;
		manager->getLed().TurnOff();
	}

	// Compute control input
	control[0] = manager->getController().GetControlCommand(currState[myIdx], desState, true);

	if (inStabilizationRange) {
		// Zero-order hold: If no new control input was received we apply the old one.
		manager->getPlant().SetControl(&control[0]);
	} else {
		manager->getPlant().SetControl(0);
	}

	// TODO: PeriodElapsedCallback is not synced with sync line -> they might interfere. However,
	// local sampling rate and communication rate need to devide without rest. Furthermore,
	// processRound is not a ISR, so PeriodElapsedCallback is prioritized.
	char buffer[250];
	int buffer_bytes =
	    sprintf(buffer, "%u;ctrl=%f pos=%f ang=%f\r\n", manager->getCurSchedId(),
	            control[0].data[0], currState[myIdx].data[0], currState[myIdx].data[1]);
	if (buffer_bytes > 250) {
		buffer_bytes = sprintf(buffer, "Exceeded maximum print buffer size (256)!\r\n");
	}
	manager->getSerOutput().Write((uint8_t *)buffer, buffer_bytes);

	GPIO_setOutputLowOnPin(GPIO_PORT_P2, GPIO_PIN2);
}

/**************************************************************************************************/

void LocalStab::operationRelay(void) {
	// The relay receives all network traffic (state information as well as control commands). Copy
	// the received state information of other pendulums to the internal array (could be that some
	// pendulum states are missing).
	bolt_data_pkt_t data;
	uint8_t dataLen = 0;
	do {
		manager->getBolt().Read((uint8_t *)&data, dataLen); // TODO no 2 extra bytes?

		if (dataLen != 0 && data.senderID != 0) {
			int16_t idx = getIndex(data.senderID);
			// If idx is >= 0, we have data from a plant node.
			if (idx >= 0) {
				memcpy((void *)&currState[idx].data[0], (void *)&data.data[0],
				       sizeof(data_t) * PendulumDimensions::STATE);
			}
		}
	} while (dataLen != 0);

	// Flush BOLT queue
	manager->getBolt().Flush();

	// Relay nodes print all states.
	char buffer[250]; // max UART buffer size 256!
	int buffer_bytes = sprintf(buffer,
	                           "%u;id=0 pos=%f ang=%f;id=1 pos=%f ang=%f;id=2 pos=%f ang=%f;"
	                           "id=3 pos=%f ang=%f;id=4 pos=%f ang=%f\r\n",
	                           manager->getCurSchedId(), currState[0].data[0], currState[0].data[1],
	                           currState[1].data[0], currState[1].data[1], currState[2].data[0],
	                           currState[2].data[1], currState[3].data[0], currState[3].data[1],
	                           currState[4].data[0], currState[4].data[1]);
	if (buffer_bytes > 250) {
		buffer_bytes = sprintf(buffer, "Exceeded maximum print buffer size (256)!\r\n");
	}
	manager->getSerOutput().Write((uint8_t *)buffer, buffer_bytes);
}

} // namespace WirelessControl

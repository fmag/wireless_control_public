#include "modes/mode.hpp"
#include "common.hpp"
#include "stdint.h"

namespace WirelessControl {

// Static variable definition
state_t Mode::currState[MAX_PLANTS];     // + MAX_CONTROLLERS];
state_t Mode::predState[MAX_PLANTS];     // + MAX_CONTROLLERS];
control_t Mode::control[MAX_PLANTS];     // + MAX_CONTROLLERS];
control_t Mode::syncControl[MAX_PLANTS]; // + MAX_CONTROLLERS];
control_t Mode::prevControl[MAX_PLANTS]; // + MAX_CONTROLLERS];
state_t Mode::desState;
int16_t Mode::myIdx;
Mode::Role Mode::role;
bool Mode::receivedData;
volatile bool Mode::callbackFlag; // TODO: check if other variables need volatile attribute
uint32_t Mode::nextSamplingPoint;

} // namespace WirelessControl

#include "modes/localStabRemoteSync.hpp"
#include "common.hpp"
#include "controls/control_manager.hpp"
#include "gpio.h"
#include "stdio.h"
#include "string.h"

namespace WirelessControl {

LocalStabRemoteSync::LocalStabRemoteSync(ControlManager *manager, unsigned int samplingRateHz,
                                         unsigned int communicationRateHz, uint16_t plantIdArr[],
                                         unsigned int numPlants)
    : Mode(manager) {
	this->samplingRateHz = samplingRateHz;
	this->samplingRateTicks = SAMPLING_RATE_TO_CYCLES(samplingRateHz);
	this->communicationRateHz = communicationRateHz;
	this->numPlants = numPlants;
	this->synclineSamplingOffset = SAMPLING_OFFSET_FACTOR * numPlants;

	memcpy((void *)plantIds, (void *)plantIdArr, numPlants * sizeof(plantIdArr[0]));
}

/**************************************************************************************************/

void LocalStabRemoteSync::setupGeneral(void) {
	myIdx = getIndex(NODE_ID);
	setNodeRole(NODE_ID);

	desState.data[0] = 0;    // position
	desState.data[1] = M_PI; // angle
	desState.data[2] = 0;    // position derivative
	desState.data[3] = 0;    // angle derivative

	memset((void *)predState, 0, sizeof(predState));
	memset((void *)control, 0, sizeof(control));
	memset((void *)prevControl, 0, sizeof(prevControl));
	memset((void *)syncControl, 0, sizeof(syncControl));

	for (unsigned int i = 0; i < MAX_PLANTS; i++) {
		currState[i].data[0] = desState.data[0];
		currState[i].data[1] = desState.data[1];
		currState[i].data[2] = desState.data[2];
		currState[i].data[3] = desState.data[3];
		inStabilizationRange[i] = true;
	}

	receivedData = false;
}

/**************************************************************************************************/

void LocalStabRemoteSync::setupRelay(void) {
	// Initialize all values to 0 at relay nodes so we can easily detect online/offline nodes in the
	// pendulum monitor GUI.
	for (unsigned int i = 0; i < MAX_PLANTS; i++) {
		currState[i].data[0] = 0;
		currState[i].data[1] = 0;
		currState[i].data[2] = 0;
		currState[i].data[3] = 0;
	}
}

/**************************************************************************************************/

void LocalStabRemoteSync::setupPlant(void) {
	// TODO: more samplingRate matrices
	// The first dimension is two times the state dimension to distinguish between local and remote
	// states.
	data_t K[2 * PendulumDimensions::STATE][PendulumDimensions::CONTROL];
	switch (samplingRateHz) {
	case 100:
		if (numPlants == 2) {
			K[0][0] = -6.5872;
			K[1][0] = 45.3723;
			K[2][0] = -12.3382;
			K[3][0] = 9.0241;
			K[4][0] = 2.7663;
			K[5][0] = -3.9957;
			K[6][0] = 1.5773;
			K[7][0] = -0.8421;
		} else if (numPlants == 3) {
			K[0][0] = -8.7284;
			K[1][0] = 48.3752;
			K[2][0] = -13.5319;
			K[3][0] = 9.6558;
			K[4][0] = 3.0661;
			K[5][0] = -4.4817;
			K[6][0] = 1.7646;
			K[7][0] = -0.9451;
		} else if (numPlants == 4) {
		    K[0][0] = -10.7880;
		    K[1][0] = 51.2800;
		    K[2][0] = -14.6856;
		    K[3][0] = 10.2673;
		    K[4][0] = 2.7306;
		    K[5][0] = -3.9561;
		    K[6][0] = 1.5610;
		    K[7][0] = -0.8339;
		} else if (numPlants == 5) {
			K[0][0] = -12.552168910889263;
			K[1][0] = 53.731746496759364;
			K[2][0] = -15.663272581281076;
			K[3][0] = 10.782968775236485;
			K[4][0] = 2.489006820672327;
			K[5][0] = -3.579994669230611;
			K[6][0] = 1.415127498344538;
			K[7][0] = -0.754356563654295;
		} else {
			ASSERT_PRINT();
		}
		break;
	default: ASSERT_PRINT();
	}

	manager->getController().SetGains(K, 2 * PendulumDimensions::STATE);
	manager->getPendulumModel().initialize(samplingRateHz);
	manager->getObserver().initialize(samplingRateHz, samplingRateHz * 0.4);
	manager->getFilter().initialize(samplingRateHz, 12.5, 0.707);

	// Set-up timers
	// TODO: For the moment we keep timerFast in order to use the PeriodElapsedCallback
	// function.
	if (manager->getTimerFast().Initialize() != LLATypes::SUCCESS) { ASSERT_PRINT(); }
	manager->getTimerFast().SetListener(this);
	// Compute the point in time [cycles] when the next sampling event should occur (call to
	// PeriodElapsedCallback).
	// TODO: start the local routine here or in operation?
	nextSamplingPoint =
	    manager->getTimeObserver().getStartTime() - MS_TO_CYCLES(synclineSamplingOffset);
	manager->getTimeObserver().setSamplingPoint(nextSamplingPoint);
}

/**************************************************************************************************/

void LocalStabRemoteSync::teardownPlant(void) {
	manager->getTimerFast().Halt();
	Timer32_clearInterruptFlag(0);
}

/**************************************************************************************************/

void LocalStabRemoteSync::operationPlant(void) {
	// Copy the received state information of other pendulums to internal array (could be that
	// some pendulum states are missing).
	bolt_data_pkt_t sensorData;
	uint8_t sensorDataLen = 0;
	do {
		manager->getBolt().Read((uint8_t *)&sensorData,
		                        sensorDataLen); // TODO no 2 extra bytes?

		if (sensorDataLen != 0 && sensorData.senderID != 0) {
			int16_t idx = getIndex(sensorData.senderID);
			// Copying to currState here does not conflict with the access in
			// PeriodElapsedCallback due to different indicies.
			memcpy((void *)&currState[idx].data[0], (void *)&sensorData.data[0],
			       sizeof(data_t) * PendulumDimensions::STATE);
		}
	} while (sensorDataLen != 0);

	// Check stabilization ranges of all plants except ours.
	for (unsigned int i = 0; i < numPlants; i++) {
		if (i == myIdx) continue;

		// Set current stabilization range.
		const controlRequirements_t *cr = inStabilizationRange[i] ? &lowReq : &highReq;
		// Check current state.
		// TODO: Could be that currState is quite old if we did not hear something from a node
		// for a while. Add something like a age check?
		if (currState[i].data[0] > cr->x_max || currState[i].data[0] < cr->x_min ||
		    currState[i].data[1] > cr->a_max || currState[i].data[1] < cr->a_min ||
		    currState[i].data[0] != currState[i].data[0] || // NaN check
		    currState[i].data[1] != currState[i].data[1]) {
			inStabilizationRange[i] = false;
		} else {
			inStabilizationRange[i] = true;
		}
	}

	callbackFlag = false;
	nextSamplingPoint =
	    manager->getTimeObserver().getStartTime() - MS_TO_CYCLES(synclineSamplingOffset);
	manager->getTimeObserver().setSamplingPoint(nextSamplingPoint);

	while (callbackFlag == false)
		;

	// Write own pendulum state to Bolt.
	// NOTE: If the local sampling rate is too high, PeriodElapsedCallback has to wait until data is
	// written to Bolt.
	Timer32_disableInterrupt(0);
	manager->getBolt().Write((uint8_t *)&currState[myIdx].data[0],
	                         sizeof(data_t) * PendulumDimensions::STATE);
	// Backup data for printing.
	data_t tmpCtrl = control[0].data[0];
	data_t tmpPos = currState[myIdx].data[0];
	data_t tmpAng = currState[myIdx].data[1];
	Timer32_enableInterrupt(0);

	// Flush BOLT queue
	manager->getBolt().Flush();

	// The plant node itself locally prints its state.
	char buffer[250]; // max UART buffer size 256!
	int buffer_bytes = sprintf(buffer, "%u;ctrl=%f pos=%f ang=%f\r\n", manager->getCurSchedId(),
	                           tmpCtrl, tmpPos, tmpAng);
	if (buffer_bytes > 250) {
		buffer_bytes = sprintf(buffer, "Exceeded maximum print buffer size (256)!\r\n");
	}
	manager->getSerOutput().Write((uint8_t *)buffer, buffer_bytes);
}

/**************************************************************************************************/

void LocalStabRemoteSync::PeriodElapsedCallback(Timers::timer_t id) {
	GPIO_setOutputHighOnPin(GPIO_PORT_P2, GPIO_PIN2);

	// Get sensor measurements
	measure_t measure = manager->getPlant().GetMeasurements();
	// Compute current state (derivatives)
	currState[myIdx] = manager->getObserver().GetStateFromMeasurementsAndControl(measure);

	// Set current stabilization range.
	const controlRequirements_t *cr = inStabilizationRange[myIdx] ? &lowReq : &highReq;
	// Check current state.
	if (currState[myIdx].data[0] > cr->x_max || currState[myIdx].data[0] < cr->x_min ||
	    currState[myIdx].data[1] > cr->a_max || currState[myIdx].data[1] < cr->a_min ||
	    currState[myIdx].data[0] != currState[myIdx].data[0] || // NaN check
	    currState[myIdx].data[1] != currState[myIdx].data[1]) {
		inStabilizationRange[myIdx] = false;
		manager->getLed().Toggle();
	} else {
		inStabilizationRange[myIdx] = true;
		manager->getLed().TurnOff();
	}

	// Compute control input
	control[0].data[0] = 0;
	for (unsigned int i = 0; i < numPlants; i++) {
		// If a node is not in the stabilization range, we have to ignore it in the sync
		// computation.
		if (inStabilizationRange[i]) {
			syncControl[i] =
			    manager->getController().GetControlCommand(currState[i], desState, (i == myIdx));
			control[0].data[0] += syncControl[i].data[0];
		}
	}

	if (inStabilizationRange[myIdx]) {
		// Zero-order hold: If no new control input was received we apply the old one.
		manager->getPlant().SetControl(&control[0]);
	} else {
		manager->getPlant().SetControl(0);
	}

	nextSamplingPoint -= samplingRateTicks;
	manager->getTimeObserver().setSamplingPoint(nextSamplingPoint);
	callbackFlag = true;

	GPIO_setOutputLowOnPin(GPIO_PORT_P2, GPIO_PIN2);
}

/**************************************************************************************************/

void LocalStabRemoteSync::operationRelay(void) {
	// The relay receives all network traffic (state information as well as control commands). Copy
	// the received state information of other pendulums to the internal array (could be that some
	// pendulum states are missing).
	bolt_data_pkt_t data;
	uint8_t dataLen = 0;
	do {
		manager->getBolt().Read((uint8_t *)&data, dataLen); // TODO no 2 extra bytes?

		if (dataLen != 0 && data.senderID != 0) {
			int16_t idx = getIndex(data.senderID);
			// If idx is >= 0, we have data from a plant node.
			if (idx >= 0) {
				memcpy((void *)&currState[idx].data[0], (void *)&data.data[0],
				       sizeof(data_t) * PendulumDimensions::STATE);
			}
		}
	} while (dataLen != 0);

	// Flush BOLT queue
	manager->getBolt().Flush();

	// Relay nodes print all states.
	char buffer[250]; // max UART buffer size 256!
	int buffer_bytes = sprintf(buffer,
	                           "%u;id=0 pos=%f ang=%f;id=1 pos=%f ang=%f;id=2 pos=%f ang=%f;"
	                           "id=3 pos=%f ang=%f;id=4 pos=%f ang=%f\r\n",
	                           manager->getCurSchedId(), currState[0].data[0], currState[0].data[1],
	                           currState[1].data[0], currState[1].data[1], currState[2].data[0],
	                           currState[2].data[1], currState[3].data[0], currState[3].data[1],
	                           currState[4].data[0], currState[4].data[1]);
	if (buffer_bytes > 250) {
		buffer_bytes = sprintf(buffer, "Exceeded maximum print buffer size (256)!\r\n");
	}
	manager->getSerOutput().Write((uint8_t *)buffer, buffer_bytes);
}

} // namespace WirelessControl

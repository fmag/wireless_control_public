#ifndef INCLUDE_CONTROLS_CONTROLLERS_CONTROLLER_HPP_
#define INCLUDE_CONTROLS_CONTROLLERS_CONTROLLER_HPP_

#include "common.hpp"

namespace WirelessControl {

// This class acts as a base class for different controllers.
class Controller {

  public:
	Controller(){};
	virtual ~Controller(){};

	// Get the suggested control command from the controller based on the current and the desired
	// state of the system.
	virtual control_t GetControlCommand(const state_t &currState, const state_t &desState,
	                                    bool isLocalState = 1) = 0;

	// Set the gains for the controller.
	virtual void SetGains(data_t K[][PendulumDimensions::CONTROL], unsigned int nbPlants) = 0;
};

} // namespace WirelessControl

#endif /* INCLUDE_CONTROLS_CONTROLLERS_CONTROLLER_HPP_ */

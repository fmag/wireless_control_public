#ifndef INCLUDE_CONTROLS_CONTROLLERS_FBFF_CONTROLLER_HPP_
#define INCLUDE_CONTROLS_CONTROLLERS_FBFF_CONTROLLER_HPP_

#include "common.hpp"
#include "controls/controllers/controller.hpp"

namespace WirelessControl {

// This class implements the basic state feedback implementation of a controller, ie ufb =
// -K*(x-x_d).
class FeedforwardFeedbackController : public Controller {

  public:
	FeedforwardFeedbackController(){};
	virtual ~FeedforwardFeedbackController(){};

	virtual void SetGains(data_t K[][PendulumDimensions::CONTROL], unsigned int nbPlants);
	virtual control_t GetControlCommand(const state_t &currState, const state_t &desState,
	                                    const bool isLocalState = 1);

  private:
	data_t _K[MAX_PLANTS * PendulumDimensions::STATE][PendulumDimensions::CONTROL];
};

} // namespace WirelessControl

#endif /* INCLUDE_CONTROLS_CONTROLLERS_FBFF_CONTROLLER_HPP_ */

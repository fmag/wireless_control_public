#ifndef INCLUDE_CONTROLS_PLANTS_PLANT_HPP_
#define INCLUDE_CONTROLS_PLANTS_PLANT_HPP_

#include "common.hpp"

namespace WirelessControl {

class ControlManager;

class Plant {

  public:
	Plant(ControlManager *manager)
	    : manager(manager){};
	virtual ~Plant(){};

	// Run any initialization routines for sensors/actuators.
	virtual bool Initialize() = 0;
	// Employ the given control input on the actuator.
	virtual void SetControl(control_t *currControl) = 0;
	// Get sensor measurements.
	virtual measure_t GetMeasurements() = 0;
	// Disables the plant (eg. turn off the actuator).
	virtual void TurnOff() = 0;

  protected:
	ControlManager *manager;
};

} // namespace WirelessControl

#endif /* INCLUDE_CONTROLS_PLANTS_PLANT_HPP_ */

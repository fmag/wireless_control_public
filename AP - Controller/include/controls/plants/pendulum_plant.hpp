#ifndef INCLUDE_CONTROLS_PLANTS_PENDULUM_PLANT_HPP_
#define INCLUDE_CONTROLS_PLANTS_PENDULUM_PLANT_HPP_

#include "common.hpp"
#include "controls/plants/plant.hpp"
#include "low_level_abstraction/encoder.hpp"
#include "low_level_abstraction/motor.hpp"

namespace WirelessControl {

class ControlManager;

// This class implements an inverted pendulum plant with physical interfaces for sensors/actuators.
class PendulumPlant : public Plant {

  public:
	PendulumPlant(ControlManager *manager);
	virtual ~PendulumPlant(){};

	virtual bool Initialize();
	virtual void SetControl(control_t *currControl);
	virtual measure_t GetMeasurements();
	virtual void TurnOff();

  private:
	EncoderInterface _encoderPosition;
	EncoderInterface _encoderAngle;
	MotorInterface _motor;
};

} // namespace WirelessControl

#endif /* INCLUDE_CONTROLS_PLANTS_PENDULUM_PLANT_HPP_ */

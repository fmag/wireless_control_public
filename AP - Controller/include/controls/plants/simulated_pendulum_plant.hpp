#ifndef INCLUDE_CONTROLS_PLANTS_SIMULATED_PENDULUM_PLANT_HPP_
#define INCLUDE_CONTROLS_PLANTS_SIMULATED_PENDULUM_PLANT_HPP_

#include "common.hpp"
#include "controls/plants/plant.hpp"

namespace WirelessControl {

class ControlManager;

class SimulatedPendulumPlant : public Plant {

  public:
	SimulatedPendulumPlant(ControlManager *manager);
	virtual ~SimulatedPendulumPlant(){};

	virtual bool Initialize();
	virtual void SetControl(control_t *currControl);
	virtual measure_t GetMeasurements();
	virtual void TurnOff();

  private:
	measure_t _measure;
	state_t _state_plant, desState;
};

} // namespace WirelessControl

#endif /* INCLUDE_CONTROLS_PLANTS_SIMULATED_PENDULUM_PLANT_HPP_ */

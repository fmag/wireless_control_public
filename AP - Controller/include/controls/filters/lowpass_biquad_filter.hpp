#ifndef INCLUDE_CONTROLS_OBSERVERS_BIQUAD_FILTER_HPP_
#define INCLUDE_CONTROLS_OBSERVERS_BIQUAD_FILTER_HPP_

#include "controls/filters/filter.hpp"

namespace WirelessControl {

// This class implements a lowpass filter using a biquad method.
class LowPassBiquadFilter : public Filter {

  public:
	LowPassBiquadFilter(){};
	virtual ~LowPassBiquadFilter(){};

	data_t ProcessData(data_t unfilteredState);
	void initialize(data_t Fs, data_t Fc, data_t Q);

  private:
	data_t _a0, _a1, _a2, _b1, _b2; // filter coefficients
	data_t _Fs, _Fc, _Q;            // filter parameters
	data_t _z1, _z2;                // delayed states
	bool initialized;
};

} // namespace WirelessControl

#endif /* INCLUDE_CONTROLS_OBSERVERS_BIQUAD_FILTER_HPP_ */

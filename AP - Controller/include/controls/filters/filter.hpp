#ifndef INCLUDE_CONTROLS_FILTERS_FILTER_HPP_
#define INCLUDE_CONTROLS_FILTERS_FILTER_HPP_

#include "common.hpp"

namespace WirelessControl {

// This class acts as a base class for different filters.
class Filter {

  public:
	Filter(){};
	virtual ~Filter(){};

	// Apply the filter to the given data.
	virtual inline data_t ProcessData(data_t unfilteredState) = 0;
};

} // namespace WirelessControl

#endif /* INCLUDE_CONTROLS_FILTERS_FILTER_HPP_ */

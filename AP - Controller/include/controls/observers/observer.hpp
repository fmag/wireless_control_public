#ifndef INCLUDE_CONTROLS_OBSERVERS_OBSERVER_HPP_
#define INCLUDE_CONTROLS_OBSERVERS_OBSERVER_HPP_

#include "common.hpp"

namespace WirelessControl {

// This class acts as a base class for different observers.
class Observer {

  public:
	Observer(){};
	virtual ~Observer(){};

	// Reconstruct full state from measurements and control signal.
	virtual state_t GetStateFromMeasurementsAndControl(const measure_t &currState,
	                                                   const control_t &prevControl) = 0;

	// Reconstruct full state from measurements
	virtual state_t GetStateFromMeasurementsAndControl(const measure_t &currState) = 0;

	virtual state_t GetStatePrediction(const state_t &currState, const control_t &prevControl,
	                                   const state_t &desState) = 0;
};

} // namespace WirelessControl

#endif /* INCLUDE_CONTROLS_OBSERVERS_OBSERVER_HPP_ */

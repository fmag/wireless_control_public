#ifndef INCLUDE_CONTROLS_OBSERVERS_FINITE_DIFFERENCE_OBSERVER_HPP_
#define INCLUDE_CONTROLS_OBSERVERS_FINITE_DIFFERENCE_OBSERVER_HPP_

#include "controls/filters/lowpass_biquad_filter.hpp"
#include "controls/observers/observer.hpp"

namespace WirelessControl {

// This observer reconstructs the states by using finite differences.
class FiniteDifferenceObserver : public Observer {

  public:
	FiniteDifferenceObserver(){};
	virtual ~FiniteDifferenceObserver(){};

	state_t GetStateFromMeasurementsAndControl(const measure_t &currState,
	                                           const control_t &prevControl);
	state_t GetStateFromMeasurementsAndControl(const measure_t &currState);
	state_t GetStatePrediction(const state_t &currState, const control_t &prevControl,
	                           const state_t &desState);

	void initialize(const unsigned int sampleRate, const data_t cutoffFreq);

  private:
	LowPassBiquadFilter _lpFilter[PendulumDimensions::STATE];
	data_t _samplePeriod;
	measure_t _z1;
	bool initialized;
};

} // namespace WirelessControl

#endif /* INCLUDE_CONTROLS_OBSERVERS_FINITE_DIFFERENCE_OBSERVER_HPP_ */

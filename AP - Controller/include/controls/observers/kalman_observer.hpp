#ifndef INCLUDE_CONTROLS_OBSERVERS_KALMAN_OBSERVER_HPP_
#define INCLUDE_CONTROLS_OBSERVERS_KALMAN_OBSERVER_HPP_

#include "common.hpp"
#include "controls/observers/observer.hpp"

namespace WirelessControl {

class KalmanObserver : public Observer {

  public:
	KalmanObserver(){};
	~KalmanObserver(){};

	state_t GetStateFromMeasurementsAndControl(const measure_t &newMeasurement,
	                                           const control_t &prevControl);
	state_t GetStateFromMeasurementsAndControl(const measure_t &newMeasurement);
	state_t GetStatePrediction(const state_t &currState, const control_t &prevControl,
	                           const state_t &desState);

	void initialize(const unsigned int sampleRate);

  private:
	data_t _A[PendulumDimensions::STATE][PendulumDimensions::STATE];
	data_t _B[PendulumDimensions::STATE][PendulumDimensions::CONTROL];
	data_t _C[PendulumDimensions::OUTPUT][PendulumDimensions::CONTROL];

	data_t _Q;
	data_t _R;

	data_t _P[PendulumDimensions::STATE][PendulumDimensions::STATE];
	state_t _xCurr;
};

} // namespace WirelessControl

#endif /* INCLUDE_CONTROLS_OBSERVERS_KALMAN_OBSERVER_HPP_ */

#ifndef INCLUDE_CONTROLS_CONTROL_MANAGER_HPP_
#define INCLUDE_CONTROLS_CONTROL_MANAGER_HPP_

//#include "drivers_msp432/msp432driver.h"

//#include "config.hpp"
#include "controls/controllers/fbff_controller.hpp"
#include "controls/filters/lowpass_biquad_filter.hpp"
#include "controls/observers/finite_difference_observer.hpp"
#include "controls/observers/kalman_observer.hpp"
#include "controls/plants/plant.hpp"

#include "low_level_abstraction/bolt.hpp"
#include "low_level_abstraction/led.hpp"
#include "low_level_abstraction/serial_port.hpp"
#include "low_level_abstraction/timer.hpp"
#include "modes/mode.hpp"
#include "modes/timeObserver.hpp"

namespace WirelessControl {

class ControlManager {

  public:
	ControlManager();
	virtual ~ControlManager();

	Plant &getPlant() {
		return *plant;
	}

	LEDInterface &getLed() {
		return led;
	}

	TimerInterface &getTimerFast() {
		return timerFast;
	}

	TimeObserver &getTimeObserver() {
		return timeObserver;
	}

	BoltInterface &getBolt() {
		return bolt;
	}

	SerialPortInterface &getSerOutput() {
		return serOutput;
	}

	FeedforwardFeedbackController &getController() {
		return controller;
	}

	KalmanObserver &getPredictor() {
		return predictor;
	}

	FiniteDifferenceObserver &getObserver() {
		return observer;
	}

	LowPassBiquadFilter &getFilter() {
		return lpFilter;
	}

	KalmanObserver &getPendulumModel() {
		return pendulumModel;
	}

	unsigned int getCurSchedId() {
		return curSchedId;
	}

	Mode *getMode() {
		return modeList[curSchedId];
	}

	static bool syncLineHigh;
	static ControlManager *manager;
	void processRound();

  private:
	static void syncLineISR(void);

	unsigned int curSchedId;
	Plant *plant;
	Mode *modeList[16];
	unsigned int modeCnt;

	LEDInterface led;
	TimerInterface timerFast;
	TimeObserver timeObserver;
	BoltInterface bolt;
	SerialPortInterface serOutput;

	FeedforwardFeedbackController controller;
	KalmanObserver predictor;
	FiniteDifferenceObserver observer;
	LowPassBiquadFilter lpFilter;
	KalmanObserver pendulumModel;
};

} // namespace WirelessControl


#endif /* INCLUDE_CONTROLS_CONTROL_MANAGER_HPP_ */

#ifndef INCLUDE_CONFIG_HPP_
#define INCLUDE_CONFIG_HPP_

// Define maximum number of pendulums. A mode cannot use more than this.
#define MAX_PLANTS 5
// Define maximum number of controllers. A mode cannot use more than this.
// #define MAX_CONTROLLERS 2
// The mode defines which node is the controller etc. AP and CP on a DPP have to use the same
// ID! Do not use NODE_ID 0!
#define NODE_ID 1
// Set this to 1 if a real pendulum is connected to the board, otherwise a simulated pendulum is
// used.
#define REAL_PENDULUM_CONNECTED 0
// In case a simulated pendulum is used we can discard control inputs that are smaller than 1 to
// introduce artificial pendulum oscillation.
#define SIM_PENDULUM_OSCILLATION 0
// Local sampling is synchronized with sync line and started a specific time after the sync line.
// This time depends on the amount of data that needs to be read from Bolt. Here we define a time in
// ms per data item in Bolt. We want to do this after reading all data from Bolt so we write the
// most recent sensor reading into Bolt.
// TODO: better implementation
#define SAMPLING_OFFSET_FACTOR 1.5

// TODO: common header for CP & AP
#define DEFAULT_MODE_ID 0
#define LWB_DATA_PKT_PAYLOAD_LEN 16

#endif /* INCLUDE_MSP432_APP_DEFINES_HPP_ */

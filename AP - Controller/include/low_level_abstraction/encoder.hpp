/*
 * encoder.hpp
 *
 *  Created on: May 10, 2016
 *      Author: singhh23
 */

#ifndef INCLUDE_LOW_LEVEL_ABSTRACTION_ENCODER_HPP_
#define INCLUDE_LOW_LEVEL_ABSTRACTION_ENCODER_HPP_

#include "low_level_abstraction/abstract_hardware.hpp"
#include "low_level_abstraction/hw_config.hpp"

namespace WirelessControl {

/**
 * @details This class acts as a user interface to an arbitrary encoder.
 */
class EncoderInterface : public AbstractHardware {

  public:
	/**
	 * @details This method constructs an object which interfaces to the selected encoder.
	 * @param[in] enc The encoder to be interfaced with.
	 */
	EncoderInterface(Encoders::encoder_t enc)
	    : _encType(enc){};
	virtual ~EncoderInterface(){};

	/**
	 * @details This method starts the encoder - it must be called prior to GetCount.
	 */
	LLATypes::lla_return_t Start();

	/**
	 * @details This method gets the current count of the given encoder.
	 * @param[out] count The current count if operation was successful.
	 * @return SUCCESS if operation passed, FAILURE otherwise.
	 */
	LLATypes::lla_return_t GetCount(uint16_t &count);

	/**
	 * @details This method changes the encoder to be interfaced with.
	 */
	void SetEncoderType(Encoders::encoder_t enc) {
		_encType = enc;
	};

	/**
	 * @details This method gets the current encoder being interfaced with.
	 */
	Encoders::encoder_t GetEncoderType() {
		return _encType;
	}

  protected:
	/**
	 * @description This method initializes the encoder.
	 */
	virtual LLATypes::lla_return_t DoInitialize();

  private:
	Encoders::encoder_t _encType; // the encoder type being interfaced with
};

} // namespace WirelessControl

#endif /* INCLUDE_LOW_LEVEL_ABSTRACTION_ENCODER_HPP_ */

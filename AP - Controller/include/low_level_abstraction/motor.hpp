/*
 * motor.hpp
 *
 *  Created on: Jun 15, 2016
 *      Author: singhh23
 */

#ifndef INCLUDE_LOW_LEVEL_ABSTRACTION_MOTOR_HPP_
#define INCLUDE_LOW_LEVEL_ABSTRACTION_MOTOR_HPP_

#include "low_level_abstraction/abstract_hardware.hpp"
#include "low_level_abstraction/hw_config.hpp"

namespace WirelessControl {

/**
 * @details This class acts as a user interface to an arbitrary motor.
 */
class MotorInterface : public AbstractHardware {

public:

	/**
	 * @details This method constructs an object which interfaces to the selected motor.
	 * @param[in] motor The motor to be interfaced with.
	 */
	MotorInterface(Motors::motor_t motor) : _motorType(motor) {};
	virtual ~MotorInterface() {};

	/**
	 * @details This method starts the motor - it must be called prior to using the RunMotor method.
	 */
	LLATypes::lla_return_t Start();

	/**
	 * @details This method sets the current output voltage of the motor.
	 * @param[in] perct An integer value representing the percentage of output
	 * voltage which should be set (accepts between -100 to 100 percent).
	 * @return SUCCESS if operation passed, FAILURE if the voltage is outside
	 * of prescribed bounds or if the SPI interface was busy.
	 */
	LLATypes::lla_return_t RunMotor(int perct);

	/**
	 * @details This method changes the encoder to be interfaced with.
	 */
	void SetMotorNumber(Motors::motor_t motor) {
		_motorType = motor;
	};

	/**
	 * @details This method gets the current encoder being interfaced with.
	 */
	Motors::motor_t GetMotorNumber() {
		return _motorType;
	}

	/**
	 * @details This method turns off the motor (disables relay).
	 */
	void TurnOff();

protected:

	/**
	 * @description This method initializes the motor.
	 */
	virtual LLATypes::lla_return_t DoInitialize();

private:
	Motors::motor_t _motorType;	// the encoder type being interfaced with
};

}



#endif /* INCLUDE_LOW_LEVEL_ABSTRACTION_MOTOR_HPP_ */

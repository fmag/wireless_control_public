#ifndef INCLUDE_LOW_LEVEL_ABSTRACTION_TIMER_LISTENER_HPP_
#define INCLUDE_LOW_LEVEL_ABSTRACTION_TIMER_LISTENER_HPP_

#include "low_level_abstraction/hw_config.hpp"

namespace WirelessControl {

// This class defines the interface that has to be implemented by all classes that listen to timers.
// The timer will notify all listeners via the PeriodElapsedCallback method once the given period
// has elapsed.
class TimerListenerInterface {

  public:
	TimerListenerInterface() {
	}
	virtual ~TimerListenerInterface() {
	}

	// This is the function that is called when the period of a timer is elapsed. Note that the same
	// listener may listen to multiple timers at once and thus this function may be called by
	// different timers at different periods.
	virtual void PeriodElapsedCallback(Timers::timer_t id) = 0;
};

} // namespace WirelessControl

#endif /* INCLUDE_LOW_LEVEL_ABSTRACTION_TIMER_LISTENER_HPP_ */

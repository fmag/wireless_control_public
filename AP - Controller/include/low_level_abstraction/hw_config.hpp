/*
 * hw_config.hpp
 *
 *  Created on: May 17, 2016
 *      Author: singhh23
 */

#ifndef INCLUDE_LOW_LEVEL_ABSTRACTION_HW_CONFIG_HPP_
#define INCLUDE_LOW_LEVEL_ABSTRACTION_HW_CONFIG_HPP_

namespace WirelessControl {

// !!NOTE!!
// Changing any of the enums below _REQUIRES_ a change in the board_config files
// of the drivers_msp432 directory

// WHEN ADDING A NEW ENUM, ALSO ADD A NEW SANITY CHECK IN sanitycheck.cpp

// An enumeration of all the available encoders.
namespace Encoders {
typedef enum Encoder {
	ENCODER_ANGLE = 0,
	ENCODER_POSITION,
	NUM_ENCODERS
} encoder_t;
}

// An enumeration of all the available motors.
namespace Motors {
typedef enum Motor {
	MOTOR_0 = 0,
	NUM_MOTORS
} motor_t;
}

// An enumeration of all the available timers.
namespace Timers {
typedef enum Timer {
	TIMER_A = 0,
	TIMER_B,
	NUM_TIMERS
} timer_t;
}

// An enumeration of all the available LEDs.
namespace LEDs {
typedef enum LED {
	LED_A = 0,
	NUM_LEDS
} led_t;
}


// An enumeration of all the available push buttons.
namespace PushButtons {
typedef enum PushButton {
	BUTTON_0 = 0,
	BUTTON_1,
	NUM_BUTTONS
} push_button_t;
}

// An enumeration of all the available serial ports.
namespace SerialPorts {
typedef enum SerialPort {
	DEBUG,
	APPLICATION,
	NUM_PORTS
} serial_port_t;

// An enumeration of the possible baudrates for uart operation.
typedef enum BaudRate {
	br9600 = 0,
	br115200,
	br230400,
	br460800,
	NUM_BAUDRATES
} baud_rate_t;
}

}



#endif /* INCLUDE_LOW_LEVEL_ABSTRACTION_HW_CONFIG_HPP_ */

/*
 * led.hpp
 *
 *  Created on: Aug 11, 2016
 *      Author: singhh23
 */

#ifndef INCLUDE_LOW_LEVEL_ABSTRACTION_LED_HPP_
#define INCLUDE_LOW_LEVEL_ABSTRACTION_LED_HPP_


#include "low_level_abstraction/abstract_hardware.hpp"
#include "low_level_abstraction/hw_config.hpp"

namespace WirelessControl {

/**
 * @details This class acts as a user interface to an arbitrary led.
 */
class LEDInterface : public AbstractHardware {

public:

	/**
	 * @details This method constructs and LED interface.
	 */
	LEDInterface(LEDs::led_t id)
		: _ledId(id), _on(false)  {};

	virtual ~LEDInterface() {};

	/**
	 * @details Turn on the LED.
	 */
	LLATypes::lla_return_t TurnOn();

	/**
	 * @details Turn off the LED.
	 */
	LLATypes::lla_return_t TurnOff();

	/**
	 * @details Toggle the LED.
	 */
	LLATypes::lla_return_t Toggle();

protected:

	/**
	 * @description This method initializes the serial port.
	 */
	virtual LLATypes::lla_return_t DoInitialize();

private:

	LEDs::led_t _ledId;	// the port being interfaced with
	bool _on;
};

}


#endif /* INCLUDE_LOW_LEVEL_ABSTRACTION_LED_HPP_ */

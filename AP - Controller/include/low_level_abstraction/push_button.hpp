/*
 * push_button.hpp
 *
 *  Created on: Oct 14, 2016
 *      Author: singhh23
 */

#ifndef INCLUDE_LOW_LEVEL_ABSTRACTION_PUSH_BUTTON_HPP_
#define INCLUDE_LOW_LEVEL_ABSTRACTION_PUSH_BUTTON_HPP_


#include "low_level_abstraction/abstract_hardware.hpp"
#include "low_level_abstraction/hw_config.hpp"

namespace WirelessControl {

/**
 * @details This class acts as a user interface to an arbitrary push button.
 * @note This class acts as a latching push button - ie, this class stores
 * the 'wasPressed' status, not the 'isPressed' status.
 * Note also that debouncing has NOT BEEN IMPLEMENTED.
 */
class PushButtonInterface : public AbstractHardware {

public:

	/**
	 * @details This method constructs an interface to a serial port.
	 * @param[in] baudRate The given baudrate to initalize the serial port with.
	 */
	PushButtonInterface(PushButtons::push_button_t id)
		: _portId(id),
		  _wasPressed(false) {};

	virtual ~PushButtonInterface() {};

	/**
	 * @details Returns the status of the push button.
	 * @return TRUE if the push button was pressed at any point since last
	 * calling this function, FALSE otherwise.
	 * @note After calling this function, the status of the push button is
	 * reset to not pressed.
	 */
	bool ButtonWasPressed();

	/**
	 * @details Start the operation of this push button.
	 */
	LLATypes::lla_return_t Start();

protected:

	/**
	 * @description This method initializes the button.
	 */
	virtual LLATypes::lla_return_t DoInitialize();

	static void PressedCallback(void* classObj) {
		PushButtonInterface* obj = static_cast<PushButtonInterface*>(classObj);
		obj->_wasPressed = true;
	}

private:

	PushButtons::push_button_t _portId;	// the button being interfaced with
	bool _wasPressed;
};

}


#endif /* INCLUDE_LOW_LEVEL_ABSTRACTION_PUSH_BUTTON_HPP_ */

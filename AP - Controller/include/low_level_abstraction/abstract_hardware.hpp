#ifndef INCLUDE_LOW_LEVEL_ABSTRACTION_ABSTRACT_HARDWARE_HPP_
#define INCLUDE_LOW_LEVEL_ABSTRACTION_ABSTRACT_HARDWARE_HPP_

#include "common.hpp"

namespace WirelessControl {

// This class defines the interfaces required by all abstract hardware.
class AbstractHardware {

  public:
	AbstractHardware()
	    : _initialized(false){};
	virtual ~AbstractHardware(){};

	// General initialization interface for hardware. Inheriting classes have to implement
	// DoInitialize.
	inline LLATypes::lla_return_t Initialize() {
		if (_initialized) { return LLATypes::SUCCESS; }

		if (DoInitialize() == LLATypes::SUCCESS) {
			_initialized = true;
			return LLATypes::SUCCESS;
		}

		return LLATypes::FAILURE;
	}

  protected:
	// Hardware-specific initialization routine, must be implemented.
	virtual LLATypes::lla_return_t DoInitialize() = 0;
	bool _initialized;
};

} // namespace WirelessControl

#endif /* INCLUDE_LOW_LEVEL_ABSTRACTION_ABSTRACT_HARDWARE_HPP_ */

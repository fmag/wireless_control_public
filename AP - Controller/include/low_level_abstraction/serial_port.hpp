/*
 * serial_port.hpp
 *
 *  Created on: Jul 28, 2016
 *      Author: singhh23
 */

#ifndef INCLUDE_LOW_LEVEL_ABSTRACTION_SERIAL_PORT_HPP_
#define INCLUDE_LOW_LEVEL_ABSTRACTION_SERIAL_PORT_HPP_

#include "low_level_abstraction/abstract_hardware.hpp"
#include "low_level_abstraction/hw_config.hpp"

namespace WirelessControl {

/**
 * @details This class acts as a user interface to an arbitrary serial port.
 */
class SerialPortInterface : public AbstractHardware {

public:

	/**
	 * @details This method constructs an interface to a serial port.
	 * @param[in] baudRate The given baudrate to initalize the serial port with.
	 */
	SerialPortInterface(SerialPorts::serial_port_t id, SerialPorts::baud_rate_t baudRate)
		: _portId(id),
		  _baudRate(baudRate) {};

	virtual ~SerialPortInterface() {};

	/**
	 * @details Write data to the serial port.
	 * @param[in] data The stream from which data should be written (only
	 * needs to persist until function returns).
	 * @param[in] length The length of data to be transferred.
	 * @return ERROR if failed - use GetSerialPortStatus to get error codes
	 */
	LLATypes::lla_return_t Write(uint8_t *data, const uint16_t length);

	/**
	 * @details Read a single byte of data from the serial port.
	 * @param[out] byte The byte of data retrieved
	 * @return ERROR if failed - use GetSerialPortStatus to get error codes.
	 */
	LLATypes::lla_return_t GetByte(uint8_t &byte);

	/**
	 * @details This method gets the current serial port being interfaced with.
	 */
	SerialPorts::serial_port_t GetId() {
		return _portId;
	}

	/**
	 * @details Get the amount of rx bytes pending on this port.
	 * @return The number of bytes waiting to be read.
	 */
	uint16_t GetRxStatus();

	/**
	 * @details Get the amount of tx bytes pending on this port.
	 * @return The number of bytes waiting to be sent.
	 */
	uint16_t GetTxStatus();

	/**
	 * @details Flush the serial port, erasing all pending rx bytes.
	 */
	LLATypes::lla_return_t Flush();

protected:

	/**
	 * @description This method initializes the serial port.
	 */
	virtual LLATypes::lla_return_t DoInitialize();

private:

	SerialPorts::serial_port_t _portId;	// the port being interfaced with
	SerialPorts::baud_rate_t _baudRate;	// the baudrate for communication
};

}


#endif /* INCLUDE_LOW_LEVEL_ABSTRACTION_SERIAL_PORT_HPP_ */

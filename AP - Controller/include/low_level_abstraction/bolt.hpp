#ifndef INCLUDE_LOW_LEVEL_ABSTRACTION_BOLT_HPP_
#define INCLUDE_LOW_LEVEL_ABSTRACTION_BOLT_HPP_

#include "low_level_abstraction/abstract_hardware.hpp"
#include "drivers_msp432/hal.h"

namespace WirelessControl {

/**
 * @details This class acts as a user interface to the bolt protocol.
 */
class BoltInterface : public AbstractHardware {

public:

	/**
	 * @details This method constructs an object which interfaces to bolt.
	 */
	BoltInterface() {};
	virtual ~BoltInterface() {};

	/**
	 * @details Write a certain amount of data to the bolt interface.
	 * @param[in] data The data to be written.
	 * @param[in] numBytes The number of bytes to be written.
	 */
	void Write(uint8_t *data, const uint8_t numBytes);

	/**
	 * @details Read data from the bolt interface
	 * @param[out] buffer The location to read the data to.
	 * @param[out] numBytes The number of bytes read.
	 * @note Ensure that buffer is at least BOLT_MAX_MSG_LEN in size.
	 */
	void Read(uint8_t *buffer, uint8_t& numBytes);

	void Flush(void);

protected:
	/**
	 * @description This method initializes the bolt interface.
	 */
	virtual LLATypes::lla_return_t DoInitialize();

private:
	uint8_t _txBuffer[BOLT_MAX_MSG_LEN];
	uint8_t _rxBuffer[BOLT_MAX_MSG_LEN];
};

}

#endif /* INCLUDE_LOW_LEVEL_ABSTRACTION_BOLT_HPP_ */

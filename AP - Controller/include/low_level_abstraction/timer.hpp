/*
 * timer.hpp
 *
 *  Created on: May 10, 2016
 *      Author: singhh23
 */

#ifndef INCLUDE_LOW_LEVEL_ABSTRACTION_TIMER_HPP_
#define INCLUDE_LOW_LEVEL_ABSTRACTION_TIMER_HPP_

#include "low_level_abstraction/abstract_hardware.hpp"
#include "low_level_abstraction/hw_config.hpp"

namespace WirelessControl {

class TimerListenerInterface; 		// forward declaration

/**
 * @breif This class acts as a user interface to a timer.
 * @details To use a timer, a class must first be created which
 * inherits from the TimerListenerInterface class and implements the requisite
 * callback function. After this, one may create a TimerInterface object and
 * call the SetListener method while passing in the relavent class object.
 */

class TimerInterface : public AbstractHardware {

public:

	/**
	 * @details This method constructs an object which interfaces to the selected timer.
	 * @param[in] num The timer to be interfaced with.
	 */
	TimerInterface(Timers::timer_t num) :
		_timerNum(num),
		_listener(0) {

	};

	virtual ~TimerInterface() {};

	/**
	 * @details This method starts the timer with the given period.
	 * @param[in] msPeriod The period in ms that should elapse before a callback occurs.
	 */
	LLATypes::lla_return_t Start(uint32_t msPeriod);
	void Halt();

	/**
	 * @details This method sets the listener for this timer. Only one listener may be assigned to a single
	 * timer (however multiple timers may be assigned to a single listener)
	 * @param[in] listener The listener object that is waitin for this timer.
	 */
	inline void SetListener(TimerListenerInterface * listener) {
		_listener = listener;
	}

	/**
	 * @details This method is automatically called when the given period elapses. It then calls the
	 * listener PeriodElapsedCallback with the correct timer id.
	 * @note This function is required between the static 'TimerElapsed' and the
	 * 'PeriodElapsedCallback' functions to preserve the 'private' status of the
	 * _listener member variable.
	 */
	void NotifyListener();

protected:

	/**
	 * @description This method initializes the given timer. This must be called before using any other method.
	 */
	virtual LLATypes::lla_return_t DoInitialize();

private:
	Timers::timer_t _timerNum;
	TimerListenerInterface * _listener;

};


}



#endif /* INCLUDE_LOW_LEVEL_ABSTRACTION_TIMER_HPP_ */

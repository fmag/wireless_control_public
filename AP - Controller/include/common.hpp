#ifndef INCLUDE_COMMON_HPP_
#define INCLUDE_COMMON_HPP_

#define __BSD_VISIBLE // allow for BSD features (eg. M_PI constant)

#include "assert.h"
#include "config.hpp"
#include "drivers_msp432/board_config.h"
#include "drivers_msp432/uart.h"
#include "math.h"
#include "stdint.h"
#include "stdio.h"

#define MS_TO_HZ(ms) (1000 / (ms))
#define HZ_TO_MS(hz) (1000 / (hz))

#define ASSERT_PRINT()                                                                             \
	{                                                                                              \
		char stringBuffer[200];                                                                    \
		int bytes = sprintf(stringBuffer, "\n\n Assertion at %s:%u\n\n", __FILE__, __LINE__);      \
		while (DRV_UART_Write(UART_BUS_0, (uint8_t *)stringBuffer, bytes) != 0)                    \
			;                                                                                      \
		assert(0);                                                                                 \
	}

#define NUM_ELEMS(a) (sizeof(a) / sizeof(a[0]))

namespace WirelessControl {

/*****************************************************************************/
/* Enums *********************************************************************/
/*****************************************************************************/

namespace PendulumDimensions {
typedef enum { STATE = 4, CONTROL = 1, OUTPUT = 2 } pendulumDimensions_t;
}

namespace LLATypes {
typedef enum { BUSY = 2, SUCCESS = 1, FAILURE = 0 } lla_return_t;
}

/*****************************************************************************/
/* Structures and Types ******************************************************/
/*****************************************************************************/
typedef float data_t;

typedef struct __attribute__((packed)) schedIdPkt_t_tag {
	uint8_t id;
	uint8_t SPItrash[2]; // TODO: Bolt currently gives 2 trash bytes at the end. Fix API.
} schedIdPkt_t;

typedef struct bolt_data_pkt_t_tag {
	uint16_t senderID;
	uint8_t data[LWB_DATA_PKT_PAYLOAD_LEN];
	uint8_t SPItrash[2]; // TODO: Bolt currently gives 2 trash bytes at the end. Fix API.
	// TODO: LWB_CONF_MAX_DATA_PKT_LEN is currently 3 bytes bigger than necessary due to (recipient)
	// and stream information.
} bolt_data_pkt_t;

typedef struct controlRequirements_t_tag {
	data_t x_min;
	data_t x_max;
	data_t a_min;
	data_t a_max;
} controlRequirements_t;

struct state_t {
	state_t() {
		for (int i = 0; i < PendulumDimensions::STATE; i++) { data[i] = 0; }
	}
	data_t data[PendulumDimensions::STATE];
};

struct control_t {
	control_t() {
		for (int i = 0; i < PendulumDimensions::CONTROL; i++) { data[i] = 0; }
	}
	data_t data[PendulumDimensions::CONTROL];
};

struct measure_t {
	measure_t() {
		for (int i = 0; i < PendulumDimensions::OUTPUT; i++) { data[i] = 0; }
	}
	data_t data[PendulumDimensions::OUTPUT];
};

/*****************************************************************************/
/* Global Variables **********************************************************/
/*****************************************************************************/

extern const controlRequirements_t lowReq, highReq;

namespace EncoderConstants {
static const int counts_per_rev = 4096;
static const double pos_enc_resolution = 0.00002275;
static const double ang_enc_resolution = 2 * M_PI / counts_per_rev;
} // namespace EncoderConstants

/*****************************************************************************/
/* Functions *****************************************************************/
/*****************************************************************************/


// This function checks whether two floats are within epsilon distance
bool isEqual(float a, float b);

// This function checks whether two doubles are within epsilon distance
bool isEqual(double a, double b);

// This function converts from a counter to a position (in mm).
double countToPosition(uint16_t count);

// This function converts from a counter to an angle (in rad).
double countToAngle(uint16_t count);

// This function converts an angle from radians to degrees.
float radToDeg(float angleRad);

// This function converts an angle from degrees to radians.
float degToRad(float angleDeg);

} // namespace WirelessControl


#endif /* INCLUDE_COMMON_HPP_ */

#ifndef INCLUDE_MODES_TIMEOBSERVER_HPP_
#define INCLUDE_MODES_TIMEOBSERVER_HPP_

#include "driverlib/MSP432P4xx/timer32.h"
#include "stdint.h"

namespace WirelessControl {

class TimeObserver {
  public:
	TimeObserver();

	void reset(uint32_t timerAddr);
	void waitUntilMS(uint32_t timerAddr, uint32_t ms);
	void setSamplingPoint(uint32_t t);
	uint32_t ticksPassed(uint32_t timerAddr);

	uint32_t getStartTime(void) {
		return startTime;
	}

	uint32_t timerValue(uint32_t timerAddr) {
		return Timer32_getValue(timerAddr);
	}

	// Counts the number of setSamplingPoint calls between reset calls.
	unsigned int counter;

  private:
	const uint32_t startTime;
};

} // namespace WirelessControl

#endif /* INCLUDE_MODES_TIMEOBSERVER_HPP_ */

#ifndef INCLUDE_MODES_REMOTESTAB1LOOP_HPP_
#define INCLUDE_MODES_REMOTESTAB1LOOP_HPP_

#include "common.hpp"
#include "modes/mode.hpp"

namespace WirelessControl {

class ControlManager;

class RemoteStab1Loop : public Mode {
  public:
	RemoteStab1Loop(ControlManager *manager, unsigned int samplingRateHz,
	                unsigned int communicationRateHz, uint16_t plantIdArr[], unsigned int numPlants,
	                int controllerId);
	~RemoteStab1Loop(){};

	void PeriodElapsedCallback(Timers::timer_t id);

  protected:
	void setupGeneral(void);
	void setupController(void);
	void setupPlant(void);
	void setupRelay(void);
	void operationController(void);
	void operationPlant(void);
	void operationRelay(void);
	void teardownPlant(void);

  private:
	bool inStabilizationRange; // TODO: move to pendulum
};

} // namespace WirelessControl

#endif /* INCLUDE_MODES_REMOTESTAB1LOOP_HPP_ */

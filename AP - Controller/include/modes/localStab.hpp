#ifndef INCLUDE_MODES_LOCALSTAB_HPP_
#define INCLUDE_MODES_LOCALSTAB_HPP_

#include "common.hpp"
#include "modes/mode.hpp"

namespace WirelessControl {

class ControlManager;

class LocalStab : public Mode {
  public:
	LocalStab(ControlManager *manager, unsigned int samplingRateHz, uint16_t plantIdArr[],
	          unsigned int numPlants);
	~LocalStab(){};

	void PeriodElapsedCallback(Timers::timer_t id);

  protected:
	void setupGeneral(void);
	void setupPlant(void);
	void setupRelay(void);
	void operationRelay(void);
	void teardownPlant(void);

  private:
	uint8_t inStabilizationRange; // TODO move or remove?
};

} // namespace WirelessControl

#endif /* INCLUDE_MODES_LOCALSTAB_HPP_ */

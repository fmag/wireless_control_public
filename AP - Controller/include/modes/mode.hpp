#ifndef INCLUDE_MODES_MODE_HPP_
#define INCLUDE_MODES_MODE_HPP_

#include "common.hpp"
#include "low_level_abstraction/timer_listener.hpp"
#include "stdint.h"

namespace WirelessControl {

class ControlManager;

class Mode : public TimerListenerInterface {
  public:
	enum Role { Controller, Plant, Relay };

	Mode(ControlManager *manager)
	    : manager(manager)
	    , controllerId(-1){};
	virtual ~Mode(){};

	void setup(void) {
		setupGeneral();
		if (role == Mode::Controller)
			setupController();
		else if (role == Mode::Plant)
			setupPlant();
		else
			setupRelay();
	};
	void operation(void) {
		operationGeneral();
		if (role == Mode::Controller)
			operationController();
		else if (role == Mode::Plant)
			operationPlant();
		else
			operationRelay();
	};
	void teardown(void) {
		teardownGeneral();
		if (role == Mode::Controller)
			teardownController();
		else if (role == Mode::Plant)
			teardownPlant();
		else
			teardownRelay();
	};

	// This callback realizes the sampling interval and is only called by plant nodes.
	virtual void PeriodElapsedCallback(Timers::timer_t id) = 0;


  protected:
	int16_t getIndex(const uint16_t id) {
		int16_t idx;
		for (idx = 0; idx < numPlants; idx++) {
			if (id == plantIds[idx]) { return idx; }
		}
		return -1;
	}

	void setNodeRole(unsigned int nodeId) {
		if (nodeId == controllerId)
			role = Mode::Controller;
		else if (myIdx >= 0)
			role = Mode::Plant;
		else
			role = Mode::Relay;
	}

	virtual void setupGeneral(void){};
	virtual void setupController(void){};
	virtual void setupPlant(void){};
	virtual void setupRelay(void){};
	virtual void operationGeneral(void){};
	virtual void operationController(void){};
	virtual void operationPlant(void){};
	virtual void operationRelay(void){};
	virtual void teardownGeneral(void){};
	virtual void teardownController(void){};
	virtual void teardownPlant(void){};
	virtual void teardownRelay(void){};

	static state_t currState[MAX_PLANTS]; // + MAX_CONTROLLERS];
	static state_t predState[MAX_PLANTS]; // + MAX_CONTROLLERS];
	static control_t control[MAX_PLANTS]; // + MAX_CONTROLLERS];
	static control_t syncControl[MAX_PLANTS];
	static control_t prevControl[MAX_PLANTS]; // + MAX_CONTROLLERS];
	static state_t desState;
	static int16_t myIdx;
	static Mode::Role role;
	static bool receivedData;
	static volatile bool callbackFlag; // TODO: check if other variables need volatile attribute
	static uint32_t nextSamplingPoint;

	ControlManager *manager;
	uint16_t plantIds[MAX_PLANTS]; // + MAX_CONTROLLERS];
	uint16_t numPlants;
	int controllerId;
	unsigned int samplingRateHz;
	unsigned int samplingRateTicks;
	unsigned int communicationRateHz;
    float synclineSamplingOffset;
};

} // namespace WirelessControl

#endif /* INCLUDE_MODES_MODE_HPP_ */

#ifndef INCLUDE_MODES_LOCALSTABREMOTESYNC_HPP_
#define INCLUDE_MODES_LOCALSTABREMOTESYNC_HPP_

#include "common.hpp"
#include "modes/mode.hpp"

namespace WirelessControl {

class ControlManager;

class LocalStabRemoteSync : public Mode {
  public:
	LocalStabRemoteSync(ControlManager *manager, unsigned int samplingRateHz,
	                    unsigned int communicationRateHz, uint16_t plantIdArr[],
	                    unsigned int numPlants);
	~LocalStabRemoteSync(){};

	void PeriodElapsedCallback(Timers::timer_t id);

  protected:
	void setupGeneral(void);
	void setupPlant(void);
	void setupRelay(void);
	void operationPlant(void);
	void operationRelay(void);
	void teardownPlant(void);

  private:
	uint8_t inStabilizationRange[MAX_PLANTS];
};

} // namespace WirelessControl

#endif /* INCLUDE_MODES_LOCALSTABREMOTESYNC_HPP_ */

/*
 * t32.h
 *
 *  Created on: May 11, 2016
 *      Author: singhh23
 */

#ifndef INCLUDE_DRIVERS_MSP432_T32_H_
#define INCLUDE_DRIVERS_MSP432_T32_H_

#include "drivers_msp432/board_config.h"

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @details This function initializes the 32-bit timer interfaces, setting up the proper configurations and clock systems.
 * @return T32_OK
 */
drv_status_t DRV_T32_Init();

/**
 * @details This function connects a particular timer with a callback function and the related lla object.
 * @param[in] timer The timer to be setup.
 * @param[in] callback The callback function to be used when this timer elapses.
 * @param[in] usrObj The related TimerInterface lla object that this timer represents. Note that this
 * is required to be able to correctly resolve listener interfaces without hardcoding relations in the lla project.
 * @return T32_OK
 */
drv_status_t DRV_T32_Setup(t32_timer_t timer, timer_callback_t callback, void *usrObj);

/**
 * @details This function starts the timer with the given period.
 * @param[in] timer The timer to be started.
 * @param[in] msPeriod The period in ms that should elapse before a callback occurs.
 * @param[in] usrObj The related TiemrInterface lla object that this timer represents.
 * This must be the same object passed in when setting up the module.
 * @return T32_ERROR is msPeriod is out of range, T32_OK otherwise.
 */
drv_status_t DRV_T32_Start(t32_timer_t timer, uint32_t msPeriod, void *usrObj);

void DRV_T32_Halt(t32_timer_t timer);


#ifdef __cplusplus
}
#endif

#endif /* INCLUDE_DRIVERS_MSP432_T32_H_ */

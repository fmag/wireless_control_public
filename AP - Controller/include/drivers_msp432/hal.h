/*
 * Copyright (c) 2016, Swiss Federal Institute of Technology (ETH Zurich).
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Author:  Reto Da Forno
 */


#ifndef __HAL_H__
#define __HAL_H__

#include "drivers_msp432/board_config.h"
//#define MSP_EXP432P401R     // uncomment to run the program on the TI MSP‐EXP432P401R LaunchPad

// gpio
#ifndef MSP_EXP432P401R
// DPP pin assignment
#define LED_0               PORT2, PIN0
#define LED_STATUS          LED_0
#define LED_ERROR           LED_0
#define MCU_INT1            PORT7, PIN3
#define MCU_INT2            PORT7, PIN2
#define VCC_SEL             PORT7, PIN3             // LDO Vset for dynamic voltage scaling (1 = 3V, 0 = 1.8V)
#define DEBUG_SWITCH        PORT2, PIN7
#define EXT_GPIO1           PORT3, PIN0
#define EXT_GPIO2           PORT3, PIN1
#define EXT_UART_TXD        PORT3, PIN2
#define EXT_UART_RXD        PORT3, PIN3
#define EXT_SPI_EN          PORT3, PIN4
#define EXT_SPI_CLK         PORT3, PIN5
#define EXT_SPI_SIMO        PORT3, PIN6
#define EXT_SPI_SOMI        PORT3, PIN7
#define EXT_ADC1            PORT8, PIN2
#define EXT_ADC2            PORT8, PIN6
#else /* MSP_EXP432P401R */
// pin assigment for LaunchPad
#define LED_0               PORT1, PIN0
#define LED_STATUS          LED_0
#define LED_ERROR           LED_0
#define LED_RED             PORT2, PIN0
#define LED_GREEN           PORT2, PIN1
#define LED_BLUE            PORT2, PIN2
#define DEBUG_SWITCH_1      PORT1, PIN1
#define DEBUG_SWITCH_2      PORT1, PIN4
#endif /* MSP_EXP432P401R */

// power management
#define SVS_ENABLE          0                       // enable the SVS (both, SVSHM and SVSL)
#define SVS_LOW_PERF        1                       // enable the low performance / low power mode

// clocks
#define HFXTCLK_SPEED       48000000                // external high-frequency crystal oscillator clock speed [Hz]
#define LFXTCLK_SPEED       32768                   // external low-frequency crystal oscillator clock speed [Hz]
#define DCOCLK_SPEED        4000000                 // internal digitally-controlled oscillator (max. 48 MHz)
#define REFOCLK_SPEED       32768                   // 32 or 128 kHz
#define MCLK_SRC            CS_HFXTCLK_SELECT       // main/master clock source
#define MCLK_DIV            CS_CLOCK_DIVIDER_1      // main clock divider (1..128)
#define MCLK_SPEED          HFXTCLK_SPEED           // main clock speed [Hz], max. 48 MHz
#define SMCLK_SRC           CS_HFXTCLK_SELECT       // sub-system master clock
//#define SMCLK_DIV           CS_CLOCK_DIVIDER_16     //
//#define SMCLK_SPEED         (HFXTCLK_SPEED / 16)    // 3 MHz
#define HSMCLK_SRC          CS_HFXTCLK_SELECT       // high-speed sub-system master clock
#define HSMCLK_DIV          CS_CLOCK_DIVIDER_4      //
#define HSMCLK_SPEED        (HFXTCLK_SPEED / 4)     // 12 MHz
#define ACLK_SRC            CS_LFXTCLK_SELECT       // auxillary clock
#define ACLK_DIV            CS_CLOCK_DIVIDER_1      //
#define ACLK_SPEED          LFXTCLK_SPEED           // max. 128 kHz
#define BCLK_SRC            CS_LFXTCLK_SELECT       // low-speed backup domain clock, LFXT or REFO
#define BCLK_DIV            CS_CLOCK_DIVIDER_1      //
#define BCLK_SPEED          LFXTCLK_SPEED           // max. 32 kHz

// timers
#define SYSTICK_PERIOD      (MCLK_SPEED / 100)      // max. value is 24-bit (~16M), this yiels a min. divider of 3 and a period of ~350ms (with a 48MHz MCLK); the divider should be an even number
#define TIMER32_PRESCALER   TIMER32_PRESCALER_16   // select 1, 16 or 256
#define TIMER32_SPEED       (MCLK_SPEED / ((TIMER32_PRESCALER == TIMER32_PRESCALER_256) ? 256 : (TIMER32_PRESCALER == TIMER32_PRESCALER_16 ? 16 : 1)))  // number of ticks per second

#define PWM_PERIOD          ( SMCLK_SPEED / 1000 )  // TA1 is sourced by SMCLK (3 MHz) -> 1000 Hz refresh rate (LED on/off per second)
#define PWM_STEP_SIZE       ( PWM_PERIOD / 100 )     // step size for LED fading, determines the fading speed

// uart
#define UART_BAUDRATE       115200
#define UART_CLKSRC         EUSCI_A_UART_CLOCKSOURCE_SMCLK
#define UART_CLKSRC_SPEED   SMCLK_SPEED
#define UART_MODULE         EUSCI_A0_BASE
#define UART_RXD            PORT1, PIN2
#define UART_TXD            PORT1, PIN3
#define UART_A2_RXD         PORT3, PIN2
#define UART_A2_TXD         PORT3, PIN3

// spi
#define SPI_CPOL            0
#define SPI_CPHA            0
#define SPI_CPOL            0                       // clock polarity (0 = inactive low)
#define SPI_CPHA            0                       // clock phase
#define SPI_CLK_SRC         UCSSEL__SMCLK           // must be UCSSEL__ACLK or UCSSEL__SMCLK (don't forget to adjust SPI_CLK_SRC_SPEED!)
#define SPI_CLK_SRC_SPEED   SMCLK_SPEED             // adjust this according to the SPI_CLK_SRC selection
#define SPI_A3_EN           PORT9, PIN4
#define SPI_A3_CLK          PORT9, PIN5
#define SPI_A3_SOMI         PORT9, PIN6
#define SPI_A3_SIMO         PORT9, PIN7
#define SPI_B0_EN           PORT4, PIN2
#define SPI_B0_CLK          PORT1, PIN5
#define SPI_B0_SOMI         PORT1, PIN7
#define SPI_B0_SIMO         PORT1, PIN6
#define SPI_FAST_READ       0

// i2c
#define I2C_CLK_SPEED       100000

// flash memory
#define FLASH_START         0x00000000
#define FLASH_SIZE          0x00040000              // 256kB
#define FLASH_BANK_SIZE     0x00020000              // 1 bank = 128kB
#define FLASH_SECTOR_SIZE   0x00001000              // size of one sector/page (4kB)


// bolt
#define BOLT_USE_DMA        0
#define BOLT_TIMEREQ_PIN    PORT4, PIN2
#define BOLT_SCLK_SPEED     (SMCLK_SPEED/2)
#define BOLT_MAX_MSG_LEN    64                      // bytes
#ifdef MSP_EXP432P401R
// pin mapping for the TI LaunchPad
#define BOLT_ACK_PIN        PORT2, PIN6
#define BOLT_IND_PIN        PORT2, PIN5
#define BOLT_FUTURE_USE_PIN PORT6, PIN7
#define BOLT_IND_OUT_PIN    PORT2, PIN7
#define BOLT_MODE_PIN       PORT2, PIN4
#define BOLT_REQ_PIN        PORT2, PIN3
#else /* MSP_EXP432P401R */
// pin mapping for the DPP
#define BOLT_ACK_PIN        PORT4, PIN7
#define BOLT_IND_PIN        PORT4, PIN4
#define BOLT_FUTURE_USE_PIN PORT10, PIN0
#define BOLT_IND_OUT_PIN    PORT4, PIN3
#define BOLT_MODE_PIN       PORT4, PIN5
#define BOLT_REQ_PIN        PORT4, PIN6
#endif /* MSP_EXP432P401R */


#endif /* __HAL_H__ */


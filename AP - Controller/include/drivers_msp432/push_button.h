/*
 * push_button.h
 *
 *  Created on: Oct 12, 2016
 *      Author: singhh23
 */

#ifndef INCLUDE_DRIVERS_MSP432_PUSH_BUTTON_H_
#define INCLUDE_DRIVERS_MSP432_PUSH_BUTTON_H_


#include "drivers_msp432/board_config.h"

#ifdef __cplusplus
extern "C" {
#endif

#define PB_NUM_PORTS	2		// only ports 1/2 (and PA == P1) can be used
#define PB_NUM_PINS		8		// only 8 pins of interest per port

typedef struct {
	void* classObj[PB_NUM_PINS];
	void(*callbackArray[PB_NUM_PINS])(void *obj);
} drv_pb_port_handle_t;

extern drv_pb_port_handle_t portCallbacks[PB_NUM_PORTS];

/**
 * @description Initialize the push button module.
 * @return DRV_OK
 */
drv_status_t DRV_PB_Init();

/**
 * @description Register callback function for button interrupt.
 * @param[in] buttonNum The pushbutton to enable interrupts on.
 * @param[in] fun The callback function to be called when interrupt occurs.
 * @note Each port may only be mapped to a single interrupt - further handling
 * must be done if the specific pins should be handled differently.
 */
drv_status_t DRV_PB_RegisterInterrupt(drv_pb_device_t buttonNum,
		void *classObj, void(*fun)(void *obj));

drv_status_t DRV_PB_EnableInterrupt(drv_pb_device_t buttonNum);

#ifdef __cplusplus
}
#endif


#endif /* INCLUDE_DRIVERS_MSP432_PUSH_BUTTON_H_ */

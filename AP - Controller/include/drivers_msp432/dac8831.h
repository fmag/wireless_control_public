/*
 * dac8831.h
 *
 *  Created on: Jun 15, 2016
 *      Author: singhh23
 */

#ifndef INCLUDE_DRIVERS_MSP432_DAC8831_H_
#define INCLUDE_DRIVERS_MSP432_DAC8831_H_

#include "drivers_msp432/board_config.h"
#include "drivers_msp432/spi.h"

#ifdef __cplusplus
extern "C" {
#endif

#define VREF 3.3
#define AMP_GAIN 3
#define MAX_PERCT 100
#define MIN_PERCT -100

/**
 * @details The components required to properly define a dac8831 device.
 */
typedef struct {
	/** The spi bus used by this device. */
	spi_bus_t spiBus;

	/** The location of the chip select gpio pin. */
	gpio_handle_t * csLoc;

	/** The location of the amp transistor.	 */
	gpio_handle_t *ampLoc;

} drv_dac8831_handle_t;

/**
 * @details Initialize the interface between the MCU and the SPI peripheral.
 * @param[in] devNum The device number to be initialized.
 * @return OK if successful, ERROR otherwise.
 */
drv_status_t DRV_DAC8831_Init(drv_dac8831_device_t devNum);

/**
 * @details Start the DAC as required.
 * @param[in] devNum The device number to be started.
 * @return OK if successful, ERROR or BUSY otherwise.
 */
drv_status_t DRV_DAC8831_Start(drv_dac8831_device_t devNum);

/**
 * @details Set the output voltage of the DAC.
 * @param[in] devNum The device number of which to set the voltage.
 * @param[in] perct The percentage of max voltage to be set (MIN_PERCT to MAX_PERCT).
 * @return OK if successful, ERROR or BUSY otherwise.
 * @note This is a blocking function and will only return once the communication between
 * the master and slave is completed.
 */
drv_status_t DRV_DAC8831_SetVoltage(drv_dac8831_device_t devNum, int perct);

/**
 * @details Turn off the dac by disabling the relay.
 * @param[in] devNum The device number to disable.
 */
drv_status_t DRV_DAC8831_TurnOff(drv_dac8831_device_t devNum);


#ifdef __cplusplus
}
#endif



#endif /* INCLUDE_DRIVERS_MSP432_DAC8831_H_ */

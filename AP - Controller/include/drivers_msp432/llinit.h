/*
 * llinit.h
 *
 *  Created on: May 11, 2016
 *      Author: singhh23
 */

#ifndef INCLUDE_DRIVERS_MSP432_LLINIT_H_
#define INCLUDE_DRIVERS_MSP432_LLINIT_H_

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @details This function handles all the required initialization of low-level modules.
 */
void LowLevelInit(void);

#ifdef __cplusplus
}
#endif

#endif /* INCLUDE_DRIVERS_MSP432_LLINIT_H_ */

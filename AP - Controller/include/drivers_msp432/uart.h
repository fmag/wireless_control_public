/*
 * uart.h
 *
 *  Created on: Jul 25, 2016
 *      Author: singhh23
 */

#ifndef INCLUDE_DRIVERS_MSP432_UART_H_
#define INCLUDE_DRIVERS_MSP432_UART_H_

#include "drivers_msp432/board_config.h"

#ifdef __cplusplus
extern "C" {
#endif

// Define two circular buffer structs in the same manner but with pre-defined
// sizes in order to prevent use of dynamic memory allocation
#define _DRV_UART_TXBUFFER_SIZE 256
#define _DRV_UART_RXBUFFER_SIZE 256

// A circular buffer used to hold asynchronous data to be sent.
typedef struct DRV_UART_CircularBufferTx {
	volatile uint8_t data[_DRV_UART_TXBUFFER_SIZE];
	volatile uint8_t *head;
	volatile uint8_t *tail;
	volatile uint16_t size;
} circular_buffer_tx_t;

// A circular Buffer used to hold asynchronous data being received.
typedef struct DRV_UART_CircularBufferRx {
	volatile uint8_t data[_DRV_UART_RXBUFFER_SIZE];
	volatile uint8_t *head;
	volatile uint8_t *tail;
	volatile uint16_t size;
} circular_buffer_rx_t;

// CIRCULAR BUFFER HELPER FUNCTIONS
int cb_rx_flush(circular_buffer_rx_t *cb);
int cb_rx_pop(circular_buffer_rx_t *cb, volatile uint8_t *byte, uint16_t maxSize);
int cb_tx_pop(circular_buffer_tx_t *cb, volatile uint8_t *byte, uint16_t maxSize);

int cb_rx_push(circular_buffer_rx_t *cb, uint8_t byte, uint16_t maxSize);
int cb_tx_push(circular_buffer_tx_t *cb, uint8_t byte, uint16_t maxSize);

//int cb_rx_reset(circular_buffer_rx_t *cb, uint16_t maxSize);
//int cb_tx_reset(circular_buffer_tx_t *cb, uint16_t maxSize);

/**
 * @details The components required to properly define a uart bus.
 */
typedef struct {
	/** The configuration settings for this bus. */
	uart_bus_handle_t busHandle;

	/** A pointer to the data which should be written onto the bus. */
	circular_buffer_tx_t txBuffer;

	/** A pointer to the location where data should be written from the bus. */
	circular_buffer_rx_t rxBuffer;

	/** UART busy status. */
	bool busy;

} uart_handle_t;

/**
 * @details This function initializes the spi interface, setting up the correct ports/pins and configurations.
 * @param[in] baudRate The baud rate to initialize this interface to
 * @return SPI_OK if successfuly initialized, SPI_ERROR otherwise.
 */
drv_status_t DRV_UART_Init(uart_bus_t busId, drv_baud_rate_t baudRate);

/**
 * @details This function enables the communication interface for a specific uart bus.
 * @param[in] busId The bus to be started.
 * @return UART_OK
 */
drv_status_t DRV_UART_Start(uart_bus_t busId);

/**
 * @details This function writes a number of bytes to the given uart bus.
 * @param[in] busId The bus to be written to.
 * @param[in] data The data stream to be written from.
 * @param[in] size The amount of data to be written.
 * @return DRV_OK if operation queued successfully, DRV_BUSY if tx operation
 * is already pending, DRV_ERROR otherwise.
 */
drv_status_t DRV_UART_Write(uart_bus_t busId, uint8_t* data, const uint16_t size);

/**
 * @details This function returns a single byte of data from the uart bus.
 * @param[in] busId The bus to be read from.
 * @param[out] byte The byte which has been read.
 * @return DRV_OK if byte was read successfully, DRV_BUSY if rx buffer is overflowing,
 * note that 'byte' parameter is still valid in this case, however user should
 * understand that data may have been lost due to buffer overflow (read data faster
 * to solve). DRV_ERROR otherwise.
 */
drv_status_t DRV_UART_GetByte(uart_bus_t busId, uint8_t* byte);

/**
 * @details This function returns the amount of rx bytes on the specified bus.
 * @param[in] busId The bus whose status should be returned.
 * @return An number corresponding to amount of rx bytes waiting to be read.
 */
uint16_t DRV_UART_GetRxAmount(uart_bus_t busId);

/**
 * @details This function returns the amount of tx bytes on the specified bus.
 * @param[in] busId The bus whose status should be returned.
 * @return An number corresponding to amount of tx bytes waiting to be read.
 */
uint16_t DRV_UART_GetTxAmount(uart_bus_t busId);

/**
 * @details This function flushes all pending rx bytes on the specified bus.
 * @param[in] busId The bus whose rx buffer should be flushed.
 * @return DRV_OK
 */
drv_status_t DRV_UART_Flush(uart_bus_t busId);

/**
 * @details Function to provide easy linking to bolt api.
 */
void uart_println(uint8_t* buffer);


#ifdef __cplusplus
}
#endif



#endif /* INCLUDE_DRIVERS_MSP432_UART_H_ */

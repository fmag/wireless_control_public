/*
 * ls7366.h
 *
 *  Created on: May 17, 2016
 *      Author: singhh23
 */

#ifndef INCLUDE_DRIVERS_MSP432_LS7366_H_
#define INCLUDE_DRIVERS_MSP432_LS7366_H_

#include "drivers_msp432/spi.h"

#ifdef __cplusplus
extern "C" {
#endif

// Various defines for ls commands

//Count modes
#define NQUAD		0x00	//non-quadrature mode
#define QUADRX1		0x01	//X1 quadrature mode
#define QUADRX2		0x02	//X2 quadrature mode
#define QUADRX4		0x03	//X4 quadrature mode

//Running modes
#define FREE_RUN	0x00
#define SINGE_CYCLE	0x04
#define RANGE_LIMIT	0x08
#define MODULO_N	0x0C

//Index modes
#define DIS_INDX	0x00	//index_disabled
#define INDX_LOADC	0x10	//index_load_CNTR
#define INDX_RESETC	0x20	//index_rest_CNTR
#define INDX_LOADO	0x30	//index_load_OL
#define ASYNCH_INDX	0x00	//asynchronous index
#define SYNCH_INDX	0x80	//synchronous index

//Clock filter modes
#define FILTER_1	0x00	//filter clock frequncy division factor 1
#define FILTER_2	0x80	//filter clock frequncy division factor 2

/* MDR1 configuration data; any of these data segments can be ORed together */
//Flag modes
#define NO_FLAGS	0x00	//all flags disabled
#define IDX_FLAG	0x10	//IDX flag
#define CMP_FLAG	0x20	//CMP flag
#define BW_FLAG		0x40	//BW flag
#define CY_FLAG		0x80	//CY flag

//1 to 4 bytes data-width
#define BYTE_4		0x00	//four byte mode
#define BYTE_3		0x01	//three byte mode
#define BYTE_2		0x02	//two byte mode
#define BYTE_1		0x03	//one byte mode

//Enable/disable counter
#define EN_CNTR		0x00	//counting enabled
#define DIS_CNTR	0x04	//counting disabled

/* LS7366R op-code list */
#define CLR_MDR0	0x08
#define CLR_MDR1	0x10
#define CLR_CNTR	0x20
#define CLR_STR		0x30
#define READ_MDR0	0x48
#define READ_MDR1	0x50
#define READ_CNTR	0x60
#define READ_OTR	0x68
#define READ_STR	0x70
#define WRITE_MDR1	0x90
#define WRITE_MDR0	0x88
#define WRITE_DTR	0x98
#define LOAD_CNTR	0xE0
#define LOAD_OTR	0xE4

#define MDR0_SETUP QUADRX4|FREE_RUN|DIS_INDX
#define MDR1_SETUP NO_FLAGS|BYTE_2|EN_CNTR

/**
 * @details The components required to properly define an ls7366 device.
 */
typedef struct {
	/** The spi bus used by this device. */
	spi_bus_t spiBus;

	/** The location of the chip select gpio pin. */
	gpio_handle_t * csLoc;

} drv_ls7366_handle_t;

/**
 * @details Initialize the interface between the MCU and the SPI peripheral.
 * @param[in] devNum The device number to be initialized.
 * @return OK if successful, ERROR ottherwise.
 */
drv_status_t DRV_LS7366_Init(drv_ls7366_device_t devNum);

/**
 * @details Start the counter with the relavent parameters.
 * @param[in] devNum The device number to be started.
 * @return OK if successful, ERROR or BUSY otherwise.
 */
drv_status_t DRV_LS7366_Start(drv_ls7366_device_t devNum);

/**
 * @details Get the current count from the quadrature counter.
 * @param[in] devNum The device number to obtain the count from.
 * @param[in] count The location to store the count.
 * @return OK if successful, ERROR or BUSY otherwise.
 * @note This is a blocking function and will only return once the communication between
 * the master and slave is completed.
 */
drv_status_t DRV_LS7366_GetCount(drv_ls7366_device_t devNum, uint16_t* count);


#ifdef __cplusplus
}
#endif

#endif /* INCLUDE_DRIVERS_MSP432_LS7366_H_ */

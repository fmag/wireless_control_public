/*
 * spi.hpp
 *
 *  Created on: May 10, 2016
 *      Author: singhh23
 */

#ifndef INCLUDE_DRIVERS_MSP432_SPI_H_
#define INCLUDE_DRIVERS_MSP432_SPI_H_

#include "drivers_msp432/board_config.h"

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @details The components required to properly define an spi bus.
 */
typedef struct {
	/** The configuration settings for this bus. */
	spi_bus_handle_t busHandle;

	/** The amount of data which remains to be transferred on the bus. */
	uint8_t lengthRemaining;

	/** A pointer to the data which should be written onto the bus. */
	volatile uint8_t* txPointer;

	/** A pointer to the location where data should be written from the bus. */
	volatile uint8_t* rxPointer;

} spi_handle_t;

/**
 * @details This function initializes the spi interface, setting up the correct ports/pins and configurations.
 * @return SPI_OK if successfuly initialized, SPI_ERROR otherwise.
 */
drv_status_t DRV_SPI_Init();

/**
 * @details This function enables the communication interface for a specific spi bus.
 * @param[in] busId The bus to be started.
 * @return SPI_OK
 */
drv_status_t DRV_SPI_Start(spi_bus_t busId);

/**
 * @details This function writes a single byte to the specified spi bus and (blocking) waits for a single byte.
 * @param[in] busId The bus on which to write the byte.
 * @param[in] byte The byte to be written - set to 0x0 to ignore.
 * @param[in] ret The location to read a single byte - set to NULL to ignore.
 * @return SPI_OK if successful, SPI_ERROR or SPI_BUSY otherwise.
 */
drv_status_t DRV_SPI_WriteRead(spi_bus_t busId, volatile uint8_t byte, volatile uint8_t* ret);

/**
 * @details This function writes a set of characters to the specified bus and stores the response (non-blocking).
 * @param[in] busId The bus on which to communicate.
 * @param[in] txData A pointer to the character array (of LENGTH size) which should be written.
 * @param[out] rxData A pointer to the character array (of LENGTH size) where the response should be stored.
 * @param[in] length The size of the tx/rx buffers (must be the same).
 * @return SPI_OK if operation successfully queued, SPI_ERROR or SPI_BUSY otherwise.
 * @note This function assumes that the tx and rx character arrays are not read/written to until the
 * DRV_SPI_CheckStatus() function returns SPI_OK.
 */
drv_status_t DRV_SPI_WriteReadBlock(spi_bus_t busId, volatile uint8_t* txData,
		volatile uint8_t* rxData, uint16_t length);

/**
 * @details This function returns the status of the specified bus.
 * @param[in] busId The bus whose status should be returned.
 * @return SPI_OK if any pending read/write operations have concluded, SPI_BUSY otherwise.
 */
drv_status_t DRV_SPI_CheckStatus(spi_bus_t busId);


#ifdef __cplusplus
}
#endif

#endif /* INCLUDE_DRIVERS_MSP432_SPI_H_ */

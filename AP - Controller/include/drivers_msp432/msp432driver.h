/*
 * Copyright (c) 2016, Swiss Federal Institute of Technology (ETH Zurich).
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Author:  Reto Da Forno
 */

/*
 * a lightweight driver lib for the MSP432
 */


#ifndef __MSP432DRIVER_H__
#define __MSP432DRIVER_H__

/*
 * I N C L U D E S
 */

#include "stdio.h"
#include "stdint.h"
#include "string.h"
#include "driverlib/MSP432P4xx/rom.h"            // comment to enable the ROM version of the MAP_[...] functions
#include "driverlib/MSP432P4xx/rom_map.h"
#include "driverlib/MSP432P4xx/driverlib.h"      // use the TI driver lib
#include "drivers_msp432/hal.h"            // hardware abstraction layer (pin mappings)


/*
 * D E F A U L T   C O N F I G
 */

#ifndef DEBUG_PRINT_MAX_LEN
#define DEBUG_PRINT_MAX_LEN     255
#endif /* DEBUG_PRINT_MAX_LEN */

#ifndef DEBUG_PRINT_MAX_MOD_LEN
#define DEBUG_PRINT_MAX_MOD_LEN 15
#endif /* DEBUG_PRINT_MAX_MOD_LEN */

#ifndef DEBUG_PRINT_TIMESTAMP
#define DEBUG_PRINT_TIMESTAMP   0
#endif /* DEBUG_PRINT_TIMESTAMP */

#ifndef SVS_ENABLE
#define SVS_ENABLE              0
#endif /* SVS_ENABLE */

// whether or not to send a command byte before each I2C data transfer
#ifndef I2C_SEND_CMD_BYTE
#define I2C_SEND_CMD_BYTE       1
#endif /* I2C_SEND_CMD_BYTE */


/*
 * D E F I N I T I O N S
 */

#define PIN0    0
#define PIN1    1
#define PIN2    2
#define PIN3    3
#define PIN4    4
#define PIN5    5
#define PIN6    6
#define PIN7    7

#define PORT1   GPIO_PORT_P1
#define PORT2   GPIO_PORT_P2
#define PORT3   GPIO_PORT_P3
#define PORT4   GPIO_PORT_P4
#define PORT5   GPIO_PORT_P5
#define PORT6   GPIO_PORT_P6
#define PORT7   GPIO_PORT_P7
#define PORT8   GPIO_PORT_P8
#define PORT9   GPIO_PORT_P9
#define PORT10  GPIO_PORT_P10
#define PORTJ   GPIO_PORT_PJ


/*
 * M A C R O S
 */

#define MAX(x, y)                       ( (x) > (y) ? (x) : (y) )
#define MIN(x, y)                       ( (x) < (y) ? (x) : (y) )
#define PIN_TO_BIT(pin)                 (1 << pin)
#define REGVAL32(x)                     (*((volatile uint32_t *)((uint32_t)x)))
#define REGVAL16(x)                     (*((volatile uint16_t *)((uint32_t)x)))
#define REGVAL8(x)                      (*((volatile uint8_t *)((uint32_t)x)))

/* Note: all following macros ending with '_I' (immediate) can only be used
   when passing numbers directly (no defines or variables) */
#define PORT_XOR_I(port)                P##port##OUT ^= 0xff
#define PORT_SET_I(port)                P##port##OUT = 0xff
#define PORT_CLR_I(port)                P##port##OUT = 0x00
#define PORT_SEL_PRI_I(port)            { P##port##SEL0 = 0xff; P##port##SEL1 = 0x00; }
#define PORT_UNSEL_I(port)              { P##port##SEL0 = 0x00; P##port##SEL1 = 0x00; }
#define PORT_RES_EN_I(port)             P##port##REN = 0xff
#define PORT_CLR_IFG_I(port)            P##port##IFG = 0x00
#define PORT_CFG_OUT_I(port)            P##port##DIR = 0xff
#define PORT_CFG_IN_I(port)             P##port##DIR = 0x00
#define PIN_XOR_I(port, pin)            P##port##OUT ^= BIT##pin
#define PIN_SET_I(port, pin)            P##port##OUT |= BIT##pin
#define PIN_CLR_I(port, pin)            P##port##OUT &= ~BIT##pin
#define PIN_SEL_PRI_I(port, pin)        { P##port##SEL0 |= BIT##pin; P##port##SEL1 &= ~BIT##pin; }
#define PIN_UNSEL_I(port, pin)          { P##port##SEL0 &= ~BIT##pin; P##port##SEL1 &= ~BIT##pin; }
#define PIN_CFG_OUT_I(port, pin)        P##port##DIR |= BIT##pin
#define PIN_CFG_IN_I(port, pin)         P##port##DIR &= ~BIT##pin
#define PIN_CLR_IFG_I(port, pin)        P##port##IFG &= ~BIT##pin
#define PIN_RES_EN_I(port, pin)         P##port##REN |= BIT##pin
#define PIN_IES_RISING_I(port, pin)     P##port##IES &= ~BIT##pin
#define PIN_IES_FALLING_I(port, pin)    P##port##IES |= BIT##pin
#define PIN_IES_TOGGLE_I(port, pin)     P##port##IES ^= BIT##pin
#define PIN_INT_EN_I(port, pin)         P##port##IE |= BIT##pin
#define PIN_IFG_I(port, pin)            (P##port##IFG & (uint8_t)(BIT##pin))
#define PIN_GET_I(port, pin)            (P##port##IN & (uint8_t)(BIT##pin))
#define PIN_CFG_INT_I(port, pin, r)     { PIN_CFG_IN_I(port, pin); \
                                          if (r) { PIN_IES_RISING_I(port, pin); PIN_PULLUP_EN_I(port, pin); } \
                                          else { PIN_IES_FALLING_I(port, pin); PIN_PULLDOWN_EN_I(port, pin); } \
                                          PIN_CLR_IFG_I(port, pin); \
                                          PIN_INT_EN_I(port, pin); }
#define PIN_PULLUP_EN_I(port, pin)      { PIN_RES_EN_I(port, pin); PIN_SET_I(port, pin); }
#define PIN_PULLDOWN_EN_I(port, pin)    { PIN_RES_EN_I(port, pin); PIN_CLR_I(port, pin); }
#define PIN_MAP_I(port, pin, map)       { PMAPKEYID = PMAP_KEYID_VAL; \
                                          PMAPCTL |= PMAPRECFG; \
                                          *((volatile uint8_t*)&P##port##MAP01 + pin) = map; \
                                          PMAPKEYID = 0; }

#define PIN_XOR(p)                      PIN_XOR_I(p)                // toggle output bit/level
#define PIN_SET(p)                      PIN_SET_I(p)                // set output high
#define PIN_CLR(p)                      PIN_CLR_I(p)                // clear (set output low)
#define PIN_SEL_PRI(p)                  PIN_SEL_PRI_I(p)            // primary module function
#define PIN_UNSEL(p)                    PIN_UNSEL_I(p)              // unselect (configure port function)
#define PIN_CFG_OUT(p)                  PIN_CFG_OUT_I(p)            // configure pin as output
#define PIN_CFG_IN(p)                   PIN_CFG_IN_I(p)             // configure pin as input
#define PIN_CLR_IFG(p)                  PIN_CLR_IFG_I(p)            // clear interrupt flag
#define PIN_IES_RISING(p)               { PIN_IES_RISING_I(p); PIN_CLR_IFG_I(p); }    // interrupt edge select (always clear the IFG when changing PxIES)
#define PIN_IES_FALLING(p)              { PIN_IES_FALLING_I(p); PIN_CLR_IFG_I(p); }   // interrupt edge select
#define PIN_IES_TOGGLE(p)               { PIN_IES_TOGGLE_I(p); PIN_CLR_IFG_I(p); }    // interrupt edge toggle
#define PIN_INT_EN(p)                   PIN_INT_EN_I(p)             // enable port interrupt
#define PIN_CFG_INT(p, rising)          PIN_CFG_INT_I(p, rising)    // configure & enable port interrupt
#define PIN_IFG(p)                      PIN_IFG_I(p)                // get interrupt flag
#define PIN_GET(p)                      PIN_GET_I(p)                // get input bit/level
#define PIN_PULLUP_EN(p)                PIN_PULLUP_EN_I(p)          // enable pullup resistor (for input pins only)
#define PIN_PULLDOWN_EN(p)              PIN_PULLDOWN_EN_I(p)        // enable pulldown resistor (for input pins only)
#define PIN_MAP(p, map)                 PIN_MAP_I(p, map)

// USCI (pass the module to these macros, e.g. A3 or B0)
#define SPI_ENABLE(module)              ( UC##module##CTLW0 &= ~UCSWRST )
#define SPI_IS_ENABLED(module)          ( !(UC##module##CTLW0 & UCSWRST) )
#define SPI_DISABLE(module)             ( UC##module##CTLW0 |= UCSWRST )
#define SPI_WAIT_TXE(module)            while (!(UC##module##IFG & UCTXIFG))
#define SPI_WAIT_RXNE(module)           while (!(UC##module##IFG & UCRXIFG))
#define SPI_WAIT_BUSY(module)           while (UC##module##STATW & UCBUSY)
#define SPI_IS_BUSY(module)             ( UC##module##STATW & UCBUSY )
#define SPI_TRANSMIT_BYTE(module, b)    { SPI_WAIT_TXE(module); UC##module##TXBUF_SPI = (b); }
#define SPI_RECEIVE_BYTE(module, b)     { SPI_WAIT_RXNE(module); (b) = UC##module##RXBUF; }
#define SPI_CLR_RX(module)              ( UC##module##RXBUF )
#define UART_A0_SEND_BYTE(b)            { while (!(UCA0IFG & UCTXIFG)); UCA0TXBUF = b; }
#define USCI_BUSY(mod)                  ( UC##mod##STATW & UCBUSY )
#define I2C_B2_ACTIVE                   ( UCB2STATW & UCBBUSY )

// TIMER
#define TIMESTAMP                       timer32_now()                // clock ticks since bootup
#define TIMESTAMP_MS                    (timer32_now() / (TIMER32_SPEED / 1000))
#define RTC_STOP                        (RTCCTL1 |= RTCHOLD)
#define RTC_START                       (RTCCTL1 &= ~RTCHOLD)

// INTERRUPTS
#define MASTER_INTERRUPT_DISABLED       (CPU_primask() & 1)

// WATCHDOG
#define WATCHDOG_STOP                   (WDTCTL = WDTPW | WDTHOLD)

// POWER MANAGEMENT
#define PM_ENABLE_SLEEP_ON_ISR_EXIT     (SCB->SCR |= SCB_SCR_SLEEPONEXIT)
#define PM_DISABLE_SLEEP_ON_ISR_EXIT    (SCB->SCR &= ~SCB_SCR_SLEEPONEXIT)
#define PM_ENABLE_SRAM_BANKS            { __delay_cycles(10000); SYSCTL_SRAM_BANKEN |= SYSCTL_SRAM_BANKEN_BNK7_EN; }  // enable ALL the memory banks
#define PM_ENABLE_SRAM_STACK_RETENTION  (SYSCTL->SRAM_BANKRET |= SYSCTL_SRAM_BANKRET_BNK7_RET)
#define PM_ENABLE_FULL_SRAM_RETENTION   { __delay_cycles(10000); SYSCTL_SRAM_BANKRET |= (SYSCTL_SRAM_BANKRET_BNK1_RET | SYSCTL_SRAM_BANKRET_BNK2_RET | SYSCTL_SRAM_BANKRET_BNK3_RET | SYSCTL_SRAM_BANKRET_BNK4_RET | SYSCTL_SRAM_BANKRET_BNK5_RET | SYSCTL_SRAM_BANKRET_BNK6_RET | SYSCTL_SRAM_BANKRET_BNK7_RET); }
#define PM_ENTER_LPM0                   { SCB->SCR &= ~(SCB_SCR_SLEEPDEEP); __sleep(); __no_operation(); }
#define PM_ENTER_LPM3                   { SCB->SCR |= (SCB_SCR_SLEEPDEEP); __sleep(); __no_operation(); }
#define PM_ENTER_LPM4                   { RTC_STOP; WATCHDOG_STOP; PM_ENTER_DEEPSLEEP; }    // hold RTC and Watchdog, then enter deep sleep
#define PM_DISABLE_SVS                  { PSSKEY = PSS_KEY_KEY_VAL; PSSCTL0 |= SVSMHOFF | SVSLOFF; PSSKEY = 0; }
#define PM_ENABLE_RUDE_MODE             { PCMCTL1 = 0xa5960000 | FORCE_LPM_ENTRY; }  // allows to enter LPM3 without waiting for peripherals

// RESET
#define RESET_TRIGGER_HARD              (RSTCTL_RESET_REQ = (RESET_KEY | 0x00000002))
#define RESET_TRIGGER_SOFT              (RSTCTL_RESET_REQ = (RESET_KEY | 0x00000001))

// LED
#define LED_ON(pin)                     PIN_SET_I(pin)
#define LED_OFF(pin)                    PIN_CLR_I(pin)
#define LED_TOGGLE(pin)                 PIN_XOR_I(pin)
#ifdef MSP_EXP432P401R
#define LED_RGB_ON                      (TA0CCR1 = TA0CCR2 = TA0CCR3 = PWM_PERIOD)            // set the luminance for all three colors of the RGB LED to max. (white)
#define LED_RGB_OFF                     (TA0CCR1 = TA0CCR2 = TA0CCR3 = 0)                     // turn the RGB LED off
#define LED_RGB_SET(lum)                (TA0CCR1 = TA0CCR2 = TA0CCR3 = ((PWM_PERIOD * (uint8_t)lum + 1) >> 8))    // set the luminance for all three colors of the RGB LED, values btw. 0 and 255
#define LED_RED_SET(lum)                (TA0CCR1 = ((PWM_PERIOD * (uint8_t)lum + 1) >> 8))    // set the luminance for the red LED, values btw. 0 and 255
#define LED_GREEN_SET(lum)              (TA0CCR2 = ((PWM_PERIOD * (uint8_t)lum + 1) >> 8))    // set the luminance for the green LED, values btw. 0 and 255
#define LED_BLUE_SET(lum)               (TA0CCR3 = ((PWM_PERIOD * (uint8_t)lum + 1) >> 8))    // set the luminance for the blue LED, values btw. 0 and 255
#else /* MSP_EXP432P401R */
#define LED_RGB_ON                      (TA0CCR1 = TA0CCR2 = TA0CCR3 = 0)                                  // set the luminance for all three colors of the RGB LED to max. (white)
#define LED_RGB_OFF                     (TA0CCR1 = TA0CCR2 = TA0CCR3 = PWM_PERIOD)                         // turn the RGB LED off
#define LED_RGB_SET(lum)                (TA0CCR1 = TA0CCR2 = TA0CCR3 = PWM_PERIOD - ((PWM_PERIOD * (uint8_t)lum + 1) >> 8))    // set the luminance for all three colors of the RGB LED, values btw. 0 and 255
#define LED_RED_SET(lum)                (TA0CCR1 = PWM_PERIOD - ((PWM_PERIOD * (uint8_t)lum + 1) >> 8))    // set the luminance for the red LED, values btw. 0 and 255
#define LED_GREEN_SET(lum)              (TA0CCR3 = PWM_PERIOD - ((PWM_PERIOD * (uint8_t)lum + 1) >> 8))    // set the luminance for the green LED, values btw. 0 and 255
#define LED_BLUE_SET(lum)               (TA0CCR2 = PWM_PERIOD - ((PWM_PERIOD * (uint8_t)lum + 1) >> 8))    // set the luminance for the blue LED, values btw. 0 and 255
#endif /* MSP_EXP432P401R */

// CLOCKS
#define CLOCK_DISABLE                   (CSCLKEN &= ~(ACLK_EN | MCLK_EN | HSMCLK_EN | SMCLK_EN))  // disable conditional clock requests
#define CLOCK_ENABLE                    (CSCLKEN |= (ACLK_EN | MCLK_EN | HSMCLK_EN | SMCLK_EN))   // enable all clocks (-> this enables conditional clock requests)

#ifndef SCB_SCR_SLEEPDEEP
#define SCB_SCR_SLEEPDEEP               (0x00000004)
#endif /* SCB_SCR_SLEEPDEEP */

#ifndef SCB_SCR_SLEEPONEXIT
#define SCB_SCR_SLEEPONEXIT             (0x00000002)
#endif /* SCB_SCR_SLEEPONEXIT */


/*
 * G L O B A L S
 */

extern char debug_print_buffer[DEBUG_PRINT_MAX_LEN + 1];
extern uint64_t ta0_sw_ext;


/*
 * F U N C T I O N   P R O T O T Y P E S
 */

void spi_b0_init(uint32_t bitclk_speed);

#endif /* __MSP432DRIVER_H__ */

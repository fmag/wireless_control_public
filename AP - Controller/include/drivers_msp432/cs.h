/*
 * cs.h
 *
 *  Created on: Jun 17, 2016
 *      Author: singhh23
 */

#ifndef INCLUDE_DRIVERS_MSP432_CS_H_
#define INCLUDE_DRIVERS_MSP432_CS_H_

#include "drivers_msp432/board_config.h"

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @details This function sets up the clock system.
 */
drv_status_t DRV_CS_Init(void);

#ifdef __cplusplus
}
#endif

#endif /* INCLUDE_DRIVERS_MSP432_CS_H_ */

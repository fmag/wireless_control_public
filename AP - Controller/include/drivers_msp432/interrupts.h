/*
 * interrupts.h
 *
 *  Created on: May 11, 2016
 *      Author: singhh23
 */

#ifndef INCLUDE_DRIVERS_MSP432_INTERRUPTS_H_
#define INCLUDE_DRIVERS_MSP432_INTERRUPTS_H_

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @details This function sets up the interrupt priorities and enables interrupts.
 */
void InterruptsInit(void);

/**
 * @details This function is the interrupt request handler for the
 * sync line.
 */
void PORT4_IRQHandler(void);

/**
 * @details This function is the interrupt request handler for the
 * enhanced universal serial communication interface (EUSCI) A0 module.
 * This interrupt routine is entered upon a universal asynchronous reciever
 * transmitter (UART) transmit OR receive operation (see user guide for more details).
 */
void EUSCIA0_IRQHandler(void);

/**
 * @details This function is the interrupt request handler for the
 * enhanced universal serial communication interface (EUSCI) A2 module.
 * This interrupt routine is entered upon a universal asynchronous reciever
 * transmitter (UART) transmit OR receive operation (see user guide for more details).
 */
void EUSCIA2_IRQHandler(void);

/**
 * @details This function is the interrupt request handler for the
 * enhanced universal serial communication interface (EUSCI) A3 module.
 * This interrupt routine is entered upon a serial pheripheral interface (SPI)
 * transmit OR receive operation (see user guide for more details).
 */
void EUSCIA3_IRQHandler(void);

/**
 * @details This function is the interrupt request handler for the
 * enhanced universal serial communication interface (EUSCI) B2 module.
 * This interrupt routine is entered upon a serial pheripheral interface (SPI)
 * transmit OR receive operation (see user guide for more details).
 */
void EUSCIB2_IRQHandler(void);

/**
 * @details This function is the interrupt request handler for one
 * of the two available 32-bit timers, it is given a higher priority than INT2.
 */
void T32_INT1_IRQHandler(void);

/**
 * @details This function is the interrupt request handler for one
 * of the two available 32-bit timers, it is given a lower priority than INT1.
 */
void T32_INT2_IRQHandler(void);

/**
 * @details This function is the interrupt for all push buttons (GPIO interrupts).
 */
void PB_IRQHandler(void);

#ifdef __cplusplus
}
#endif

#endif /* INCLUDE_DRIVERS_MSP432_INTERRUPTS_H_ */

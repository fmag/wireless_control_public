/*
 * gpio.h
 *
 *  Created on: May 17, 2016
 *      Author: singhh23
 */

#ifndef INCLUDE_DRIVERS_MSP432_GPIO_H_
#define INCLUDE_DRIVERS_MSP432_GPIO_H_

#include "drivers_msp432/board_config.h"

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @description Initialize the gpio module.
 * @return GPIO_OK
 */
drv_status_t DRV_GPIO_Init();

/**
 * @description Sets the given gpio as an input.
 * @param[in] gpio The gpio to be set as input - port/pin should be correctly
 * set prior to calling this function, mode will be overwritten.
 * @return GPIO_OK
 */
drv_status_t DRV_GPIO_SetAsInput(gpio_handle_t* gpio);

/**
 * @description Sets the given gpio as an an output.
 * @param[in] gpio The gpio to be set as input - port/pin should be correctly
 * set prior to calling this function, mode will be overwritten.
 * @return GPIO_OK
 */
drv_status_t DRV_GPIO_SetAsOutput(gpio_handle_t* gpio);

/**
 * @description Sets the given output pin to high.
 * @param[in] gpio The output pin to be set to high.
 * @return GPIO_OK if successful, GPIO_ERROR if SetOutput function was not
 * called prior to this function.
 */
drv_status_t DRV_GPIO_SetHigh(gpio_handle_t* gpio);

/**
 * @description Sets the given output pin to low.
 * @param[in] gpio The output pin to be set to low.
 * @return GPIO_OK if successful, GPIO_ERROR if SetOutput function was not
 * called prior to this function.
 */
drv_status_t DRV_GPIO_SetLow(gpio_handle_t* gpio);

/**
 * @description Gets the value of an input pin.
 * @param[in] gpio The input pin to be queried.
 * @param[out] val The location to place the input pin value.
 * @return GPIO_OK if successful, GPIO_ERROR if SetAsInput function was not
 * called prior to this function.
 */
drv_status_t DRV_GPIO_GetInputPinValue(gpio_handle_t* gpio, uint8_t* val);

#ifdef __cplusplus
}
#endif

#endif /* INCLUDE_DRIVERS_MSP432_GPIO_H_ */

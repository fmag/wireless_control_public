/*
 * led.h
 *
 *  Created on: Aug 11, 2016
 *      Author: singhh23
 */

#ifndef INCLUDE_DRIVERS_MSP432_LED_H_
#define INCLUDE_DRIVERS_MSP432_LED_H_


#include "drivers_msp432/board_config.h"

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @description Initialize the led module.
 * @return LED_OK
 */
drv_status_t DRV_LED_Init(drv_led_device_t devNum);

/**
 * @description Turns the given LED on.
 * @return GPIO_OK
 */
drv_status_t DRV_LED_TurnOn(drv_led_device_t devNum);

/**
 * @description Turns the given LED off.
 * @return GPIO_OK
 */
drv_status_t DRV_LED_TurnOff(drv_led_device_t devNum);

#ifdef __cplusplus
}
#endif


#endif /* INCLUDE_DRIVERS_MSP432_LED_H_ */

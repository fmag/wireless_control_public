/*
 * Copyright (c) 2015, Swiss Federal Institute of Technology (ETH Zurich).
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Author:  Reto Da Forno
 */

#ifndef __BOLT_H__
#define __BOLT_H__


/*
 * I N C L U D E S
 */

#include "drivers_msp432/msp432driver.h"

#ifdef __cplusplus
extern "C" {
#endif

/*
 * D E F I N I T I O N S
 */


#define BOLT_SPI				SPI_BUS_0
#define BOLT_UART				UART_BUS_0
#define BOLT_BAUDRATE			br115200

// bolt
#define BOLT_USE_DMA        0
#define BOLT_TIMEREQ_PIN    PORT4, PIN2
#define BOLT_SCLK_SPEED     (SMCLK_SPEED/2)
#define BOLT_MAX_MSG_LEN    64                      // bytes
// pin mapping for the DPP
#define BOLT_ACK_PIN        PORT4, PIN7
#define BOLT_IND_PIN        PORT4, PIN4
#define BOLT_FUTURE_USE_PIN PORT10, PIN0
#define BOLT_IND_OUT_PIN    PORT4, PIN3
#define BOLT_MODE_PIN       PORT4, PIN5
#define BOLT_REQ_PIN        PORT4, PIN6

/*
 * M A C R O S
 */

#define BOLT_DATA_AVAILABLE        PIN_GET(BOLT_IND_PIN)

#define BOLT_WRITE(data, num_bytes) \
{ \
    if(bolt_acquire(BOLT_OP_WRITE)) { \
        bolt_start(data, num_bytes); \
        bolt_release(); \
    } \
}

#define BOLT_READ(out_data, num_rcvd_bytes) \
{ \
    num_rcvd_bytes = 0; \
    if(bolt_acquire(BOLT_OP_READ)) { \
        num_rcvd_bytes = bolt_start(out_data, 0); \
        bolt_release(); \
    } \
}

/*
 * T Y P E D E F S
 */

typedef enum
{
    BOLT_OP_READ = 0,
    BOLT_OP_WRITE,
    NUM_OF_OPS
} bolt_op_mode_t;


/*
 * P R O T O T Y P E S
 */

uint8_t bolt_init(void);
uint8_t bolt_status(void);
void bolt_flush(void);
uint8_t bolt_acquire(bolt_op_mode_t mode);
uint8_t bolt_start(uint8_t *data, uint16_t num_bytes);
void bolt_release(void);

#ifdef __cplusplus
}
#endif

#endif /* __BOLT_H__ */

/*
 * pcm.h
 *
 *  Created on: Sep 22, 2016
 *      Author: singhh23
 */

#ifndef INCLUDE_DRIVERS_MSP432_PCM_H_
#define INCLUDE_DRIVERS_MSP432_PCM_H_

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @details This function sets up the power control manager.
 */
drv_status_t DRV_PCM_Init(void);

#ifdef __cplusplus
}
#endif


#endif /* INCLUDE_DRIVERS_MSP432_PCM_H_ */

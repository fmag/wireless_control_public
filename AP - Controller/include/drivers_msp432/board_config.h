/*
 * board_config.h
 *
 *  Created on: May 17, 2016
 *      Author: singhh23
 */

#ifndef INCLUDE_DRIVERS_MSP432_BOARD_CONFIG_H_
#define INCLUDE_DRIVERS_MSP432_BOARD_CONFIG_H_

#include "driverlib/MSP432P4xx/spi.h"
#include "driverlib/MSP432P4xx/timer32.h"
#include "driverlib/MSP432P4xx/uart.h"
#include "stdint.h"
#include "stdlib.h"

#ifdef __cplusplus
extern "C" {
#endif

/**************** GENERAL *****************/

// An enumeration of the possible results of driver operations.
typedef enum { DRV_OK = 0, DRV_ERROR, DRV_BUSY } drv_status_t;

/******************** GPIO *********************/
// An enumeration of the potential operating modes for GPIO.
typedef enum DRV_GPIO_Mode { GPIO_OUTPUT = 0, GPIO_INPUT, GPIO_TRISTATE } gpio_mode_t;

/**
 * @description The components required to properly represent a gpio.
 */
typedef struct {

	/** The port number for this gpio. */
	const uint_fast8_t port;

	/** The pin number for this gpio. */
	const uint_fast16_t pin;

	/** The operating mode for this gpio (in/out). */
	gpio_mode_t mode;

} gpio_handle_t;

/******************** SPI BUSES *********************/
// An enumeration of all the buses which are used.
typedef enum { SPI_BUS_0 = 0, SPI_BUS_1, SPI_NUM_BUSES } spi_bus_t;

/**
 * @details The components required to properly represent a single bus.
 */
typedef struct {

	/** The port number that is used by this particular bus (eg. port 1) */
	uint_fast8_t port;

	/** The combination of pins that are required for the 3-pin SPI interface
	 * (eg. port 1, pin 5 (CLK), pin 6 (MOSI), pin 7 (MISO)) */
	uint_fast16_t pinCombination;

	/** The mode of operation for these pins (see datasheet table 6-32) */
	uint_fast8_t functionMode;

	/** See the API for eUSCI_SPI_MasterConfig in the driverlib. */
	const eUSCI_SPI_MasterConfig *busConfiguration;

	/** The location for the tx interrupt status register. */
	uint16_t txIntAddr;

	/** The location for the rx interrupt status register. */
	uint16_t rxIntAddr;

	/** The base address for this SPI bus. */
	uint32_t baseAddr;

} spi_bus_handle_t;

// A container for the bus configurations
extern const spi_bus_handle_t spiBus[SPI_NUM_BUSES];

/******************** UART BUSES *********************/
// An enumeration of all the buses which are used.
typedef enum { UART_BUS_0 = 0, UART_BUS_1, UART_NUM_BUSES } uart_bus_t;

// An enumeration of the possible baudrates for uart operation.
typedef enum { br9600 = 0, br115200, br230400, br460800, NUM_BAUDRATES } drv_baud_rate_t;

/**
 * @details The components required to properly represent a single bus.
 */
typedef struct {

	/** The port number that is used by this particular bus */
	uint_fast8_t port;

	/** The combination of pins that are required for the rx/tx UART interface */
	uint_fast16_t pinCombination;

	/** The mode of operation for these pins (see datasheet table 6-32) */
	uint_fast8_t functionMode;

	/** The location for the tx interrupt status register. */
	uint16_t txIntAddr;

	/** The location for the rx interrupt status register. */
	uint16_t rxIntAddr;

	/** The base address for this UART bus. */
	uint32_t baseAddr;

} uart_bus_handle_t;

// A container for the bus configurations
extern const uart_bus_handle_t uartBus[UART_NUM_BUSES];

/******************** CS CONFIG **********************/
typedef struct {
	/** The port number that is used by the external xtal */
	uint_fast8_t port;

	/** The combination of pins that are required for the external xtal */
	uint_fast16_t pinCombination;

	/** The mode of operation for these pins (see datasheet table 6-32) */
	uint_fast8_t functionMode;
} cs_handle_t;

extern const cs_handle_t csConfig;

#define MCLK_SRC CS_HFXTCLK_SELECT
#define MCLK_DIV CS_CLOCK_DIVIDER_1
#define MCLK_SPEED HFXTCLK_SPEED    // main clock speed [Hz], max. 48 MHz
#define SMCLK_SRC CS_HFXTCLK_SELECT // sub-system master clock
#define SMCLK_DIV CS_CLOCK_DIVIDER_1
#define SMCLK_SPEED HFXTCLK_SPEED / 8
#define HSMCLK_SRC CS_HFXTCLK_SELECT // high-speed sub-system master clock
#define HSMCLK_DIV CS_CLOCK_DIVIDER_4
#define HSMCLK_SPEED (HFXTCLK_SPEED / 4) // 12 MHz
#define ACLK_SRC CS_LFXTCLK_SELECT       // auxillary clock
#define ACLK_DIV CS_CLOCK_DIVIDER_1
#define ACLK_SPEED LFXTCLK_SPEED   // max. 128 kHz
#define BCLK_SRC CS_LFXTCLK_SELECT // low-speed backup domain clock, LFXT or REFO
#define BCLK_DIV CS_CLOCK_DIVIDER_1
#define BCLK_SPEED LFXTCLK_SPEED // max. 32 kHz

#define HFXTCLK_SPEED 48000000
#define LFXTCLK_SPEED 32768
#define DCOCLK_SPEED 4000000
#define REFO_SPEED 32768

/******************** T32 TIMERS *********************/
// Conversion from ms to cycles
#define MS_TO_CYCLES(ms) ((ms) * (MCLK_SPEED / 1000.0))
#define SAMPLING_RATE_TO_CYCLES(f) (MCLK_SPEED / (f))

// A function pointer typedef
typedef void (*timer_callback_t)(void *ptr);

// An enumeration of all the timers that are used.
typedef enum { T32_0 = 0, T32_1, T32_NUM_TIMERS } t32_timer_t;

/**
 * @details The components required to properly represent a single timer object.
 */
typedef struct {

	/** The base address for this timer. */
	Timer32_Type *baseAddr;

	/** The scaling for the timer clock. */
	uint32_t prescaler;

	/** The number of bits used for the counter. */
	uint32_t bitMode;

	/** The operation mode (periodic vs. free-running vs. one-shot) */
	uint32_t operMode;

	/** The callback function to be used when timer has elapsed. */
	timer_callback_t timerCallback;

	/** The timer object that this handle represents (required for resolving listener collisions).
	 */
	void *usrObj;

} t32_timer_handle_t;

// A container for the t32 configurations
extern t32_timer_handle_t t32Timer[T32_NUM_TIMERS];

/******************* LS7366 DEVICE ******************/
// An enumeration of all the quadrature decoders that are used.
typedef enum { LS7366_DEV_0 = 0, LS7366_DEV_1, LS7366_NUM_DEV } drv_ls7366_device_t;

// Mapping from device number to spi bus (multiple devices on the same bus are allowed).
extern const spi_bus_t LS7366_SPI_BUS[LS7366_NUM_DEV];

// Mapping from device number to physical location.
extern gpio_handle_t LS7366_GPIO[LS7366_NUM_DEV];

/******************* DAC8831 DEVICE ******************/
// An enumeration of all the quadrature decoders that are used.
typedef enum { DAC8831_DEV_0 = 0, DAC8831_NUM_DEV } drv_dac8831_device_t;

// Mapping from device number to spi bus (multiple devices on the same bus are allowed).
extern const spi_bus_t DAC8831_SPI_BUS[DAC8831_NUM_DEV];

// Mapping from device number to physical location.
extern gpio_handle_t DAC8831_GPIO[DAC8831_NUM_DEV];
extern gpio_handle_t DAC8831_EN[DAC8831_NUM_DEV];

/******************* LED DEVICE ******************/
// An enumeration of all the led devices that are used.
typedef enum { LED_DEV_0 = 0, LED_NUM_DEV } drv_led_device_t;

// Mapping from device number to physical location.
extern gpio_handle_t LED_GPIO[LED_NUM_DEV];

/******************* PUSHBUTTON DEVICE ******************/
// An enumeration of all the push buttons that are used.
typedef enum { PB_0 = 0, PB_1, PB_NUM_DEV } drv_pb_device_t;

// Mapping from device number to physical location.
extern gpio_handle_t PB_GPIO[PB_NUM_DEV];

/*****************************************************/

#ifdef __cplusplus
}
#endif

#endif /* INCLUDE_DRIVERS_MSP432_BOARD_CONFIG_H_ */

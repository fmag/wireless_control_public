/*
 * Copyright (c) 2016, Swiss Federal Institute of Technology (ETH Zurich).
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Author:  Reto Da Forno
 */

#ifndef __CONFIG_H__
#define __CONFIG_H__

//#define FLOCKLAB /* uncomment to run on FlockLAB */
#ifndef FLOCKLAB

/******************************************************************************
 * Experiment configuration
 *****************************************************************************/
#define HOST_ID 10
#define NODE_ID 10

#if NODE_ID == 0
#error "NODE_ID must be greater than 0!"
#elif NODE_ID >= 0xfffd
#error "NODE_ID must be smaller than 65.533"
#endif

#define DEFAULT_MODE_ID 0
#define LWB_CONF_MAX_HOPS 3
#define MAX_NUM_NODES 25
#define ENABLE_GUI_MODE_CHANGES 1
#define RNDS_UNTIL_SCHED_SWITCH 5

// specific overall fault probabilities:
// 75%: FAULTS1_50     , FAULTS2_50
// 60%: FAULTS1_36_7544, FAULTS2_36_7544
// 45%: FAULTS1_25_8380, FAULTS2_25_8380
// 30%: FAULTS1_16_3340, FAULTS2_16_3340
// 15%: FAULTS1_7_8046 , FAULTS2_7_8046
// 0% : FAULTS_0
// burst faults:
// FAULTS_BURST_10, FAULTS_BURST_20, FAULTS_BURST_30, FAULTS_BURST_40, FAULTS_BURST_50
// TODO: Check if burst experiment code still works.
#define BURST_EXPERIMENT 0 // enable bursts across nodes, i.e., consecutive slots in a LWB round
#if NODE_ID == 1
#define NODE_FAULTS FAULTS_0 // faults on channel from node 2 to node 1
#else
#define NODE_FAULTS FAULTS_0 // faults on channel from node 1 to node 2
#endif

/******************************************************************************
 * Non-FlockLab general configuration
 *****************************************************************************/
#define RF_CONF_TX_CH 5
#define RF_CONF_TX_POWER RF1A_TX_POWER_0_dBm
// RF1A_TX_POWER_MINUS_6_dBm
// RF1A_TX_POWER_MAX
#define LWB_CONF_OUT_BUFFER_SIZE 5
#define LWB_CONF_IN_BUFFER_SIZE 10
#define LWB_CONF_USE_LF_FOR_WAKEUP 0
#define LWB_CONF_MAX_PKT_LEN 19//11 // TODO: problem that this is equal to LWB_CONF_MAX_DATA_PKT_LEN?
// max. data payload length is LWB_CONF_MAX_DATA_PKT_LEN - 3, must be <= (LWB_CONF_MAX_PKT_LEN - 5)
#define LWB_CONF_MAX_DATA_PKT_LEN 19//11 // 16 byte payload and 3 bytes lwb header (lwb_data_pkt_t)
#define LWB_CONF_T_SCHED LWB_T_SLOT_MIN(12) // 2B control_pkt_t, 3B lwb hdr, 7B glossy hdr
#define LWB_CONF_SCHED_STREAM_REMOVAL_THRES 256
#define FAULT_BURST_LEN                                                                            \
	1 // number of consecutive failed slots of the same node (1 means no burst failures)
#define LWB_SCHED_STATIC_ECOCPS
#define LWB_CONF_SCHED_PERIOD_IDLE 1
#define LWB_CONF_SCHED_COMPRESS 0
#define LWB_CONF_MAX_DATA_SLOTS 5 // does not matter
#define LWB_CONF_TX_CNT_SCHED 4
#define LWB_CONF_TX_CNT_DATA 2
#define LWB_CONF_T_PREPROCESS 1                 // 1ms
#define LWB_CONF_T_GAP (RTIMER_SECOND_HF / 500) // 2ms
#define SYNC_LINE_GAP (RTIMER_SECOND_HF / 2000) // 0.5ms
#define ENERGEST_CONF_ON 1
#define DEBUG_PRINT_CONF_LEVEL DEBUG_PRINT_LVL_INFO

/******************************************************************************
 * Non-FlockLab pin macros
 *****************************************************************************/
#define LWB_RND_PIN COM_GPIO1 // visualizes LWB round and print time
#ifdef LWB_RND_PIN
#define LWB_RND_ACTIVE PIN_SET(LWB_RND_PIN)
#define LWB_RND_SUSPENDED PIN_CLR(LWB_RND_PIN)
#else
#define LWB_RND_ACTIVE
#define LWB_RND_SUSPENDED
#endif

#define GLOSSY_RND_PIN COM_GPIO2
#ifdef GLOSSY_RND_PIN
#define GLOSSY_RND_ACTIVE PIN_SET(GLOSSY_RND_PIN)
#define GLOSSY_RND_SUSPENDED PIN_CLR(GLOSSY_RND_PIN)
#else
#define GLOSSY_RND_ACTIVE
#define GLOSSY_RND_SUSPENDED
#endif

//#define TA_OVERFLOW_INT_PIN COM_PROG2
#ifdef TA_OVERFLOW_INT_PIN
#define TA_OVERFLOW_INT_ACTIVE PIN_SET(TA_OVERFLOW_INT_PIN)
#define TA_OVERFLOW_INT_SUSPENDED PIN_CLR(TA_OVERFLOW_INT_PIN)
#else
#define TA_OVERFLOW_INT_ACTIVE
#define TA_OVERFLOW_INT_SUSPENDED
#endif

//#define RX_SUCCESS_PIN COM_PROG2
#ifdef RX_SUCCESS_PIN
#define RX_SUCCESS_ACTIVE PIN_SET(RX_SUCCESS_PIN)
#define RX_SUCCESS_SUSPENDED PIN_CLR(RX_SUCCESS_PIN)
#else
#define RX_SUCCESS_ACTIVE
#define RX_SUCCESS_SUSPENDED
#endif

// #define FAULT_PIN COM_PROG2
#ifdef FAULT_PIN
#define FAULT_ACTIVE PIN_SET(FAULT_PIN)
#define FAULT_SUSPENDED PIN_CLR(FAULT_PIN)
#else
#define FAULT_ACTIVE
#define FAULT_SUSPENDED
#endif

// #define SYNCLINE_PIN COM_GPIO1
#ifdef SYNCLINE_PIN
#define SYNCLINE_ACTIVE PIN_SET(SYNCLINE_PIN)
#define SYNCLINE_SUSPENDED PIN_CLR(SYNCLINE_PIN)
#else
#define SYNCLINE_ACTIVE
#define SYNCLINE_SUSPENDED
#endif

// #define TEST_PIN COM_GPIO1
#ifdef TEST_PIN
#define TEST_ACTIVE PIN_SET(TEST_PIN)
#define TEST_SUSPENDED PIN_CLR(TEST_PIN)
#else
#define TEST_ACTIVE
#define TEST_SUSPENDED
#endif

// #define TEST_PIN2 COM_GPIO2
#ifdef TEST_PIN2
#define TEST2_ACTIVE PIN_SET(TEST_PIN2)
#define TEST2_SUSPENDED PIN_CLR(TEST_PIN2)
#else
#define TEST2_ACTIVE
#define TEST2_SUSPENDED
#endif

#define DEBUG_PRINT_PIN COM_GPIO1
#ifdef DEBUG_PRINT_PIN
#define DEBUG_PRINT_ACTIVE PIN_SET(DEBUG_PRINT_PIN)
#define DEBUG_PRINT_SUSPENDED PIN_CLR(DEBUG_PRINT_PIN)
#else
#define DEBUG_PRINT_ACTIVE
#define DEBUG_PRINT_SUSPENDED
#endif

/******************************************************************************
 * FlockLab config
 *****************************************************************************/
 #else
/* set the highest antenna gain if the program runs on FlockLAB */
#define HOST_ID 1
#define RF_CONF_TX_POWER RF1A_TX_POWER_MINUS_6_dBm // 0_dBm
#define RF_CONF_TX_CH 5                            /* approx. 869 MHz */
#define GLOSSY_START_PIN FLOCKLAB_LED1
#define RF_GDO2_PIN FLOCKLAB_INT1
// #define APP_TASK_ACT_PIN FLOCKLAB_INT2
/* note: FLOCKLAB_LED2 should not be used */
#endif /* FLOCKLAB */

#endif /* __CONFIG_H__ */

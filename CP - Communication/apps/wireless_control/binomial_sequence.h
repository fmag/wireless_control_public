#ifndef APPS_LWB_BINOMIAL_SEQUENCES_H_
#define APPS_LWB_BINOMIAL_SEQUENCES_H_

#include "stdint.h"

uint8_t binomial_get_next();

#endif // APPS_LWB_BINOMIAL_SEQUENCES_H_

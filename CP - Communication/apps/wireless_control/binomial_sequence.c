#include "binomial_sequence.h"
#include "sequences.h"
#include "config.h"

// FAULTS_0, FAULTS1_2_5, FAULTS2_2_5, FAULTS1_5, FAULTS2_5, FAULTS1_7_5, FAULTS2_7_5, FAULTS1_10, FAULTS2_10,
// FAULTS1_12_5, FAULTS2_12_5, FAULTS1_15, FAULTS2_15, FAULTS1_17_5, FAULTS2_17_5, FAULTS1_20, FAULTS2_20,
// FAULTS1_22_5, FAULTS2_22_5, FAULTS1_25, FAULTS2_25, FAULTS1_27_5, FAULTS2_27_5, FAULTS1_30, FAULTS2_30,
// FAULTS_35, FAULTS_40, FAULTS_45, FAULTS_50, FAULTS_55, FAULTS_60, FAULTS_BURST_2, FAULTS_BURST_3,
// FAULTS_BURST_4, FAULTS_BURST_5, FAULTS_BURST_6, FAULTS_BURST_7, FAULTS_BURST_8, FAULTS_BURST_9, FAULTS_BURST_10,
// FAULTS_BURST_20, FAULTS_BURST_25, FAULTS_BURST_30, FAULTS_BURST_40, FAULTS_BURST_50

// specific overall fault probabilities:
// 95%: FAULTS1_77_6393, FAULTS2_77_6393
// 90%: FAULTS1_68_3772, FAULTS2_68_3772
// 85%: FAULTS1_61_2702, FAULTS2_61_2702
// 80%: FAULTS1_55_2786, FAULTS2_55_2786
// 75%: FAULTS1_50     , FAULTS2_50
// 70%: FAULTS1_45_2277, FAULTS2_45_2277
// 65%: FAULTS1_40_8392, FAULTS2_40_8392
// 60%: FAULTS1_36_7544, FAULTS2_36_7544
// 55%: FAULTS1_32_9179, FAULTS2_32_9179
// 50%: FAULTS1_29_2893, FAULTS2_29_2893
// 45%: FAULTS1_25_8380, FAULTS2_25_8380
// 40%: FAULTS1_22_5403, FAULTS2_22_5403
// 35%: FAULTS1_19_3774, FAULTS2_19_3774
// 30%: FAULTS1_16_3340, FAULTS2_16_3340
// 25%: FAULTS1_13_3974, FAULTS2_13_3974
// 20%: FAULTS1_10_5572, FAULTS2_10_5572
// 15%: FAULTS1_7_8046 , FAULTS2_7_8046
// 10%: FAULTS1_5_1317 , FAULTS2_5_1317
// 5%:  FAULTS1_2_5321 , FAULTS2_2_5321

static const uint8_t sequence[4096] = NODE_FAULTS;

//#if NODE_ID == 1
//    #if (FAULTY_CHANNEL == 0) || (FAULTY_CHANNEL == 1)
//    static const uint8_t sequence[4096] = FAULTS_0;
//    #else
//    static const uint8_t sequence[4096] = FAULTS_BURST_25;
//    #endif
//#else // NODE_ID 2, 3, ...
//    #if (FAULTY_CHANNEL == 0) || (FAULTY_CHANNEL == 2)
//    static const uint8_t sequence[4096] = FAULTS_0;
//    #else
//    static const uint8_t sequence[4096] = FAULTS_BURST_25;
//    #endif
//#endif


static uint16_t element_idx = 0;
static uint8_t mask = 0x80;
static uint8_t burst_cnt = 0;


// Returns 0 if next bit in the sequence is 0, otherwise a value greater 0 is returned.
uint8_t binomial_get_next() {
    if (burst_cnt) {
        --burst_cnt;
        return 1;
    }

	uint8_t prob = sequence[element_idx & 0x0FFF] & mask; // element_idx should only count to 4095
	mask >>= 1;
	if (mask == 0) {
		element_idx++;
		mask = 0x80;
	}

	if (prob) {
	    burst_cnt = FAULT_BURST_LEN - 1;
	}

	return prob;
}

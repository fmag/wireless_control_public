/*
 * Copyright (c) 2016, Swiss Federal Institute of Technology (ETH Zurich).
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Author:  Reto Da Forno
 *          Federico Ferrari
 *          Marco Zimmerling
 */

/**
 * @file
 *
 * an implementation of the Low-Power Wireless Bus
 */

#include "binomial_sequence.h"
#include "contiki.h"

#if LWB_VERSION == 1
/*---------------------------------------------------------------------------*/
#if LWB_CONF_HEADER_LEN != 3
#error "LWB_CONF_HEADER_LEN must be 3!"
#endif
#define LWB_DATA_PKT_PAYLOAD_LEN (LWB_CONF_MAX_DATA_PKT_LEN - LWB_CONF_HEADER_LEN)

/*---------------------------------------------------------------------------*/
/* internal sync state of the LWB on the source node */
typedef enum {
	BOOTSTRAP = 0,
	QUASI_SYNCED,
	SYNCED,
	SYNCED_2,
	MISSED,
	UNSYNCED,
	UNSYNCED2,
	NUM_OF_SYNC_STATES
} lwb_sync_state_t;
/*---------------------------------------------------------------------------*/
typedef enum {
	EVT_1ST_SCHED_RCVD = 0,
	EVT_2ND_SCHED_RCVD,
	EVT_SCHED_MISSED,
	NUM_OF_SYNC_EVENTS
} sync_event_t;
/*---------------------------------------------------------------------------*/
typedef struct {
	// TODO: remove stream_id
	uint16_t recipient; /* target node ID */
	uint8_t stream_id;  /* message type and connection ID (used as stream ID
	                       in LWB); first bit is msg type */
	uint8_t payload[LWB_DATA_PKT_PAYLOAD_LEN];
} lwb_data_pkt_t;

/*---------------------------------------------------------------------------*/

// This structure contains information about the current schedule ID (in case a node joins the
// network) and when (# of rounds) a new schedule should be applied.
typedef struct __attribute__((packed)) control_pkt_t_tag {
	uint8_t curSchedID : 4;
	uint8_t newSchedID : 4;
	uint8_t newSchedRnds;
} control_pkt_t;

/*---------------------------------------------------------------------------*/

typedef struct bolt_data_pkt_t_tag {
	uint16_t senderID;
	uint8_t data[LWB_DATA_PKT_PAYLOAD_LEN];
	// TODO: LWB_CONF_MAX_DATA_PKT_LEN is currently 3 bytes bigger than necessary due to (recipient)
	// and stream information.
} bolt_data_pkt_t;

/*---------------------------------------------------------------------------*/

typedef struct {
	/* be aware of structure alignment (8-bit are aligned to 8-bit, 16 to 16 etc)
	 * the first 3 bytes of a glossy packet are always node_id and stream_id */
	union {
		struct {
			lwb_data_pkt_t data_pkt;
			uint8_t reserved1[LWB_CONF_MAX_PKT_LEN - LWB_CONF_MAX_DATA_PKT_LEN];
		};
		uint8_t raw_data[LWB_CONF_MAX_PKT_LEN];
	};
} glossy_payload_t;
/*---------------------------------------------------------------------------*/
/**
 * @brief the finite state machine for the time synchronization on a source
 * node the next state can be retrieved from the current state (column) and
 * the latest event (row)
 * @note  undefined transitions force the SM to go back into bootstrap
 */
#if LWB_CONF_SKIP_QUASI_SYNCED
#define AFTER_BOOT SYNCED
#else
#define AFTER_BOOT QUASI_SYNCED
#endif
static const lwb_sync_state_t next_state[NUM_OF_SYNC_EVENTS][NUM_OF_SYNC_STATES] = {
    /* STATES: EVENTS:           */
    /* BOOTSTRAP,  QUASISYNCED,  SYNCED,    SYNCED2,   MISSED,    UNSYNCED,  UNSYNCED2 */
    {AFTER_BOOT, SYNCED, BOOTSTRAP, SYNCED, SYNCED, BOOTSTRAP, SYNCED}, /* 1st schedule rcvd */
    {BOOTSTRAP, QUASI_SYNCED, SYNCED_2, BOOTSTRAP, BOOTSTRAP, SYNCED_2,
     BOOTSTRAP},                                                             /* 2nd schedule rcvd */
    {BOOTSTRAP, BOOTSTRAP, MISSED, UNSYNCED, UNSYNCED, UNSYNCED2, BOOTSTRAP} /* schedule missed   */
};
static const lwb_sync_state_t next_state_mod[NUM_OF_SYNC_EVENTS][NUM_OF_SYNC_STATES] = {
    /* STATES: EVENTS:           */
    /* BOOTSTRAP,  QUASISYNCED,  SYNCED,    SYNCED2,   MISSED,    UNSYNCED,  UNSYNCED2 */
    {AFTER_BOOT, SYNCED, BOOTSTRAP, SYNCED, SYNCED, BOOTSTRAP, SYNCED}, /* 1st schedule rcvd */
    {BOOTSTRAP, QUASI_SYNCED, SYNCED_2, BOOTSTRAP, BOOTSTRAP, SYNCED_2,
     BOOTSTRAP},                                                            /* 2nd schedule rcvd */
    {BOOTSTRAP, BOOTSTRAP, MISSED, UNSYNCED, UNSYNCED, UNSYNCED2, UNSYNCED} /* schedule missed   */
};
/* note: syn2 = already synced */
// static const char* lwb_sync_state_to_string[NUM_OF_SYNC_STATES] =
//{ "BOOTSTRAP", "QSYN", "SYN", "SYN2", "MISS", "USYN", "USYN2" };
static const uint32_t guard_time[NUM_OF_SYNC_STATES] = {
    /* STATE:      BOOTSTRAP,        QUASI_SYNCED,      SYNCED,           SYNCED_2,
       MISSED,             UNSYNCED,           UNSYNCED2 */
    /* T_GUARD: */ LWB_CONF_T_GUARD,
    LWB_CONF_T_GUARD,
    LWB_CONF_T_GUARD,
    LWB_CONF_T_GUARD,
    LWB_CONF_T_GUARD_1,
    LWB_CONF_T_GUARD_2,
    LWB_CONF_T_GUARD_3};

/*---------------------------------------------------------------------------*/

typedef struct schedule_t_tag {
	uint16_t n_slots;                         // Number of slots in this schedule.
	uint16_t slot[LWB_CONF_MAX_DATA_SLOTS];   // Assign node ids to slots.
	uint16_t target[LWB_CONF_MAX_DATA_SLOTS]; // Assign targets to slots.
	// E.g., node id at slot[0] sends to all nodes of type target[0].
	uint16_t nodeIdentity[MAX_NUM_NODES]; // defines the type of ALL nodes including those
	                                      // that don't have a slot (index is node_id - 1)
	unsigned int timescale;               // update interval = 1 / timescale * 1000
} schedule_t;

/*---------------------------------------------------------------------------*/

// Example schedule.
// .n_slots = 3,
// .slot = {2, 3, 4},
// .target = {PENDULUM_NODES, CONTROLLER_NODES, CONTROLLER_NODES},
// nodeIdentity: nodeID 1 (index 0) is a relay node and not listed
// .nodeIdentity = {[1] = CONTROLLER_NODES, [2] = PENDULUM_NODES, [3] = PENDULUM_NODES},
// .timescale = 33

static const schedule_t schedule_list[16] = {
    // 0: Local stabilization
    {.n_slots = 0,
     .slot = {},
     .target = {},
     .nodeIdentity = {[1] = PENDULUM_NODES,
                      [2] = PENDULUM_NODES,
                      [4] = PENDULUM_NODES,
                      [5] = PENDULUM_NODES,
                      [6] = PENDULUM_NODES},
     .timescale = 40}, // 25ms

    // 1: Remote stabilization (1 pendulum - 1 loop)
    {.n_slots = 2,
     .slot = {1, 2},
     .target = {PENDULUM_NODES, CONTROLLER_NODES},
     .nodeIdentity = {[0] = CONTROLLER_NODES,
                      [1] = PENDULUM_NODES,
                      [2] = PENDULUM_NODES,
                      [4] = PENDULUM_NODES,
                      [5] = PENDULUM_NODES,
                      [6] = PENDULUM_NODES},
     .timescale = 25 /*40ms*/},

    // 2: Remote stabilization (2 pendulums - 1 loop)
    {.n_slots = 3,
     .slot = {1, 2, 3},
     .target = {PENDULUM_NODES, CONTROLLER_NODES, CONTROLLER_NODES},
     .nodeIdentity = {[0] = CONTROLLER_NODES,
                      [1] = PENDULUM_NODES,
                      [2] = PENDULUM_NODES,
                      [4] = PENDULUM_NODES,
                      [5] = PENDULUM_NODES,
                      [6] = PENDULUM_NODES},
     .timescale = 25 /*40ms*/},

    // 3: Remote stabilization (2 pendulums - 2 loops)
    {.n_slots = 4,
     .slot = {1, 2, 3, 4},
     .target = {2, 1, 4, 3},
     .nodeIdentity = {[0] = CONTROLLER_NODES,
                      [1] = PENDULUM_NODES,
                      [2] = PENDULUM_NODES,
                      [3] = CONTROLLER_NODES,
                      [4] = PENDULUM_NODES,
                      [5] = PENDULUM_NODES,
                      [6] = PENDULUM_NODES},
     .timescale = 22 /*45ms*/},

    // 4: Remote stabilization (3 pendulums - 1 loop)
    {.n_slots = 4,
     .slot = {1, 2, 3, 5},
     .target = {PENDULUM_NODES, CONTROLLER_NODES, CONTROLLER_NODES, CONTROLLER_NODES},
     .nodeIdentity = {[0] = CONTROLLER_NODES,
                      [1] = PENDULUM_NODES,
                      [2] = PENDULUM_NODES,
                      [4] = PENDULUM_NODES,
                      [5] = PENDULUM_NODES,
                      [6] = PENDULUM_NODES},
     .timescale = 22 /*40ms*/},

    // 5: Local stabilization and synchronization (5 pendulums)
    {.n_slots = 5,
     .slot = {2, 3, 5, 6, 7},
     .target = {PENDULUM_NODES, PENDULUM_NODES, PENDULUM_NODES, PENDULUM_NODES, PENDULUM_NODES},
     .nodeIdentity = {[1] = PENDULUM_NODES,
                      [2] = PENDULUM_NODES,
                      [4] = PENDULUM_NODES,
                      [5] = PENDULUM_NODES,
                      [6] = PENDULUM_NODES},
     .timescale = 20 /*50ms*/},

    // // 4: Local stabilization and synchronization (2 pendulums)
    // {.n_slots = 2,
    //  .slot = {2, 3},
    //  .target = {PENDULUM_NODES, PENDULUM_NODES},
    //  .nodeIdentity = {[1] = PENDULUM_NODES,
    //                   [2] = PENDULUM_NODES,
    //                   [4] = PENDULUM_NODES,
    //                   [5] = PENDULUM_NODES,
    //                   [6] = PENDULUM_NODES},
    //  .timescale = 20 /*50ms*/},
    //
    // // 5: Local stabilization and synchronization (3 pendulums)
    // {.n_slots = 3,
    //  .slot = {2, 3, 5},
    //  .target = {PENDULUM_NODES, PENDULUM_NODES, PENDULUM_NODES},
    //  .nodeIdentity = {[1] = PENDULUM_NODES,
    //                   [2] = PENDULUM_NODES,
    //                   [4] = PENDULUM_NODES,
    //                   [5] = PENDULUM_NODES,
    //                   [6] = PENDULUM_NODES},
    //  .timescale = 20 /*50ms*/},
    //
    // // 6: Local stabilization and synchronization (4 pendulums)
    // {.n_slots = 4,
    //  .slot = {2, 3, 5, 6},
    //  .target = {PENDULUM_NODES, PENDULUM_NODES, PENDULUM_NODES, PENDULUM_NODES},
    //  .nodeIdentity = {[1] = PENDULUM_NODES,
    //                   [2] = PENDULUM_NODES,
    //                   [4] = PENDULUM_NODES,
    //                   [5] = PENDULUM_NODES,
    //                   [6] = PENDULUM_NODES},
    //  .timescale = 20 /*50ms*/},
    //
    // // 7: Local stabilization and synchronization (5 pendulums)
    // {.n_slots = 5,
    //  .slot = {2, 3, 5, 6, 7},
    //  .target = {PENDULUM_NODES, PENDULUM_NODES, PENDULUM_NODES, PENDULUM_NODES, PENDULUM_NODES},
    //  .nodeIdentity = {[1] = PENDULUM_NODES,
    //                   [2] = PENDULUM_NODES,
    //                   [4] = PENDULUM_NODES,
    //                   [5] = PENDULUM_NODES,
    //                   [6] = PENDULUM_NODES},
    //  .timescale = 20 /*50ms*/},
    //
    // // 8: Remote stabilization (1 pendulum, 30ms)
    // {.n_slots = 2,
    //  .slot = {1, 2},
    //  .target = {PENDULUM_NODES, CONTROLLER_NODES},
    //  .nodeIdentity = {[0] = CONTROLLER_NODES, [1] = PENDULUM_NODES},
    //  //     .timescale = 50 /*20ms*/},
    //  .timescale = 33 /*30ms*/},
    //
    // // 9: Remote stabilization (1 pendulum, 40ms)
    // {.n_slots = 2,
    //  .slot = {1, 2},
    //  .target = {PENDULUM_NODES, CONTROLLER_NODES},
    //  .nodeIdentity = {[0] = CONTROLLER_NODES, [1] = PENDULUM_NODES},
    //  .timescale = 25 /*40ms*/},
};

/*---------------------------------------------------------------------------*/

static uint8_t defaultSchedID = DEFAULT_MODE_ID;
static schedule_t schedule;
static control_pkt_t control_pkt;
static uint8_t slot_fail;
static long unsigned flood_ok_cnt;
//static rtimer_clock_t radio_on_prev, radio_on_now, cpu_on_prev, cpu_on_now;

/*---------------------------------------------------------------------------*/

#define LWB_T_SLOT_START(i)                                                                        \
	((LWB_CONF_T_SCHED + LWB_CONF_T_GAP) + (LWB_CONF_T_DATA + LWB_CONF_T_GAP) * i)
#define LWB_DATA_RCVD (glossy_get_n_rx() > 0)
#define RTIMER_CAPTURE (t_now = rtimer_now_hf())
#define RTIMER_ELAPSED ((rtimer_now_hf() - t_now) * 1000 / 3250)
#define GET_EVENT(firstSchedule)                                                                   \
	(glossy_is_t_ref_updated() ? (firstSchedule ? EVT_1ST_SCHED_RCVD : EVT_2ND_SCHED_RCVD)         \
	                           : EVT_SCHED_MISSED)
/*#define GET_EVENT                 (glossy_is_t_ref_updated() ? \
                                     EVT_1ST_SCHED_RCVD \
                                   : EVT_SCHED_MISSED)*/

/*---------------------------------------------------------------------------*/

#define LWB_SEND_SCHED()                                                                           \
	{                                                                                              \
		glossy_start(node_id, (uint8_t *)&control_pkt, sizeof(control_pkt_t),                      \
		             LWB_CONF_TX_CNT_SCHED, GLOSSY_WITH_SYNC, GLOSSY_WITH_RF_CAL, 0);              \
		LWB_WAIT_UNTIL(rt->time + LWB_CONF_T_SCHED);                                               \
		glossy_stop();                                                                             \
	}

#define LWB_RCV_SCHED()                                                                            \
	{                                                                                              \
		glossy_start(GLOSSY_UNKNOWN_INITIATOR, (uint8_t *)&control_pkt, sizeof(control_pkt_t),     \
		             LWB_CONF_TX_CNT_SCHED, GLOSSY_WITH_SYNC, GLOSSY_WITH_RF_CAL, 0);              \
		LWB_WAIT_UNTIL(rt->time + LWB_CONF_T_SCHED + t_guard);                                     \
		glossy_stop();                                                                             \
	}

#define LWB_SEND_PACKET()                                                                          \
	{                                                                                              \
		glossy_start(node_id, (uint8_t *)&glossy_payload, payload_len, LWB_CONF_TX_CNT_DATA,       \
		             GLOSSY_WITHOUT_SYNC, GLOSSY_WITHOUT_RF_CAL, slot_fail);                       \
		LWB_WAIT_UNTIL(rt->time + LWB_CONF_T_DATA);                                                \
		glossy_stop();                                                                             \
		flood_ok_cnt += 1;                                                                         \
	}

#define LWB_RCV_PACKET()                                                                           \
	{                                                                                              \
		glossy_start(GLOSSY_UNKNOWN_INITIATOR, (uint8_t *)&glossy_payload,                         \
		             GLOSSY_UNKNOWN_PAYLOAD_LEN, LWB_CONF_TX_CNT_DATA, GLOSSY_WITHOUT_SYNC,        \
		             GLOSSY_WITHOUT_RF_CAL, slot_fail);                                            \
		LWB_WAIT_UNTIL(rt->time + LWB_CONF_T_DATA + t_guard);                                      \
		glossy_stop();                                                                             \
		flood_ok_cnt += glossy_get_n_rx() ? 1 : 0;                                                 \
	}

/*---------------------------------------------------------------------------*/

/* suspend the LWB proto-thread until the rtimer reaches the specified time */
#define LWB_WAIT_UNTIL(time)                                                                       \
	{                                                                                              \
		rtimer_schedule(LWB_CONF_RTIMER_ID, time, 0, callback_func);                               \
		PT_YIELD(&lwb_pt);                                                                         \
	}
/* same as LWB_WAIT_UNTIL, but use the LF timer to schedule the wake-up */
#define LWB_LF_WAIT_UNTIL(time)                                                                    \
	{                                                                                              \
		rtimer_schedule(LWB_CONF_LF_RTIMER_ID, time, 0, callback_func);                            \
		PT_YIELD(&lwb_pt);                                                                         \
		LWB_AFTER_DEEPSLEEP();                                                                     \
	}
#define LWB_UPDATE_SYNC_STATE(firstSchedule)                                                       \
	{                                                                                              \
		/* get the new state based on the event */                                                 \
		static uint8_t unsynced_cnt;                                                               \
		if (sync_state == UNSYNCED2) {                                                             \
			if (unsynced_cnt < (RNDS_UNTIL_SCHED_SWITCH - 1)) { /*TODO: -1 necessary?*/            \
				sync_state = next_state_mod[GET_EVENT(firstSchedule)][sync_state];                 \
				if (sync_state == UNSYNCED) {                                                      \
					unsynced_cnt++;                                                                \
				} else {                                                                           \
					unsynced_cnt = 0;                                                              \
				}                                                                                  \
			} else {                                                                               \
				sync_state = next_state[GET_EVENT(firstSchedule)][sync_state];                     \
				unsynced_cnt = 0;                                                                  \
			}                                                                                      \
		} else {                                                                                   \
			sync_state = next_state[GET_EVENT(firstSchedule)][sync_state];                         \
		}                                                                                          \
		t_guard = guard_time[sync_state]; /* adjust the guard time */                              \
	}
#ifndef LWB_BEFORE_DEEPSLEEP
#define LWB_BEFORE_DEEPSLEEP()
#endif /* LWB_PREPARE_DEEPSLEEP */
#ifndef LWB_AFTER_DEEPSLEEP
#define LWB_AFTER_DEEPSLEEP()
#endif /* LWB_AFTER_DEEPSLEEP */
/*---------------------------------------------------------------------------*/
static struct pt lwb_pt;
static void (*pre_proc)(void);
static void (*post_proc)(void);
static lwb_sync_state_t sync_state;
static lwb_statistics_t stats = {0};
/* allocate memory in the SRAM (+1 to store the message length) */
static uint8_t in_buffer_mem[LWB_CONF_IN_BUFFER_SIZE * (LWB_CONF_MAX_DATA_PKT_LEN + 1)];
static uint8_t out_buffer_mem[LWB_CONF_OUT_BUFFER_SIZE * (LWB_CONF_MAX_DATA_PKT_LEN + 1)];
FIFO(in_buffer, LWB_CONF_MAX_DATA_PKT_LEN + 1, LWB_CONF_IN_BUFFER_SIZE);
FIFO(out_buffer, LWB_CONF_MAX_DATA_PKT_LEN + 1, LWB_CONF_OUT_BUFFER_SIZE);

/*---------------------------------------------------------------------------*/

/* store a received message in the incoming queue, returns 1 if successful,
 * 0 otherwise */
uint8_t lwb_in_buffer_put(const uint8_t *const data, uint8_t len) {
	if (!len) { return 0; }
	if (len > LWB_CONF_MAX_DATA_PKT_LEN) {
		len = LWB_CONF_MAX_DATA_PKT_LEN;
		DEBUG_PRINT_WARNING("received data packet is too big");
	}
	/* received messages will have the max. length LWB_CONF_MAX_DATA_PKT_LEN */
	uint32_t pkt_addr = fifo_put(&in_buffer);
	if (FIFO_ERROR != pkt_addr) {
		/* copy the data into the queue */
		uint8_t *next_msg = (uint8_t *)((uint16_t)pkt_addr);
		memcpy(next_msg, data, len);
		/* last byte holds the payload length */
		*(next_msg + LWB_CONF_MAX_DATA_PKT_LEN) = len;
		return 1;
	}
	stats.rxbuf_drop++;
	DEBUG_PRINT_WARNING("lwb rx queue full");
	return 0;
}

/*---------------------------------------------------------------------------*/

/* fetch the next 'ready-to-send' message from the outgoing queue
 * returns the message length in bytes */
uint8_t lwb_out_buffer_get(uint8_t *out_data) {
	/* messages have the max. length LWB_CONF_MAX_DATA_PKT_LEN and are already
	 * formatted according to glossy_payload_t */
	uint32_t pkt_addr = fifo_get(&out_buffer);
	if (FIFO_ERROR != pkt_addr) {
		/* assume pointers are always 16-bit */
		uint8_t *next_msg = (uint8_t *)((uint16_t)pkt_addr);
		/* check the length */
		uint8_t len = *(next_msg + LWB_CONF_MAX_DATA_PKT_LEN);
		if (len > LWB_CONF_MAX_DATA_PKT_LEN) {
			DEBUG_PRINT_WARNING("invalid message length detected");
			len = LWB_CONF_MAX_DATA_PKT_LEN; /* truncate */
		}
		memcpy(out_data, next_msg, len);
		return len;
	}
	DEBUG_PRINT_VERBOSE("lwb tx queue empty");
	return 0;
}

/*---------------------------------------------------------------------------*/

/* puts a message into the outgoing queue, returns 1 if successful,
 * 0 otherwise */
// TODO: remove stream_id
uint8_t lwb_send_pkt(uint16_t recipient, uint8_t stream_id, const uint8_t *const data,
                     uint8_t len) {
	/* data has the max. length LWB_DATA_PKT_PAYLOAD_LEN, lwb header needs
	 * to be added before the data is inserted into the queue */
	if (len > LWB_DATA_PKT_PAYLOAD_LEN || !data) {
		DEBUG_PRINT_WARNING("invalid payload length");
		return 0;
	}
	uint32_t pkt_addr = fifo_put(&out_buffer);
	if (FIFO_ERROR != pkt_addr) {
		/* assume pointers are 16-bit */
		uint8_t *next_msg = (uint8_t *)(uint16_t)pkt_addr;
		*(next_msg) = (uint8_t)recipient; /* recipient L */
		*(next_msg + 1) = recipient >> 8; /* recipient H */
		*(next_msg + 2) = stream_id;
		*(next_msg + LWB_CONF_MAX_DATA_PKT_LEN) = len + LWB_CONF_HEADER_LEN;
		memcpy(next_msg + LWB_CONF_HEADER_LEN, data, len);
		return 1;
	}
	stats.txbuf_drop++;
	DEBUG_PRINT_VERBOSE("lwb tx queue full");
	return 0;
}

/*---------------------------------------------------------------------------*/

/* copies the oldest received message in the queue into out_data and returns
 * the message size (in bytes) */
// TODO: remove stream_id
uint8_t lwb_rcv_pkt(uint8_t *out_data, uint16_t *const out_node_id, uint8_t *const out_stream_id) {
	if (!out_data) { return 0; }
	/* messages in the queue have the max. length LWB_CONF_MAX_DATA_PKT_LEN,
	 * lwb header needs to be stripped off; payload has max. length
	 * LWB_DATA_PKT_PAYLOAD_LEN */
	uint32_t pkt_addr = fifo_get(&in_buffer);
	if (FIFO_ERROR != pkt_addr) {
		/* assume pointers are 16-bit */
		uint8_t *next_msg = (uint8_t *)(uint16_t)pkt_addr;
		uint8_t msg_len = *(next_msg + LWB_CONF_MAX_DATA_PKT_LEN) - LWB_CONF_HEADER_LEN;
		if (msg_len > LWB_CONF_MAX_DATA_PKT_LEN) { msg_len = LWB_CONF_MAX_DATA_PKT_LEN; }
		memcpy(out_data, next_msg + LWB_CONF_HEADER_LEN, msg_len);
		if (out_node_id) {
			/* cant just treat next_msg as 16-bit value due to misalignment */
			*out_node_id = (uint16_t)next_msg[1] << 8 | next_msg[0];
		}
		if (out_stream_id) { *out_stream_id = next_msg[2]; }
		return msg_len;
	}
	DEBUG_PRINT_VERBOSE("lwb rx queue empty");
	return 0;
}

/*---------------------------------------------------------------------------*/

void switch_mode(unsigned int modeID) {
	// Prevent other nodes from using this function.
	if (node_id == HOST_ID) {
		control_pkt.newSchedID = modeID;
		control_pkt.newSchedRnds = RNDS_UNTIL_SCHED_SWITCH;
	}
}

/*---------------------------------------------------------------------------*/
// TODO: move variables that both host and source use away from functions (e.g. switched should be a
// static variable in this file rather than in the host function and src function)

/**
 * @brief thread of the host node
 */
PT_THREAD(lwb_thread_host(rtimer_t *rt)) {
	/* all variables must be static */
	static rtimer_clock_t t_start;
	static glossy_payload_t glossy_payload; /* packet buffer */
	/* constant guard time for the host */
	static const uint32_t t_guard = LWB_CONF_T_GUARD;
	static uint8_t slot_idx;
	static uint8_t payload_len;
	static uint8_t switched;
	static const void *callback_func = lwb_thread_host;
#if BURST_EXPERIMENT
	static uint8_t tmp_schedule;
#endif

	/* note: all statements above PT_BEGIN() will be executed each time the
	 * protothread is scheduled */
	PT_BEGIN(&lwb_pt); /* declare variables before this statement! */

	/* initialization specific to the host node */
	sync_state = SYNCED; /* the host is always 'synced' */
	rtimer_reset();
	rt->time = 0;
	schedule = schedule_list[defaultSchedID];
	control_pkt.curSchedID = defaultSchedID;
	control_pkt.newSchedID = defaultSchedID;

	while (1) {
		LWB_RND_ACTIVE;
		// TODO: start preprocess time

		// clear output queue at the beginning of a round
		FIFO_RESET(&out_buffer);
		flood_ok_cnt = 0;
		switched = 0;
		if (control_pkt.newSchedRnds) {
			--control_pkt.newSchedRnds;
			if (control_pkt.newSchedRnds == 0) {
				// if (control_pkt.curSchedID != control_pkt.newSchedID) {
				schedule = schedule_list[control_pkt.newSchedID];
				control_pkt.curSchedID = control_pkt.newSchedID;
				switched = 1;
				// }
			}
		}


#if BURST_EXPERIMENT
		tmp_schedule = 0;
		slot_fail = 0;
		tmp_schedule |=
		    binomial_get_next() ? 1 : 0; // decision for first slot in next LWB data round
		tmp_schedule |=
		    binomial_get_next() ? 2 : 0; // decision for second slot in next LWB data round
#endif

		// Read from AP and send via LWB.
		uint8_t data[LWB_CONF_MAX_DATA_PKT_LEN];
		static uint16_t rx_bytes = 0;

		while (BOLT_DATA_AVAILABLE) {
			rx_bytes = bolt_read(data);
			// TODO: bolt_read returns 2 additional trash bytes at the end (depends on SPI speed)
			if (rx_bytes > 2) {
				// When switching the schedule, current packets from Bolt are thrown away since they
				// are from the old schedule.
				// if (!switched) {
				// TODO: Recipient does not matter here since we do a schedule based check at
				// reception.
				if (!lwb_send_pkt(0, 1, data, rx_bytes - 2)) {
					DEBUG_PRINT_WARNING("out queue full, packet dropped");
				}
			}
		}

#if LWB_CONF_T_PREPROCESS
		if (pre_proc) { (*pre_proc)(); }
		LWB_WAIT_UNTIL(rt->time + LWB_CONF_T_PREPROCESS * RTIMER_SECOND_HF / 1000);
#endif /* LWB_CONF_T_PREPROCESS */

		/* set the start time of the round to the expiration time of the last
		 * scheduled timeout */
		// TODO: Why not refer to round begin? Would make this block superflous and LWB_SEND_SCHED
		// could follow directly after LWB_WAIT_UNTIL.
		t_start = rt->time;

		/* ------------- COMMUNICATION ROUND STARTS ------------- */
		// TODO: stop preprocess time

		LWB_SEND_SCHED(); /* send the previously computed schedule */

		/* ------------- DATA SLOTS ------------- */

		// rcvd_data_pkts = 0; /* number of received data packets in this round */
		// stats.t_proc_max = 0;

		// Skip data slots after schedule switch since data that would be sent in this round was
		// generated from the old schedule.
		if (switched) {
			LWB_WAIT_UNTIL(t_start + LWB_T_SLOT_START(schedule.n_slots));
		} else {
			for (slot_idx = 0; slot_idx < schedule.n_slots; slot_idx++) {

#if BURST_EXPERIMENT
				if (tmp_schedule & (slot_idx + 1)) {
					FAULT_ACTIVE;
					slot_fail = 1;
					FAULT_SUSPENDED;
				} else {
					slot_fail = 0;
				}
#else
				slot_fail = binomial_get_next();
#endif

				/* is this our slot? Note: slots assigned to node ID 0 always belong
				 * to the host */
				if (schedule.slot[slot_idx] == 0 || schedule.slot[slot_idx] == node_id) {
					payload_len = lwb_out_buffer_get(glossy_payload.raw_data);
					if (payload_len) {
						LWB_WAIT_UNTIL(t_start + LWB_T_SLOT_START(slot_idx));
						LWB_SEND_PACKET();
						DEBUG_PRINT_VERBOSE("data packet sent (%ub)", payload_len);
					}
				} else {
					LWB_WAIT_UNTIL(t_start + LWB_T_SLOT_START(slot_idx) - t_guard);
					LWB_RCV_PACKET();
					payload_len = glossy_get_payload_len();
					if (LWB_DATA_RCVD && payload_len) {
						// Decide wether the msg should be delivered or ignored.
						if ( // Check if node identity and target identity match.
						    schedule.nodeIdentity[node_id - 1] == schedule.target[slot_idx] ||
						    // Check if msg should be delivered to all nodes.
						    schedule.target[slot_idx] == ALL_NODES ||
						    // Check if msg should be delivered explicitly to me (node_id).
						    schedule.target[slot_idx] == node_id) {
							/* replace target node ID by sender node ID */
							glossy_payload.data_pkt.recipient = schedule.slot[slot_idx];
							lwb_in_buffer_put(glossy_payload.raw_data, payload_len);
						} else {
							DEBUG_PRINT_VERBOSE("packet dropped, target_id != node_id");
						}
					} else {
						DEBUG_PRINT_VERBOSE("no data received from node %u",
						                    schedule.slot[slot_idx]);
					}
				}
			}
		}

		LWB_RND_SUSPENDED;
		LWB_RND_ACTIVE;

		// Bolt after all data slots

		// Send current schedule to AP
		if (!bolt_write((uint8_t *)&control_pkt, sizeof(control_pkt_t))) {
			DEBUG_PRINT_ERROR("Failed BOLT tramsmission of schedule ID (CP->AP)");
		}
		// Read from LWB and send to AP.
		bolt_data_pkt_t bolt_data;
		uint16_t senderID;
		uint8_t pkt_len = 0;
		do {
			pkt_len = lwb_rcv_pkt(bolt_data.data, &senderID, 0);
			if (pkt_len) {
				bolt_data.senderID = senderID;
				if (!bolt_write((uint8_t *)&bolt_data, pkt_len + 2)) { // 2 bytes senderID
					DEBUG_PRINT_ERROR("Failed BOLT transmission (CP->AP) with pkt_len: %u",
					                  pkt_len);
				}
			}
		} while (pkt_len);

		LWB_RND_SUSPENDED;
		LWB_RND_ACTIVE;

		// if (post_proc) { (*post_proc)(); }
		// TODO: Schedule sync line depending on number of bolt_write calls and data sizes
		LWB_WAIT_UNTIL(t_start + LWB_T_SLOT_START(schedule.n_slots) + SYNC_LINE_GAP);
		LWB_RND_SUSPENDED;
		LWB_RND_ACTIVE;
		PIN_SET(BOLT_CONF_TIMEREQ_PIN);
		PIN_CLR(BOLT_CONF_TIMEREQ_PIN);

#if ENABLE_GUI_MODE_CHANGES
		// Store one byte received over UART.
		static uint8_t rxBuffer[64];
		static uint8_t *rxBufferPtr = rxBuffer;
		static uint8_t *rxBufferEnd = &rxBuffer[sizeof(rxBuffer)];
		uint8_t c;
		if (UCA0IFG & UCRXIFG) { /* data received */
			if (UCA0STAT & UCRXERR) {
				c = UCA0RXBUF; /* clear error flags by forcing a dummy read. */
				// Reading UCAxRXBUF clears all error flags but if UCAxRXBUF was overwritten between
				// the read access to UCAxSTAT and to UCAxRXBUF, UCOE is still set.
				if (UCA0STAT & UCOE) {
					c = UCA0RXBUF; /* clear error flags by forcing a dummy read. */
				}
			} else {
				c = UCA0RXBUF;
				// overflow protection
				if (rxBufferPtr != rxBufferEnd) {
					*rxBufferPtr++ = c; // dereference, store value, increment pointer
				} else {
					rxBufferPtr = rxBuffer; // reset rxBufferPtr
				}
			}
		}

		// Switch schedule if a newline character was received.
		if (*(rxBufferPtr - 1) == 0x0a) { // newline detection
			*(rxBufferPtr - 1) = '\0';    // zero terminate string
			// TODO: no failure handling. atoi returns 0 on failure but we use 0 as a valid mode.
			int newMode = atoi((char *)rxBuffer);
			// DEBUG_PRINT_INFO("switch to %d", newMode);
			switch_mode(newMode);
			rxBufferPtr = rxBuffer; // reset rxBufferPtr
		}
#endif

		LWB_RND_SUSPENDED;

		// // times are in LF ticks (ACLK: 32768 HZ)
		// rtimer_clock_t time_tx = energest_type_time(ENERGEST_TYPE_TRANSMIT);
		// rtimer_clock_t time_rx = energest_type_time(ENERGEST_TYPE_LISTEN);
		// rtimer_clock_t time_cpu = energest_type_time(ENERGEST_TYPE_CPU);
		// radio_on_now = time_tx + time_rx - radio_on_prev;
		// radio_on_prev = time_tx + time_rx;
		// cpu_on_now = time_cpu - cpu_on_prev;
		// cpu_on_prev = time_cpu;

		// DEBUG_PRINT_INFO("msg_missed=%u curSchedID=%u newSchedID=%u newSchedRnds=%u",
		//                  (unsigned int)schedule.n_slots - (unsigned int)flood_ok_cnt,
		//                  (unsigned int)control_pkt.curSchedID, (unsigned int)control_pkt.newSchedID,
		//                  (unsigned int)control_pkt.newSchedRnds);

		debug_print_poll();

		/* suspend this task and wait for the next round */
		LWB_WAIT_UNTIL(t_start + (rtimer_clock_t)(RTIMER_SECOND_HF / schedule.timescale) -
		               LWB_CONF_T_PREPROCESS * RTIMER_SECOND_HF / 1000);
	}

	PT_END(&lwb_pt);
}

/*---------------------------------------------------------------------------*/

/**
 * @brief declaration of the protothread (source node)
 */
PT_THREAD(lwb_thread_src(rtimer_t *rt)) {
	/* all variables must be static */
	static glossy_payload_t glossy_payload; /* packet buffer */
#if BURST_EXPERIMENT
	static uint8_t tmp_schedule;
#endif
	static rtimer_clock_t t_ref;
	static uint32_t t_guard; /* 32-bit is enough for t_guard! */
	static uint8_t slot_idx;
	static uint8_t switched; // TODO: change name
	static uint8_t switch_next_rnd;
	static uint8_t payload_len;
	static const void *callback_func = lwb_thread_src;

	PT_BEGIN(&lwb_pt); /* declare variables before this statement! */

	/* initialization specific to the source node */
	sync_state = BOOTSTRAP;

	while (1) {
		LWB_RND_ACTIVE;

		// clear output queue at the beginning of a round
		FIFO_RESET(&out_buffer);
		flood_ok_cnt = 0;
		switched = 0;
		if (switch_next_rnd) {
			switched = 1;
			switch_next_rnd = 0;
		}

		// Read from AP and send via LWB.
		uint8_t data[LWB_CONF_MAX_DATA_PKT_LEN];
		static uint16_t rx_bytes = 0;

		// TODO: Only one data item should be available?!
		while (BOLT_DATA_AVAILABLE) {
			rx_bytes = bolt_read(data);
			// TODO: bolt_read returns 2 additional trash bytes at the end (depends on SPI speed)
			if (rx_bytes > 2) {
				// TODO: Recipient does not matter here since we do a schedule based check at
				// reception.
				if (!lwb_send_pkt(0, 1, data, rx_bytes - 2)) {
					DEBUG_PRINT_WARNING("out queue full, packet dropped");
				}
			}
		}

#if LWB_CONF_T_PREPROCESS
		if (pre_proc) { (*pre_proc)(); }
		LWB_WAIT_UNTIL(rt->time + LWB_CONF_T_PREPROCESS * RTIMER_SECOND_HF / 1000);
#endif /* LWB_CONF_T_PREPROCESS */

		/* ------------- COMMUNICATION ROUND STARTS ------------- */

#if BURST_EXPERIMENT
		tmp_schedule = 0;
		slot_fail = 0;
#endif

		if (sync_state == BOOTSTRAP) {
		BOOTSTRAP_MODE:
			DEBUG_PRINT_MSG_NOW("BOOTSTRAP ");
			stats.bootstrap_cnt++;
			/* synchronize first! wait for the first schedule... */
			do {
				LWB_RCV_SCHED(); // TODO: currently stores packet in tmp buffer
			} while (!glossy_is_t_ref_updated());
		} else {
			LWB_RCV_SCHED();
		}

		// Update schedule.
		// Manually decrement schedule switch counter if no schedule packet was received.
		if (!glossy_is_t_ref_updated()) {
			if (control_pkt.newSchedRnds) {
				--control_pkt.newSchedRnds;
				if (control_pkt.newSchedRnds == 0) {
					switched = 1;
					schedule = schedule_list[control_pkt.newSchedID];
					control_pkt.curSchedID = control_pkt.newSchedID;
					// Clear the outgoing queue for this round since the messages there are created
					// with the old schedule.
					// FIFO_RESET(&out_buffer);
				}
			}
		} else {
			schedule = schedule_list[control_pkt.curSchedID];
			if (control_pkt.newSchedRnds == 1) switch_next_rnd = 1;
		}

		/* update the sync state machine (compute new sync state and update
		 * t_guard) */
		// LWB_SCHED_SET_AS_1ST(&schedule); // necessary to work around state machine
		LWB_UPDATE_SYNC_STATE(1);
		if (BOOTSTRAP == sync_state) { goto BOOTSTRAP_MODE; }
		// LWB_SCHED_SET_AS_2ND(&schedule); // necessary to work around state machine

		if (glossy_is_t_ref_updated()) {
			/* HF timestamp of first RX; subtract a constant offset */
			t_ref = glossy_get_t_ref() - LWB_CONF_T_REF_OFS;
		} else {
			/* we can only estimate t_ref and t_ref_lf */
			t_ref += (RTIMER_SECOND_HF + stats.drift / 16) / schedule.timescale;
			/* don't update schedule.time here! */
		}

		/* ------------- DATA SLOTS ------------- */

		// Skip data slots after schedule switch since data that would be sent in this round was
		// generated from the old schedule.
		if (switched) {
			LWB_WAIT_UNTIL(t_ref + LWB_T_SLOT_START(schedule.n_slots));
		} else {
			/* permission to participate in this round? */
			if (sync_state == SYNCED || sync_state == UNSYNCED) {
				for (slot_idx = 0; slot_idx < schedule.n_slots; slot_idx++) {
#if BURST_EXPERIMENT
					if (tmp_schedule & (slot_idx + 1)) {
						FAULT_ACTIVE;
						slot_fail = 1;
						FAULT_SUSPENDED;
					} else {
						slot_fail = 0;
					}
#else
					slot_fail = binomial_get_next();
#endif
					if (schedule.slot[slot_idx] == node_id) {
						// stats.t_slot_last = schedule.time;
						/* this is our data slot, send a data packet */
						payload_len = lwb_out_buffer_get(glossy_payload.raw_data);
						if (payload_len) {
							LWB_WAIT_UNTIL(t_ref + LWB_T_SLOT_START(slot_idx));
							LWB_SEND_PACKET();
							DEBUG_PRINT_VERBOSE("data packet sent (%ub)", payload_len);
						} else {
							DEBUG_PRINT_VERBOSE("no message to send (data slot ignored)");
						}
					} else {
						LWB_WAIT_UNTIL(t_ref + LWB_T_SLOT_START(slot_idx) - t_guard);
						LWB_RCV_PACKET();
						payload_len = glossy_get_payload_len();
						/* process the received data */
						if (LWB_DATA_RCVD && payload_len) {
							// Decide wether the msg should be delivered or ignored.
							if ( // Check if node identity and target identity match.
							    schedule.nodeIdentity[node_id - 1] == schedule.target[slot_idx] ||
							    // Check if msg should be delivered to all nodes.
							    schedule.target[slot_idx] == ALL_NODES ||
							    // Check if msg should be delivered explicitly to me (node_id).
							    schedule.target[slot_idx] == node_id ||
							    // Deliver all msgs to relay nodes as well in order to log
							    // communication on AP side.
							    schedule.nodeIdentity[node_id - 1] == RELAY_NODES) {
								/* replace target node ID by sender node ID */
								glossy_payload.data_pkt.recipient = schedule.slot[slot_idx];
								lwb_in_buffer_put(glossy_payload.raw_data, payload_len);
							} else {
								DEBUG_PRINT_VERBOSE("packet dropped, target_id != node_id");
							}
						} else {
							DEBUG_PRINT_VERBOSE("no data received from node %u",
							                    schedule.slot[slot_idx]);
						}
					}
				}
			}
		}

		LWB_RND_SUSPENDED;
		LWB_RND_ACTIVE;

		// Send current schedule to AP
		if (!bolt_write((uint8_t *)&control_pkt, sizeof(control_pkt_t))) {
			DEBUG_PRINT_ERROR("Failed BOLT tramsmission of schedule ID (CP->AP)");
		}

		// Read from LWB and send to AP.
		bolt_data_pkt_t bolt_data;
		uint16_t senderID;
		uint8_t pkt_len = 0;
		do {
			pkt_len = lwb_rcv_pkt(bolt_data.data, &senderID, 0);
			if (pkt_len) {
				bolt_data.senderID = senderID;
				if (!bolt_write((uint8_t *)&bolt_data, pkt_len + 2)) { // 2 bytes senderID
					DEBUG_PRINT_ERROR("Failed BOLT transmission (CP->AP) with pkt_len: %u",
					                  pkt_len);
				}
			}
		} while (pkt_len);

		LWB_RND_SUSPENDED;
		LWB_RND_ACTIVE;

		// if (post_proc) { (*post_proc)(); }
		// TODO: Schedule sync line depending on number of bolt_write calls and data sizes
		LWB_WAIT_UNTIL(t_ref + LWB_T_SLOT_START(schedule.n_slots) + SYNC_LINE_GAP);
		LWB_RND_SUSPENDED;
		LWB_RND_ACTIVE;
		PIN_SET(BOLT_CONF_TIMEREQ_PIN);
		PIN_CLR(BOLT_CONF_TIMEREQ_PIN);

		/* update the state machine and the guard time */
		LWB_UPDATE_SYNC_STATE(0);
		if (BOOTSTRAP == sync_state) { goto BOOTSTRAP_MODE; }

		/* ------------- COMMUNICATION ROUND ENDS ------------- */

		if (sync_state > SYNCED_2) { stats.unsynced_cnt++; }

		LWB_RND_SUSPENDED;

		// times are in LF ticks (ACLK: 32768 HZ)
		// rtimer_clock_t time_tx = energest_type_time(ENERGEST_TYPE_TRANSMIT);
		// rtimer_clock_t time_rx = energest_type_time(ENERGEST_TYPE_LISTEN);
		// rtimer_clock_t time_cpu = energest_type_time(ENERGEST_TYPE_CPU);
		// radio_on_now = time_tx + time_rx - radio_on_prev;
		// radio_on_prev = time_tx + time_rx;
		// cpu_on_now = time_cpu - cpu_on_prev;
		// cpu_on_prev = time_cpu;

		// DEBUG_PRINT_INFO("msg_missed=%u curSchedID=%u newSchedID=%u newSchedRnds=%u",
		//                  (unsigned int)schedule.n_slots - (unsigned int)flood_ok_cnt,
		//                  (unsigned int)control_pkt.curSchedID, (unsigned int)control_pkt.newSchedID,
		//                  (unsigned int)control_pkt.newSchedRnds);

		debug_print_poll();

		LWB_WAIT_UNTIL(t_ref + (RTIMER_SECOND_HF + stats.drift / 16) / schedule.timescale -
		               t_guard - LWB_CONF_T_PREPROCESS * RTIMER_SECOND_HF / 1000);
	}

	PT_END(&lwb_pt);
}

/*---------------------------------------------------------------------------*/

void lwb_resume(void) {
	// Start in one second to give AP enough time to boot.
	rtimer_clock_t start_time = rtimer_now_hf() + RTIMER_SECOND_HF;
	rtimer_id_t timer_id = LWB_CONF_RTIMER_ID;

	if (node_id == HOST_ID) {
		/* note: must add at least some clock ticks! */
		rtimer_schedule(timer_id, start_time, 0, lwb_thread_host);
	} else {
		rtimer_schedule(timer_id, start_time, 0, lwb_thread_src);
	}
}

/*---------------------------------------------------------------------------*/

/* define the process control block */
PROCESS(lwb_process, "Communication Task (LWB)");

/*---------------------------------------------------------------------------*/

/* define the body (protothread) of a process */
PROCESS_THREAD(lwb_process, ev, data) {
	PROCESS_BEGIN();

	/* pass the start addresses of the memory blocks holding the queues */
	fifo_init(&in_buffer, (uint16_t)in_buffer_mem);
	fifo_init(&out_buffer, (uint16_t)out_buffer_mem);

	PT_INIT(&lwb_pt); /* initialize the protothread */

	lwb_resume();

	PROCESS_END();
}

/*---------------------------------------------------------------------------*/

void lwb_start(void (*pre_lwb_proc)(void), void (*post_lwb_proc)(void)) {
	pre_proc = pre_lwb_proc;
	post_proc = post_lwb_proc;

	uart_enable(1);
	printf("Starting '%s'\r\n", lwb_process.name);
	printf("sizeof(control_pkt_t): %u\r\n", sizeof(control_pkt_t));
	//  printf("t_sched=%ums, t_data=%ums, t_cont=%ums, t_round=%ums, "
	//         "data=%ub, slots=%u, tx=%u, hop=%u, scale=%u, sched2=%ums\r\n",
	//         (uint16_t)RTIMER_HF_TO_MS(LWB_CONF_T_SCHED),
	//(uint16_t)RTIMER_HF_TO_MS(LWB_CONF_T_DATA),
	//         (uint16_t)RTIMER_HF_TO_MS(LWB_CONF_T_CONT),
	//(uint16_t)RTIMER_HF_TO_MS(LWB_T_ROUND_MAX),          LWB_CONF_MAX_DATA_PKT_LEN,
	// LWB_CONF_MAX_DATA_SLOTS, LWB_CONF_TX_CNT_DATA,          LWB_CONF_MAX_HOPS,
	// LWB_CONF_TIME_SCALE,            (uint16_t)RTIMER_HF_TO_MS(LWB_CONF_T_SCHED2_START));
	// if ((LWB_CONF_T_SCHED2_START + LWB_CONF_T_SCHED) > (RTIMER_SECOND_HF / LWB_CONF_TIME_SCALE))
	// {    printf("WARNING: LWB_CONF_T_SCHED2_START > min round period\r\n");
	// }
	// if (LWB_CONF_T_SCHED2_START < (LWB_T_ROUND_MAX / LWB_CONF_TIME_SCALE)) {
	//  printf("WARNING: LWB_CONF_T_SCHED2_START < LWB_T_ROUND_MAX!");
	// }
	process_start(&lwb_process, NULL);
}

/*---------------------------------------------------------------------------*/

#endif /* LWB_VERSION */

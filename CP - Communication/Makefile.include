ifndef CONTIKI
  ${error CONTIKI not defined! You must specify where CONTIKI resides}
endif

#ifeq ($(MAKECMDGOALS),upload)
#ifndef NODE_ID
#  ${error Please specify a value for NODE_ID}
#endif
#EXEFILE_ID = $(CONTIKI_PROJECT)-$(NODE_ID).exe
#HEXFILE_ID = $(CONTIKI_PROJECT)-$(NODE_ID).hex
#endif


ifndef TARGET
  ${error TARGET not defined!}
else
  ${info TARGET is $(TARGET)}
endif


MCUFOLDER = cc430
PLATFORM = dpp2-cc430

ifeq ($(TARGET),ddp2-new)
  MCU = cc430f5137
else ifeq ($(TARGET),dpp2)
  MCU = cc430f5147
else
  ${error unknown TARGET}
endif


#$(info MCUFOLDER = $(MCUFOLDER), Platform = $(PLATFORM))

EXEFILE = $(CONTIKI_PROJECT).exe
HEXFILE = $(CONTIKI_PROJECT).hex
DISFILE = $(CONTIKI_PROJECT).dis

COREDIR = $(CONTIKI)/core
SYSDIR  = $(COREDIR)/sys
DEVDIR  = $(COREDIR)/dev
LIBDIR  = $(COREDIR)/lib
NETDIR  = $(COREDIR)/net
SCHEDDIR = $(COREDIR)/net/scheduler
CPUDIR  = $(CONTIKI)/mcu/$(MCUFOLDER)
MCUDIR  = $(CONTIKI)/mcu
PLATDIR = $(CONTIKI)/platform/$(PLATFORM)
TOOLSDIR= $(CONTIKI)/tools
OBJDIR  = ./obj
ifeq (${wildcard $(OBJDIR)},)
  DUMMY := ${shell mkdir $(OBJDIR)}
endif

#CC = msp430-gcc
#LD = msp430-gcc
CC = /usr/local/bin/msp430-gcc
LD = /usr/local/bin/msp430-gcc

CFLAGS  = -mmcu=$(MCU) -Os -Wall -ffunction-sections -fdata-sections -ggdb
LDFLAGS = -mmcu=$(MCU) -Wl,--gc-sections -ggdb

CFLAGS  += -D$(MCU)=1

# add timestamp to the compile flags (to be used in the code), will work on UNIX systems only
#`date '+%s'`
#CFLAGS += -DCOMPILE_TIME=${shell date '+%s'}

# add git revision SHA to compile flags
#git log | head -1 | sed 's/.*\s//' equivalent to git rev-parse HEAD
#ifndef GITSHA
#  GITSHA = ${shell git rev-parse HEAD}
#endif
#CFLAGS += -DGIT_HEADREV_SHA="\"$(GITSHA)\""

CORESRCS = ${shell /usr/local/bin/gfind $(SYSDIR) -type f -name "*.[c]" -printf "%f "}
CORESRCS += ${shell /usr/local/bin/gfind $(LIBDIR) -type f -name "*.[c]" -printf "%f "}
CORESRCS += ${shell /usr/local/bin/gfind $(DEVDIR) -type f -name "*.[c]" -printf "%f "}
CORESRCS += ${shell /usr/local/bin/gfind $(NETDIR) -type f -name "*.[c]" -printf "%f "}
#$(info core = $(CORESRCS))
PLATSRCS = ${shell /usr/local/bin/gfind $(PLATDIR) -type f -name "*.[c]" -printf "%f "}
PLATSRCS += ${shell /usr/local/bin/gfind $(CPUDIR) -type f -name "*.[c]" -printf "%f "}

ifndef APPDIR
  APPDIR = ""
endif

#ifdef WITH_GLOSSY
#  WITH_RADIO = 1
#  PLATSRCS += glossy.c
#  CFLAGS += -DWITH_GLOSSY
#endif
#ifdef WITH_NULLMAC
#  WITH_RADIO = 1
#  PLATSRCS += nullmac.c
#  CFLAGS += -DWITH_NULLMAC
#endif
#ifdef WITH_RADIO
#  PLATSRCS += rf1a.c
#  CFLAGS += -DWITH_RADIO
#endif

OBJS = ${addprefix $(OBJDIR)/,$(CORESRCS:.c=.o) $(PLATSRCS:.c=.o) $(SRCS:.c=.o)}

SOURCEDIRS = . $(COREDIR) $(SYSDIR) $(DEVDIR) $(LIBDIR) $(PLATDIR) $(CONTIKI) $(CPUDIR) $(NETDIR) $(SCHEDDIR) $(MCUDIR) $(APPDIR)
vpath %.c $(SOURCEDIRS)
CFLAGS += ${addprefix -I,$(SOURCEDIRS)}

# forward comma-separated list of arbitrary defines to the compiler
COMMA := ,
CFLAGS += ${addprefix -D,${subst $(COMMA), ,$(DEFINES)}}

$(EXEFILE): $(OBJS)
	$(LD) $(LDFLAGS) -o $@ $^
	@/usr/local/bin/msp430-objcopy $(EXEFILE) -O ihex $(HEXFILE)
	@/usr/local/bin/msp430-objdump -d $(EXEFILE) > $(DISFILE)
	@/usr/local/bin/msp430-size $(EXEFILE)

ifneq ($(MAKECMDGOALS),clean)
-include ${addprefix $(OBJDIR)/,$(CORESRCS:.c=.d) $(PLATSRCS:.c=.d) $(SRCS:.c=.d)}
endif

### See http://make.paulandlesley.org/autodep.html#advanced

define FINALIZE_DEPENDENCY
cp $(@:.o=.d) $(@:.o=.$$$$); \
sed -e 's/#.*//' -e 's/^[^:]*: *//' -e 's/ *\\$$//' \
    -e '/^$$/ d' -e 's/$$/ :/' < $(@:.o=.$$$$) >> $(@:.o=.d); \
rm -f $(@:.o=.$$$$)
endef

$(OBJDIR)/%.o: %.c
	$(CC) $(CFLAGS) -DAUTOSTART_ENABLE -MMD -c $< -o $@
	@$(FINALIZE_DEPENDENCY)

#upload: $(EXEFILE)
#	@$(TOOLSDIR)/tos-set-symbols --exe --objcopy /usr/local/bin/msp430-objcopy --objdump /usr/local/bin/msp430-objdump $(EXEFILE) $(EXEFILE_ID) TOS_NODE_ID=$(NODE_ID)
#	@/usr/local/bin/msp430-objcopy $(EXEFILE_ID) -O ihex $(HEXFILE_ID)
#	@mspdebug uif -j -d /dev/tty.TIVCP3410f* "prog $(HEXFILE_ID)"
#	@rm -f $(EXEFILE_ID) $(HEXFILE_ID)

.PHONY: clean

clean:
	@rm -rf $(OBJDIR) *~ $(CONTIKI)/*~ $(COREDIR)/*~ $(SYSDIR)/*~ $(DEVDIR)/*~ $(LIBDIR)/*~ $(PLATDIR)/*~ $(CPUDIR)/*~

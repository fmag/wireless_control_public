# Wireless Control

This repository provides the public code base of our wireless control research collaboration. The code base is a snapshot of our development branch so do not expect any polished code or thorough documentation. Keep in mind that the code is specifically tailored towards our Dual Processor Platform (DPP) and cart-pole systems.

## Selected Publications

- Wireless Control for Smart Manufacturing: Recent Approaches and Open Challenges (https://arxiv.org/abs/2010.09087)
- Feedback control goes wireless: Guaranteed stability over low-power multi-hop networks (https://arxiv.org/pdf/1804.08986.pdf)
- Fast Feedback Control over Multi-hop Wireless Networks with Mode Changes and Stability Guarantees (https://arxiv.org/pdf/1909.10873.pdf)
- Control-guided Communication: Efficient Resource Arbitration and Allocation in Multi-hop Wireless Control Systems (https://arxiv.org/pdf/1906.03458.pdf)

## Questions?

- dbaumann@tuebingen.mpg.de
- fabian.mager@tu-dresden.de